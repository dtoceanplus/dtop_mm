#/bin/bash

# This is the Main Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

#
# Script Name: prepare_dr_schemas.sh
# Version: 0.1
# TermsOfService: 'https://www.dtoceanplus.eu/'
#
# Description:
#  The script prepares the available digital respesenatation schema and doc (example) for all DTOP modules
#  extracted from module full openapi.yml files. 
#  It uses `prepare_dr_schemas.py` - see also comments there
#
#  bash -c "./prepare_dr_schemas.sh"
#

# Step 1
echo "--------------------------------------------------------------------------------"
echo "Step 1 : Preparation of full dereferenced openapi.yml files for all DTOP modules"
echo ""

START_TIME1=$(date +%s)

git submodule update --init --recursive --remote

END_TIME1=$(date +%s)

echo ""
echo " Recursive cloning api-cooperative.git process took $(($END_TIME1 - $START_TIME1)) seconds"
echo ""
# Recursive cloning api-cooperative.git process took 4 seconds

START_TIME2=$(date +%s)

api_coop="src/digitalrep/api-cooperative"
cp src/digitalrep/src/services/prepare_dr_schema_doc.py ${api_coop}
cd  ${api_coop}

# see details in doc and Makefile of api-cooperative
make test

END_TIME2=$(date +%s)

echo ""
echo " The process of generating ans testing full dereferenced openapi.yml files for all DTOP modules took $(($END_TIME2 - $START_TIME2)) seconds"
echo ""
# The process of generating ans testing full dereferenced openapi.yml files for all DTOP modules took 305 seconds

# Step 2
echo "-----------------------------------------------------------------------------------------------------------------------------------------------"
echo "Step 2 : Preparation of the digital respesenatation schema and doc (example) for all DTOP modules extracted from module full openapi.yml files"
echo ""

START_TIME3=$(date +%s)

dr_schemas_dir="../src/services/dr_schemas"
dr_examples_dir="../src/services/dr_examples"

rm -rf ${dr_schemas_dir}
rm -rf ${dr_examples_dir}

mkdir -p ${dr_schemas_dir}
mkdir -p ${dr_examples_dir}

modules_openapi=$(make --print-data-base x-pre-order | grep x-pre-order:)

for module_openapi in ${modules_openapi}; do
  if [ "${module_openapi}" != "x-pre-order:" ]; then

    echo "${module_openapi}"

    if [ ! -f "${module_openapi}" ]; then
      echo "the file ${module_openapi} is not found"
    fi

    module_name="$(echo ${module_openapi} | cut -d'/' -f1)"
    module_api_version="$(echo ${module_openapi} | cut -d'/' -f2)"
    echo "module_name = " ${module_name}

    python prepare_dr_schema_doc.py ${module_openapi} \
				    ${dr_schemas_dir}/${module_name}_${module_api_version}_dr_schema.yml \
				    ${dr_examples_dir}/${module_name}_${module_api_version}_dr_doc.yml

# python prepare_dr_schema_doc.py ./dtop-slc/0.0.1/openapi.yml ./_test_openapi_slc_dr_schema.yml ./_test_openapi_slc_dr_doc.yml
# python prepare_dr_schema_doc.py ./dtop-rams/0.0.1/openapi.yml ./_test_openapi_rams_dr_schema.yml ./_test_openapi_rams_dr_doc.yml

  fi
done

rm prepare_dr_schema_doc.py
cd ../../..

END_TIME3=$(date +%s)

echo ""
echo " The process of preparation of the digital respesenatation schema and doc (example) for all DTOP modules"
echo "   extracted from module full openapi.yml files"
echo "     took $(($END_TIME3 - $START_TIME3)) seconds"
echo ""

# The process of preparation of the digital respesenatation schema and doc (example) for all DTOP modules"
#   extracted from module full openapi.yml files"
#     took 54 seconds"


# Result on 26.06.2021 :
# api-cooperative provides only 7 openapi with DR endpoints 
