# Integration OpenAPI of Main Module (MM) for using by modules

This document describes the validated MM Integration OpenAPI for using by modules : the endpoints and the examples.

The database should be initialized with test data by the provided MySQL script `mm_mysql_3_db_test_data.sql` or by
bash script `.\create_test_data_with_curl.sh`.


## Get Array of the registered Entities of all modules (projectId, studyId, moduleId, entityId, entityName) for Module with module nickname = {mname} and registered Modules Entities Id = {meid}

  @get('/integration/modules/{mname}/entities/{meid}', {
        description: 'Array of the registered Entities of all modules (projectId, studyId, moduleId, entityId, entityName) for Module with module nickname = {mname} and registered Modules Entities Id = {meid}',

```bash
http://mm.dto.opencascade.com/api/integration/modules/sg/entities/2222

[
  {
    "projectId": 1,
    "studyId": 2,
    "moduleId": 13,
    "entityId": 2222,
    "entityName": "entity name for study 2 for module 13 (SG) and entity id  2222"
  },
  {
    "projectId": 1,
    "studyId": 2,
    "moduleId": 9,
    "entityId": 3333,
    "entityName": "entity name for study 2 for module 9 (RAMS) and entity id  3333"
  }
]
```

## GET Array of Project instances

  @get('/integration/projects', {
        description: 'Array of Project model instances',

```bash
http://mm.dto.opencascade.com/api/integration/projects
[
  {
    "createdAt": "2020-12-30T11:15:23.000Z",
    "description": "desc of project 1 created by user_1",
    "id": 1,
    "logo": "logo 1",
    "name": "project 1",
    "technology": "Wave",
    "typeOfDevice": "Fixed"
  },
  {
    "createdAt": "2020-12-30T12:25:23.000Z",
    "description": "desc of project 2 created by user_2",
    "id": 2,
    "logo": "logo 2",
    "name": "project 2",
    "technology": "Tidal",
    "typeOfDevice": "Floating"
  },
  {
    "createdAt": "2020-12-30T12:35:23.000Z",
    "description": "desc of project 3 created by user_3",
    "id": 3,
    "logo": "logo 3",
    "name": "project 3",
    "technology": "Wave",
    "typeOfDevice": "Floating"
  }
]
```

## GET Project instance

  @get('/integration/projects/{id}', {
        description: 'Project model instance',

```bash
http://mm.dto.opencascade.com/api/integration/projects/1
{
  "createdAt": "2020-12-30T11:15:23.000Z",
  "description": "desc of project 1 created by user_1",
  "id": 1,
  "logo": "logo 1",
  "name": "project 1",
  "technology": "Wave",
  "typeOfDevice": "Fixed"
}
```

## GET Array of Study  instances

  @get('/integration/studies', {
        description: 'Array of Study model instances',

```bash
http://mm.dto.opencascade.com/api/integration/studies
[
  {
    "createdAt": "2020-12-30T10:52:11.000Z",
    "description": "desc of study 1 for project 1 created by user_1",
    "id": 1,
    "name": "study 1",
    "projectId": 1,
    "template": "Design and assessment"
  },
  {
    "createdAt": "2020-12-30T11:52:21.000Z",
    "description": "desc of study 2 for project 1 created by user_1",
    "id": 2,
    "name": "study 2",
    "projectId": 1,
    "template": "Design and assessment"
  },
  {
    "createdAt": "2020-12-30T11:52:31.000Z",
    "description": "desc of study 3 for project 2 created by user_2",
    "id": 3,
    "name": "study 3",
    "projectId": 2,
    "template": "Design and assessment"
  }
]
```

## GET Study instance

  @get('/integration/studies/{id}', {
        description: 'Study model instance',

```bash
http://mm.dto.opencascade.com/api/integration/studies/1
{
  "createdAt": "2020-12-30T10:52:11.000Z",
  "description": "desc of study 1 for project 1 created by user_1",
  "id": 1,
  "name": "study 1",
  "projectId": 1,
  "template": "Design and assessment"
}
```

## GET Array of Module instances for the given Study

  @get('/integration/studies/{id}/modules', {
        description: 'Array of Study has many Module through StudyModule',

```bash
http://mm.dto.opencascade.com/api/integration/studies/1/modules

[
  {
    "description": "Structured Innovation",
    "id": 1,
    "name": "SI",
    "nickname": "si",
    "type": "Default tools"
  },
  {
    "description": "System Performance and Energy Yield",
    "id": 10,
    "name": "SPEY",
    "nickname": "spey",
    "type": "Assessment tools"
  }
]
```

## GET Array of Study instances for the given Project

  @get('/integration/projects/{id}/studies', {
        description: 'Array of Project has many Study',

```bash
http://mm.dto.opencascade.com/api/integration/projects/1/studies
[
  {
    "createdAt": "2020-12-30T10:52:11.000Z",
    "description": "desc of study 1 for project 1 created by user_1",
    "id": 1,
    "name": "study 1",
    "projectId": 1,
    "template": "Design and assessment"
  },
  {
    "createdAt": "2020-12-30T11:52:21.000Z",
    "description": "desc of study 2 for project 1 created by user_1",
    "id": 2,
    "name": "study 2",
    "projectId": 1,
    "template": "Design and assessment"
  }
]
```

## GET Array of the Projects for Module with module nickname = {mname} and registered Modules Entities Id = {meid}

  @get('/integration/modules/{mname}/entities/{meid}/projects', {
        description: 'Array of the Projects for Module with module nickname = {mname} and registered Modules Entities Id = {meid}',

```bash
http://mm.dto.opencascade.com/api/integration/modules/si/entities/1111/projects
[
  {
    "id": 1,
    "name": "project 1"
  }
]
```

## GET Array of the Studies in the Project with project Id = {pid} for Module with module nickname = {mname} and registered Modules Entities Id = {meid}

  @get('/integration/projects/{pid}/modules/{mname}/entities/{meid}/studies', {
        description: 'Array of the Studies in the Project with project Id = {pid} for Module with module nickname = {mname} and registered Modules Entities Id = {meid}',

```bash
http://mm.dto.opencascade.com/api/integration/projects/1/modules/si/entities/1111/studies
[
  {
    "id": 1,
    "name": "study 1",
    "projectId": 1
  }
]
```

## GET Array of the all Modules nicknames registered in the Project with project Id = {pid} and Study with study Id = {sid}

  @get('/integration/projects/{pid}/studies/{sid}/modules', {
        description: 'Array of the all Modules nicknames registered in the Project with project Id = {pid} and Study with study Id = {sid} ',

```bash
http://mm.dto.opencascade.com/api/integration/projects/1/studies/2/modules

[{"nickname":"sg"},{"nickname":"rams"}]
```

## GET Array of the all Modules Entities registered in the Project with project Id = {pid} and Study with study Id = {sid}

  @get('/integration/projects/{pid}/studies/{sid}/entities', {
        description: 'Array of the all Modules Entities registered in the Project with project Id = {pid} and Study with study Id = {sid} ',

```bash
http://mm.dto.opencascade.com/api/integration/projects/1/studies/2/entities
[
  {
    "studyId": 2,
    "moduleId": 2,
    "entityId": 2222,
    "entityName": "entity name for study 2 for module 2 (SG) and entity id  2222",
    "entityStatus": "Finished"
  },
  {
    "studyId": 2,
    "moduleId": 11,
    "entityId": 3333,
    "entityName": "entity name for study 2 for module 11 (RAMS) and entity id  3333",
    "entityStatus": "Finished"
  }
]
```

## GET Array of the all Modules Entities registered in the Project with project Id = {pid} for Module with module nickname = {mname}

  @get('/integration/projects/{pid}/modules/{mname}/entities', {
        description: 'Array of the all Modules Entities registered in the Project with project Id = {pid} for Module with module nickname = {mname}',

```bash
http://mm.dto.opencascade.com/api/integration/projects/1/modules/rams/entities

[
  {
    "studyId": 2,
    "moduleId": 11,
    "entityId": 3333,
    "entityName": "entity name for study 2 for module 11 (RAMS) and entity id  3333",
    "entityStatus": "Finished",
    "studyId": 2
  }
]
```

## GET Array of the all Modules Entities registered in the Project with project Id = {pid} and Study with study Id = {sid} for Module with module nickname = {mname}

  @get('/integration/projects/{pid}/studies/{sid}/modules/{mname}/entities', {
        description: 'Array of the all Modules Entities registered in the Project with project Id = {pid} and Study with study Id = {sid} for Module with module nickname = {mname}',

```bash
http://mm.dto.opencascade.com/api/integration/projects/1/studies/2/modules/sg/entities
[
  {
    "studyId": 2,
    "moduleId": 2,
    "entityId": 2222,
    "entityName": "entity name for study 2 for module 2 (SG) and entity id  2222",
    "entityStatus": "Finished"
  }
]
```
