## !
## Set the module nickname
MODULE_SHORT_NAME=mm

## !
## Set the module docker image TAG
MM_IMAGE_TAG=1.0.0

## !
## Set the value to GitLab container registry of the module
MM_CI_REGISTRY=registry.gitlab.com/dtoceanplus/dtop_mm

## !
## Set DTOP Basic Authentication Header if necessary.
## That is Base64 encoded values of username: and password: that are requested for initial access to modules
## For example: username: "admin@dtop.com", password: "j2zjf#afw21"
## "Basic YWRtaW5AZHRvcC5jb206ajJ6amYjYWZ3MjE="
DTOP_BASIC_AUTH=Basic YWRtaW5AZHRvcC5jb206ajJ6amYjYWZ3MjE=

login:
	echo ${CI_REGISTRY_PASSWORD} | docker login --username ${CI_REGISTRY_USER} --password-stdin ${MM_CI_REGISTRY}

logout:
	docker logout ${MM_CI_REGISTRY}

build-prod-be:
	docker pull ${MM_CI_REGISTRY}/${MODULE_SHORT_NAME}_backend:${MM_IMAGE_TAG} || true
	docker build --cache-from ${MM_CI_REGISTRY}/${MODULE_SHORT_NAME}_backend:${MM_IMAGE_TAG} \
	  --tag ${MM_CI_REGISTRY}/${MODULE_SHORT_NAME}_backend:${MM_IMAGE_TAG} \
          --file ./src/backend/Dockerfile \
          ./src/backend/
	docker push ${MM_CI_REGISTRY}/${MODULE_SHORT_NAME}_backend:${MM_IMAGE_TAG}
	docker images

build-prod-fe:
	docker pull ${MM_CI_REGISTRY}/${MODULE_SHORT_NAME}_frontend:${MM_IMAGE_TAG} || true
	docker build --build-arg DTOP_BASIC_AUTH="${DTOP_BASIC_AUTH}" --build-arg DTOP_MODULE_SHORT_NAME="${MODULE_SHORT_NAME}" \
	  --cache-from ${MM_CI_REGISTRY}/${MODULE_SHORT_NAME}_frontend:${MM_IMAGE_TAG} \
	  --tag ${MM_CI_REGISTRY}/${MODULE_SHORT_NAME}_frontend:${MM_IMAGE_TAG} \
          --file ./src/frontend/frontend-prod.dockerfile \
	  ./src
	docker push ${MM_CI_REGISTRY}/${MODULE_SHORT_NAME}_frontend:${MM_IMAGE_TAG}
	docker images

build-prod-dr:
	docker pull ${MM_CI_REGISTRY}/${MODULE_SHORT_NAME}_digitalrep:${MM_IMAGE_TAG} || true
	docker build --build-arg DTOP_BASIC_AUTH="${DTOP_BASIC_AUTH}" --build-arg DTOP_MODULE_SHORT_NAME="${MODULE_SHORT_NAME}" \
	  --cache-from ${MM_CI_REGISTRY}/${MODULE_SHORT_NAME}_digitalrep:${MM_IMAGE_TAG} \
	  --tag ${MM_CI_REGISTRY}/${MODULE_SHORT_NAME}_digitalrep:${MM_IMAGE_TAG} \
          --file ./src/digitalrep/dr-prod.dockerfile \
	  ./src/digitalrep/
	docker push ${MM_CI_REGISTRY}/${MODULE_SHORT_NAME}_digitalrep:${MM_IMAGE_TAG}
	docker images
