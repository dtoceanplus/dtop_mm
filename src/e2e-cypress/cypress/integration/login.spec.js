import HomePage from '../pages/home.page';

describe('login', () => {
  it('login', () => {
    HomePage.openHomePage();
    HomePage.openLoginForm();
    cy.wait(1000);
    cy.contains('Login');
    HomePage.fillEmail('admin@dtop.com');
    HomePage.fillPassword('adminadmin');
    HomePage.login();
    cy.wait(1000);
    cy.contains('Dashboard')
  });
});
