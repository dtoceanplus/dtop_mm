import HomePage from '../pages/home.page';
import UsersPage from '../pages/users.page';

describe('users', () => {
	beforeEach(() => {
		HomePage.openHomePage();
		HomePage.openLoginForm();
		cy.wait(1000);
		cy.contains('Login');
		HomePage.fillEmail('admin@dtop.com');
		HomePage.fillPassword('adminadmin');
		HomePage.login();
		cy.wait(1000);
		cy.contains('Dashboard')
	});

	it('create user', () => {
		UsersPage.openUsersPage();
		UsersPage.openCreateUserForm();
		cy.wait(1000);
		cy.contains('Create User');

		UsersPage.fillName('test user');
		UsersPage.fillEmail('user-test@dtop.com');
		UsersPage.fillPassword('123123123');
		UsersPage.fillConfirmPassword('123123123');
		UsersPage.fillRoleSelect();
		UsersPage.createUser();
		cy.wait(1000);
		cy.contains('user-test@dtop.com');
	});

	it('delete user', () => {
		UsersPage.openUsersPage();
		cy.wait(1000);
		UsersPage.deleteUserPopup();
		cy.wait(1000);
		cy.contains('Delete User?');
		UsersPage.deleteUser();
		cy.wait(1000);
		cy.get('.table-data').should('not.contain', 'user-test@dtop.com');
	});
});
