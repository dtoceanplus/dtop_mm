import HomePage from '../pages/home.page';
import UsersPage from '../pages/users.page';
import ProjectsPage from '../pages/projects.page';
import DashboardPage from '../pages/dashboard.page';



describe('users', () => {
	before(() => {
		//login as admin
		HomePage.openHomePage();
		HomePage.openLoginForm();
		cy.wait(1000);
		cy.contains('Login');
		HomePage.fillEmail('admin@dtop.com');
		HomePage.fillPassword('adminadmin');
		HomePage.login();
		cy.wait(1000);
		cy.contains('Dashboard');

		// create user
		UsersPage.openUsersPage();
		UsersPage.openCreateUserForm();
		cy.wait(1000);
		cy.contains('Create User');

		UsersPage.fillName('test user');
		UsersPage.fillEmail('user-test@dtop.com');
		UsersPage.fillPassword('123123123');
		UsersPage.fillConfirmPassword('123123123');
		UsersPage.fillRoleSelect();
		UsersPage.createUser();
		cy.wait(1000);
		cy.contains('user-test@dtop.com');

		// logout
		DashboardPage.openDashboardPage();
		DashboardPage.opengetUserCard();
		cy.wait(1000);
		DashboardPage.logout();
		cy.wait(1000);
		cy.url().should('not.include', 'dashboard');

		// login as user
		HomePage.openHomePage();
		HomePage.openLoginForm();
		cy.wait(1000);
		cy.contains('Login');
		HomePage.fillEmail('user-test@dtop.com');
		HomePage.fillPassword('123123123');
		HomePage.login();
		cy.wait(1000);
		cy.contains('Dashboard');
	});


	it('create/delete project', () => {
		ProjectsPage.openProjectsPage();
		ProjectsPage.openCreateProjectForm();
		ProjectsPage.fillName('project test');
		ProjectsPage.fillDescription('project description');
		ProjectsPage.createProject();
		cy.wait(1000);
		cy.contains('project test');

		ProjectsPage.openProjectsPage();
		cy.wait(1000);
		ProjectsPage.deleteProjectPopup();
		cy.wait(1000);
		cy.contains('Delete project');
		ProjectsPage.deleteProject();
		cy.wait(1000);
		cy.get('.table-data').should('not.contain', 'project test');
	})

	after(() => {
		// logout as user
		DashboardPage.openDashboardPage();
		DashboardPage.opengetUserCard();
		cy.wait(1000);
		DashboardPage.logout();
		cy.wait(1000);
		cy.url().should('not.include', 'dashboard');
		
		// login as admin
		HomePage.openHomePage();
		HomePage.openLoginForm();
		cy.wait(1000);
		cy.contains('Login');
		HomePage.fillEmail('admin@dtop.com');
		HomePage.fillPassword('adminadmin');
		HomePage.login();
		cy.wait(1000);
		cy.contains('Dashboard');

		// delete user
		UsersPage.openUsersPage();
		cy.wait(1000);
		UsersPage.deleteUserPopup();
		cy.wait(1000);
		cy.contains('Delete User?');
		UsersPage.deleteUser();
		cy.wait(1000);
		cy.get('.table-data').should('not.contain', 'user-test@dtop.com');
	})
});
