const openUsersPage = () => {
    cy.visit('/users');
};

const getCreateUserPopupBtn = () => {
    return cy.get(`[data-cy-mm=create-user-popup-btn]`);
};

const openCreateUserForm = () => {
    getCreateUserPopupBtn().click({ force: true })
};

const getNameInput = () => {
    return cy.get(`[data-cy-mm=name-input]`);
};

const fillName = (name) => {
    getNameInput().type(name);
};

const getEmailInput = () => {
    return cy.get(`[data-cy-mm=email-input]`);
};

const fillEmail = (email) => {
    getEmailInput().type(email);
};

const getPasswordInput = () => {
    return cy.get(`[data-cy-mm=password-input]`);
};

const fillPassword = (password) => {
    getPasswordInput().type(password);
};

const getConfirmPasswordInput = () => {
    return cy.get(`[data-cy-mm=confirm-password-input]`);
};

const fillConfirmPassword = (password) => {
    getConfirmPasswordInput().type(password);
};

const getRoleSelect = () => {
    return cy.get(`[data-cy-mm=role-select]`);
};

const fillRoleSelect = () => {
	getRoleSelect().click({ force: true });
	cy.wait(1000);
	cy.get('.el-select-dropdown__item').last().click({ force: true });
};

const getCreateUserBtn = () => {
    return cy.get(`[data-cy-mm=create-user-btn]`);
};

const createUser = () => {
    getCreateUserBtn().click({ force: true });
};

const getDeleteUserPopupBtn = () => {
    return cy.get('.el-table__row').last().find(`[data-cy-mm=delete-user-popup-btn]`);
};

const deleteUserPopup = () => {
    getDeleteUserPopupBtn().click({ force: true });
};

const getDeleteUserBtn = () => {
    return cy.get(`[data-cy-mm=delete-user-btn]`);
};

const deleteUser = () => {
    getDeleteUserBtn().click({ force: true });
};

export default {
    openUsersPage,
	openCreateUserForm,
	fillName,
    fillEmail,
	fillPassword,
	fillConfirmPassword,
	fillRoleSelect,
	createUser,
	deleteUserPopup,
	deleteUser
};
