const openHomePage = () => {
    cy.visit('/');
};

const getLoginPopupBtn = () => {
    return cy.get(`[data-cy-mm=login-popup-btn]`);
};

const openLoginForm = () => {
    getLoginPopupBtn().click({ force: true })
};

const getEmailInput = () => {
    return cy.get(`[data-cy-mm=email-input]`);
};

const fillEmail = (email) => {
    getEmailInput().type(email);
};

const getPasswordInput = () => {
    return cy.get(`[data-cy-mm=password-input]`);
};

const fillPassword = (password) => {
    getPasswordInput().type(password);
};

const getLoginBtn = () => {
    return cy.get(`[data-cy-mm=login-btn]`);
};

const login = () => {
    getLoginBtn().click({ force: true });
};

export default {
    openHomePage,
    openLoginForm,
    fillEmail,
    fillPassword,
    login
};
