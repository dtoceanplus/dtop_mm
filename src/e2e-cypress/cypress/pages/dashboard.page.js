const openDashboardPage = () => {
    cy.visit('/dashboard');
};

const getUserCardBtn = () => {
    return cy.get(`[data-cy-mm=open-user-card-btn]`);
};

const opengetUserCard = () => {
    getUserCardBtn().click({ force: true })
};

const getLogoutBtn = () => {
    return cy.get(`[data-cy-mm=logout-btn]`);
};

const logout = () => {
    getLogoutBtn().click({ force: true })
};

export default {
    openDashboardPage,
	opengetUserCard,
	logout
}
