const openProjectsPage = () => {
    cy.visit('/projects');
};

const getCreateProjectPopupBtn = () => {
    return cy.get(`[data-cy-mm=create-project-popup-btn]`);
};

const openCreateProjectForm = () => {
    getCreateProjectPopupBtn().click({ force: true })
};

const getNameInput = () => {
    return cy.get(`[data-cy-mm=name-input]`);
};

const fillName = (name) => {
    getNameInput().type(name);
};

const getDescriptionInput = () => {
    return cy.get(`[data-cy-mm=description-input]`);
};

const fillDescription= (name) => {
    getDescriptionInput().type(name);
};

const getCreateProjectBtn = () => {
    return cy.get(`[data-cy-mm=create-project-input]`);
};

const createProject = () => {
    getCreateProjectBtn().click({ force: true });
};

const getDeleteProjectPopupBtn = () => {
    return cy.get('.el-table__row').last().find(`[data-cy-mm=delete-project-popup-btn]`);
};

const deleteProjectPopup = () => {
    getDeleteProjectPopupBtn().click({ force: true });
};

const getDeleteProjectBtn = () => {
    return cy.get(`[data-cy-mm=delete-project-btn]`);
};

const deleteProject = () => {
    getDeleteProjectBtn().click({ force: true });
};

export default {
    openProjectsPage,
	openCreateProjectForm,
	fillName,
    fillDescription,
	createProject,
	deleteProjectPopup,
	deleteProject
};
