FROM node:14.15-alpine as build-stage

WORKDIR /app

# for build context   ./src :
COPY ./frontend .

# ARG VUE_APP_DTOP_DOMAIN=$DTOP_DOMAIN

ARG DTOP_MODULE_SHORT_NAME
ARG DTOP_BASIC_AUTH

ENV VUE_APP_DTOP_MODULE_SHORT_NAME=$DTOP_MODULE_SHORT_NAME
ENV VUE_APP_DTOP_BASIC_AUTH=$DTOP_BASIC_AUTH

RUN npm ci

RUN npm run build

FROM nginx:1.19-alpine

COPY --from=build-stage /app/dist /usr/share/nginx/html
COPY --from=build-stage /app/prod.conf /etc/nginx/conf.d/local.conf

COPY ./frontend/nginx.conf /etc/nginx/nginx.conf



EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
