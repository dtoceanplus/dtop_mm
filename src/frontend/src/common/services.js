// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
/* eslint-disable */
import axios from 'axios'
import { DTOP_DOMAIN, DTOP_PROTOCOL } from "@/dtop/dtopConsts";
export const openSingleEntity = (entityNickname, id) => {
	let url = "";
	if (entityNickname == 'ED') {
		url = `${DTOP_PROTOCOL}//ed.${DTOP_DOMAIN}/#/energy-deliv-studies/${id}`
	} else if (entityNickname == 'LMO') {
		url = `${DTOP_PROTOCOL}//lmo.${DTOP_DOMAIN}/#/study/${id}/home`
	} else if (entityNickname == 'RAMS') {
		url = `${DTOP_PROTOCOL}//rams.${DTOP_DOMAIN}/#/rams-entity?entityID=${id}`
	} else if (entityNickname == 'SLC') {
		url = `${DTOP_PROTOCOL}//slc.${DTOP_DOMAIN}/#/projects/${id}/home`
	} else if (entityNickname == 'SG') {
		url = `${DTOP_PROTOCOL}//sg.${DTOP_DOMAIN}/#/stage-gate-studies/${id}`
	} else if (entityNickname == 'SC') {
		url = `${DTOP_PROTOCOL}//sc.${DTOP_DOMAIN}/#/inputs?EntityId=${id}`
	} else if (entityNickname == 'SK') {
		url = `${DTOP_PROTOCOL}//sk.${DTOP_DOMAIN}/#/skinputs/devices_positioning?EntityId=${id}`
	} else if (entityNickname == 'ESA') {
		url = `${DTOP_PROTOCOL}//esa.${DTOP_DOMAIN}/#/esainputs/farm_info?EntityId=${id}`
	} else if (entityNickname == 'EC') {
		url = `${DTOP_PROTOCOL}//ec.${DTOP_DOMAIN}/#/ec-entity?entityID=${id}`
	} else if (entityNickname == 'MC') {
		url = `${DTOP_PROTOCOL}//mc.${DTOP_DOMAIN}/#/mc-entity?entityID=${id}`
	} else if (entityNickname == 'ET') {
		url = `${DTOP_PROTOCOL}//et.${DTOP_DOMAIN}/#/analysis-mode/${id}/other_module_inputs`
	} else if (entityNickname == 'SPEY') {
		url = `${DTOP_PROTOCOL}//spey.${DTOP_DOMAIN}/#/spey-inputs?entityID=${id}`
	} else if (entityNickname == 'SI') {
		url = `${DTOP_PROTOCOL}//si.${DTOP_DOMAIN}/${id}`
	}
	window.open(url, '_blank');
}

export const deleteEntityInModule = async (entityName, entityId) => {
	if (entityName == "MC") {
		return axios.delete(`${DTOP_PROTOCOL}//mc.${DTOP_DOMAIN}/mc/${entityId}`);
	} else if (entityName == "LMO") {
		return axios.delete(
			`${DTOP_PROTOCOL}//lmo.${DTOP_DOMAIN}/api/study/${entityId}`
		);
	} else if (entityName == "ED") {
		return axios.delete(
			`${DTOP_PROTOCOL}//ed.${DTOP_DOMAIN}/api/energy-deliv-studies/${entityId}`
		);
	} else if (entityName == "SLC") {
		return axios.delete(
			`${DTOP_PROTOCOL}//slc.${DTOP_DOMAIN}/api/projects/${entityId}`
		);
	} else if (entityName == "SG") {
		return axios.delete(
			`${DTOP_PROTOCOL}//sg.${DTOP_DOMAIN}/api/stage-gate-studies/${entityId}`
		);
	} else if (entityName == "SC") {
		return axios.delete(
			`${DTOP_PROTOCOL}//sc.${DTOP_DOMAIN}/sc/${entityId}`
		);
	} else if (entityName == "SK") {
		return axios.delete(
			`${DTOP_PROTOCOL}//sk.${DTOP_DOMAIN}/sk/${entityId}`
		);
	} else if (entityName == "ESA") {
		return axios.delete(
			`${DTOP_PROTOCOL}//esa.${DTOP_DOMAIN}/esa/${entityId}`
		);
	} else if (entityName == "EC") {
		return axios.delete(
			`${DTOP_PROTOCOL}//ec.${DTOP_DOMAIN}/ec/${entityId}`
		);
	} else if (entityName == "SPEY") {
		return axios.delete(
			`${DTOP_PROTOCOL}//spey.${DTOP_DOMAIN}/spey/${entityId}`
		);
	} else if (entityName == "RAMS") {
		return axios.delete(
			`${DTOP_PROTOCOL}//rams.${DTOP_DOMAIN}/rams/${entityId}`
		);
	} else if (entityName == "ET") {
		return axios.delete(
			`${DTOP_PROTOCOL}//et.${DTOP_DOMAIN}/energy_transf/${entityId}`
		);
	} else if (entityName == "SI") {
		try {
			return axios.delete(`${DTOP_PROTOCOL}//si.${DTOP_DOMAIN}/api/api/${entityId}`);
		} catch (err) { }
	}
}

export const getDrFromModule = async (moduleName, entityId) => {
	return axios.get(`${DTOP_PROTOCOL}//${moduleName}.${DTOP_DOMAIN}/representation/${entityId}`);
}


export const getModulesFullList = () => {
	return [{ "id": 1, "name": "SC", "nickname": "sc", "description": "Site Characterisation", "type": "System" }, { "id": 2, "name": "MC", "nickname": "mc", "description": "Machine Characterisation", "type": "System" }, { "id": 3, "name": "EC", "nickname": "ec", "description": "Energy Capture", "type": "Design tools" }, { "id": 4, "name": "ET", "nickname": "et", "description": "Energy Transformation", "type": "Design tools" }, { "id": 5, "name": "ED", "nickname": "ed", "description": "Energy Delivery", "type": "Design tools" }, { "id": 6, "name": "SK", "nickname": "sk", "description": "Station Keeping", "type": "Design tools" }, { "id": 7, "name": "LMO", "nickname": "lmo", "description": "Logistics and Marine Operation", "type": "Design tools" }, { "id": 8, "name": "SPEY", "nickname": "spey", "description": "System Performance and Energy Yield", "type": "Assessment tools" }, { "id": 9, "name": "RAMS", "nickname": "rams", "description": "Reliability, Availability, Maintainability, Survivability", "type": "Assessment tools" }, { "id": 10, "name": "SLC", "nickname": "slc", "description": "System Lifetime Costs", "type": "Assessment tools" }, { "id": 11, "name": "ESA", "nickname": "esa", "description": "Environmental and Social Acceptance", "type": "Assessment tools" }, { "id": 12, "name": "SI", "nickname": "si", "description": "Structured Innovation", "type": "Default tools" }, { "id": 13, "name": "SG", "nickname": "sg", "description": "Stage Gate", "type": "Default tools" }]
}

export const getModulesDr = () => {
	return [{ "id": 1, "name": "SC", "nickname": "sc", "description": "Site Characterisation", "type": "System" }, { "id": 2, "name": "MC", "nickname": "mc", "description": "Machine Characterisation", "type": "System" }, { "id": 3, "name": "EC", "nickname": "ec", "description": "Energy Capture", "type": "Design tools" }, { "id": 4, "name": "ET", "nickname": "et", "description": "Energy Transformation", "type": "Design tools" }, { "id": 5, "name": "ED", "nickname": "ed", "description": "Energy Delivery", "type": "Design tools" }, { "id": 6, "name": "SK", "nickname": "sk", "description": "Station Keeping", "type": "Design tools" }, { "id": 7, "name": "LMO", "nickname": "lmo", "description": "Logistics and Marine Operation", "type": "Design tools" }, { "id": 8, "name": "SPEY", "nickname": "spey", "description": "System Performance and Energy Yield", "type": "Assessment tools" }, { "id": 9, "name": "RAMS", "nickname": "rams", "description": "Reliability, Availability, Maintainability, Survivability", "type": "Assessment tools" }, { "id": 10, "name": "SLC", "nickname": "slc", "description": "System Lifetime Costs", "type": "Assessment tools" }, { "id": 11, "name": "ESA", "nickname": "esa", "description": "Environmental and Social Acceptance", "type": "Assessment tools" }]
}

export const getModulesList = () => {
	return [{ "id": 3, "name": "EC", "nickname": "ec", "description": "Energy Capture", "type": "Design tools" }, { "id": 4, "name": "ET", "nickname": "et", "description": "Energy Transformation", "type": "Design tools" }, { "id": 5, "name": "ED", "nickname": "ed", "description": "Energy Delivery", "type": "Design tools" }, { "id": 6, "name": "SK", "nickname": "sk", "description": "Station Keeping", "type": "Design tools" }, { "id": 7, "name": "LMO", "nickname": "lmo", "description": "Logistics and Marine Operation", "type": "Design tools" }, { "id": 8, "name": "SPEY", "nickname": "spey", "description": "System Performance and Energy Yield", "type": "Assessment tools" }, { "id": 9, "name": "RAMS", "nickname": "rams", "description": "Reliability, Availability, Maintainability, Survivability", "type": "Assessment tools" }, { "id": 10, "name": "SLC", "nickname": "slc", "description": "System Lifetime Costs", "type": "Assessment tools" }, { "id": 11, "name": "ESA", "nickname": "esa", "description": "Environmental and Social Acceptance", "type": "Assessment tools" }, { "id": 12, "name": "SI", "nickname": "si", "description": "Structured Innovation", "type": "Default tools" }, { "id": 13, "name": "SG", "nickname": "sg", "description": "Stage Gate", "type": "Default tools" }]
}

export const getModulesForkList = () => {
	return [{ "id": 1, "name": "SC", "nickname": "sc", "description": "Site Characterisation", "type": "System" }, { "id": 2, "name": "MC", "nickname": "mc", "description": "Machine Characterisation", "type": "System" },{ "id": 3, "name": "EC", "nickname": "ec", "description": "Energy Capture", "type": "Design tools" }, { "id": 4, "name": "ET", "nickname": "et", "description": "Energy Transformation", "type": "Design tools" }, { "id": 5, "name": "ED", "nickname": "ed", "description": "Energy Delivery", "type": "Design tools" }, { "id": 6, "name": "SK", "nickname": "sk", "description": "Station Keeping", "type": "Design tools" }, { "id": 7, "name": "LMO", "nickname": "lmo", "description": "Logistics and Marine Operation", "type": "Design tools" }, { "id": 8, "name": "SPEY", "nickname": "spey", "description": "System Performance and Energy Yield", "type": "Assessment tools" }, { "id": 9, "name": "RAMS", "nickname": "rams", "description": "Reliability, Availability, Maintainability, Survivability", "type": "Assessment tools" }, { "id": 10, "name": "SLC", "nickname": "slc", "description": "System Lifetime Costs", "type": "Assessment tools" }, { "id": 11, "name": "ESA", "nickname": "esa", "description": "Environmental and Social Acceptance", "type": "Assessment tools" }]
}

export const getMandatoryModules = () => {
	return [{ "id": 1, "name": "SC", "nickname": "sc", "description": "Site Characterisation", "type": "System", "entityId": null }, { "id": 2, "name": "MC", "nickname": "mc", "description": "Machine Characterisation", "type": "System", "entityId": null }]
}

export const getStudyTemplates = () => {
	return [{ name: "Design and Assessment", value: "Design and Assessment", }, { name: "Standalone", value: "Standalone", },]
}
