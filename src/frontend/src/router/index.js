// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
/* eslint-disable */
import Vue from "vue";
import VueRouter from "vue-router";
import checkAuth from "@/services/checkAuth";
import qs from 'qs';

import store from '../store';
Vue.use(VueRouter);

const routes = [{
    path: "/",
    name: "Home",
    component: () =>
        import( /* webpackChunkName: "Home" */ "../views/Home/index.vue"),
    meta: { layout: "home" },
},
{
    path: "/dashboard",
    name: "Dashboard",
    meta: { requiresAuth: true, permission: 'all' },
    component: () =>
        import( /* webpackChunkName: "Dashboard" */ "../views/Dashboard.vue"),
    // meta: { layout: "home" },
},
{
    path: "/projects",
    name: "Projects",
    meta: {
        requiresAuth: true,
        permission: 'user'
    },
    component: () =>
        import( /* webpackChunkName: "Projects" */ "../components/Projects/Projects.vue")
},

{
    path: "/project/:id",
    name: "ProjectSingle",
    meta: {
        requiresAuth: true,
        permission: 'user'
    },
    component: () =>
        import( /* webpackChunkName: "ProjectSingle" */ "../components/ProjectSingle/ProjectSingle.vue")
},
{
    path: "/users",
    name: "users",
    meta: {
        requiresAuth: true,
        permission: 'admin'
    },
    component: () =>
        import( /* webpackChunkName: "users" */ "../components/Users/Users.vue")
},

{
    path: "/logs",
    name: "Logs",
    meta: {
        requiresAuth: true, permission: 'all'
    },
    component: () =>
        import( /* webpackChunkName: "Logs" */ "../views/Logs.vue")
},
{
    path: "/ui-kit",
    name: "UiKit",
    meta: {
        requiresAuth: true, permission: 'all'
    },
    component: () =>
        import( /* webpackChunkName: "UiKit" */ "../views/UiKit.vue")
},
{
    path: "*",
    name: "404",
    component: () =>
        import( /* webpackChunkName: "404" */ "../views/404.vue")
},
];



const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
    parseQuery(query) {
        return qs.parse(query);
    },
    stringifyQuery(query) {
        var result = qs.stringify(query);
        return result ? ('?' + result) : '';
    }
});

router.beforeEach(async (to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (await checkAuth()) {
            if (to.matched.some(record => record.meta.permission != 'all')) {
                if (to.matched.some(record => record.meta.permission == store.state.auth.user.role)) {
                    next();
                } else {
                    next({ name: 'Dashboard' });
                }
                next();
            } else {
                next();
            }
        } else {
            next({ name: 'Home' });
        }
    } else {
        next();
    }
})

export default router;
