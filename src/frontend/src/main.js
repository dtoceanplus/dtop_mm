// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import ElementUI from "element-ui";
import locale from "element-ui/lib/locale/lang/en";
import "element-ui/lib/theme-chalk/index.css";

import Vuelidate from "vuelidate";

import ApiService from "./common/axios.service";

import Home from "./layout/home.vue";
import Default from "./layout/index.vue";

// import {
//   CHECK_AUTH,
// } from "./store/actions.type";

Vue.component("default-layout", Default);
Vue.component("home-layout", Home);

Vue.use(Vuelidate);
Vue.use(ElementUI, { locale });
Vue.config.productionTip = false;

import "@/assets/css/style.scss";

ApiService.init();

// Ensure we checked auth before each page load.

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
