//AUTH
export const LOGIN = "login";
export const LOGOUT = "logout";
export const UPDATE_USER = "updateUser";
export const CHECK_AUTH = "checkAuth";
