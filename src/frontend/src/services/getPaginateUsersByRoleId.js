// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage users.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import getCountUsersByRoleId from "@/services/getCountUsersByRoleId";
import getUsersByRoleId from "@/services/getUsersByRoleId";
import usersComponentFilterToServerFilter from "@/api/usersComponentFilterToServerFilter";

const getPaginateUsersByRoleId = async (roleId, filter) => {
  let copyFilter = usersComponentFilterToServerFilter(
    JSON.parse(JSON.stringify(filter))
  );

  const usersCount = await getCountUsersByRoleId(roleId, copyFilter.where);
  const users = await getUsersByRoleId(roleId, copyFilter);

  return { usersCount, users };
};

export default getPaginateUsersByRoleId;
