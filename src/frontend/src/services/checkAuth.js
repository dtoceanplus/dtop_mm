// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

/* eslint-disable */

// import axios from 'axios'

import cookie from 'js-cookie';
import { decodeToken } from "@/api/token";
import auth from "@/services/auth";
import getUserRole from "@/services/getUserRole";
import store from "@/store";
import { LOGIN } from "@/store/actions.type";

export const checkAuth = async () => {
	try {
		if (store.getters.isAuthenticated) {
			return true;
		} else {
			const userData = cookie.get('accessToken') ? decodeToken(cookie.get('accessToken')) : '';
			if (!userData) return false;
			let user = await auth(userData);
			if (user.data.length == 0) {
				return false;
			}
			const roleId = await getUserRole(user.data[0].id);
			if (roleId) {
				const role = roleId.data[0].roleId == 1 ? "admin" : "user";
				user.data[0].role = role;
				await store.dispatch(LOGIN, user.data[0]);
				return true;
			} else {
				return false;
			}
		}
	} catch (e) {
		this.$notify({
            title: "Error",
            message: `${e}`,
            type: "error",
          });
	}

}

export default checkAuth;
