// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import getCountStudies from "@/services/getCountStudies";
import getStudies from "@/services/getStudies";
import studiesComponentFilterToServerFilter from "@/api/studiesComponentFilterToServerFilter";

import { merge } from "lodash";

const getPaginateStudies = async (projectId, template, filter) => {
  let copyFilter = studiesComponentFilterToServerFilter(
    JSON.parse(JSON.stringify(filter))
  );

  merge(copyFilter, { where: { template: template } });
  let filterResult = {
    where: {
      and: Object.entries(copyFilter.where).map(([k, v]) => ({ [k]: v }))
    }
  };
  //exclude Default Study
  filterResult.where.and.push({
    name: { neq: "Default Study" }
  });
  merge(copyFilter, filterResult);
  const studiesCount = await getCountStudies(projectId, copyFilter.where);
  const studies = await getStudies(projectId, copyFilter);

  return { studiesCount, studies };
};

export default getPaginateStudies;
