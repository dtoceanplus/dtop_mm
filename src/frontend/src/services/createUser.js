// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import axios from "axios";
import createEvent from "@/services/createEvent";
// import hashPassword from '@/api/hashPassword'

const createUser = async (roleId, formData) => {
  // formData.password = await hashPassword(formData.password);
  const newUser = (await axios.post(`roles/${roleId}/users`, formData)).data;
  await createEvent("create the user", `user name: ${newUser.name}, user id: ${newUser.id}`);
  return newUser;
};

export default createUser;
