// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import axios from "axios";
import createEvent from "@/services/createEvent";
import getStudy from "@/services/getStudy";

const deleteStudy = async id => {
  const study = await getStudy(id);
  const deletedStudy = await axios.delete(`/studies/${id}`);
  await createEvent("delete the study", `study name: ${study.name}, study id: ${study.id} from project with id: ${study.projectId}`);
  return deletedStudy;
};

export default deleteStudy;
