// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import Table from "@/components/Table/Table.vue";
import Pagination from "@/components/Pagination/Pagination.vue";
import PerPage from "@/components/PerPage/PerPage.vue";

import getPaginateEvents from "@/services/getPaginateEvents";
import {
	EventsFilterTableFabric,
	EventsFilterTableWhereFabric
} from "@/api/filterTable";

const eventsFilterTableFabric = new EventsFilterTableFabric();
const eventsFilterTableWhereFabric = new EventsFilterTableWhereFabric();

export default {
  components: {
    Table,
    Pagination,
    PerPage
  },
  data() {
    return {
      tableData: [],
      tableHeader: {
        id: true,
        name: true,
        description: true,
        createdAt: true
      },

      loading: true,
      eventsCount: 0,
      filterWhereCopy: eventsFilterTableWhereFabric.createFilterTableWhereFromQuery(
        this.$route.query.where
      ),
      filter: eventsFilterTableFabric.createFilterTableFromQuery(
        this.$route.query
      ),
      defaultSort: {
        prop: this.$route.query.order
          ? this.$route.query.order.split(" ")[0]
          : "",
        order: this.$route.query.order
          ? this.$route.query.order.split(" ")[1] === `DESC`
            ? `descending`
            : `ascending`
          : ""
      }
    };
  },
  async mounted() {
    await this.getPaginateEvents();
  },
  methods: {
    async getPaginateEvents() {
      const paginateEvents = await getPaginateEvents(this.filter);
      this.eventsCount = paginateEvents.eventsCount;
      this.tableData = paginateEvents.events;
    },
    async loadEvents() {
      this.setLoadingState();
      await this.getPaginateEvents();
    },
    filterData() {
      for (let prop in this.filterWhereCopy) {
        if (this.filterWhereCopy[prop] == "")
          this.filterWhereCopy[prop] = undefined;
      }
      this.filter.where ==
        this.$set(
          this.filter,
          "where",
          JSON.parse(JSON.stringify(this.filterWhereCopy))
        );
    },
    resetFilterData() {
      for (let prop in this.filterWhereCopy) {
        this.filterWhereCopy[prop] = undefined;
      }
      this.filter.where ==
        this.$set(
          this.filter,
          "where",
          JSON.parse(JSON.stringify(this.filterWhereCopy))
        );
    },
    changePage(goToPage) {
      this.filter.offset = (goToPage - 1) * this.filter.limit;
    },
    changeOrderItems({ prop, order }) {
      this.filter.order =
        order === null
          ? ""
          : order === `descending`
          ? `${prop} DESC`
          : `${prop} ASC`;
    },
    updateQueryParams() {
      this.$router.replace({
        query: this.queryObj
      });
    },
    setLoadingState() {
      this.loading = true;
    },
    removeLoadingState() {
      this.loading = false;
    },
    resetPagination() {
      if (this.filter.offset === 0) return this.loadEvents();
      this.filter.offset = 0;
    },
    async getEvents() {
      await this.getPaginateEvents(this.$route.query);
    },
  },
  computed: {
    currPage() {
      return parseInt(this.filter.offset / this.filter.limit) + 1;
    },
    queryObj() {
      let filterCopy = JSON.parse(JSON.stringify(this.filter));
      delete filterCopy.offset;
      return {
        ...filterCopy,
        currPage: this.currPage
      };
    }
  },
  watch: {
    "filter.limit": ["resetPagination"],
    "filter.offset": ["loadEvents"],
    "filter.order": ["resetPagination"],
    "filter.where": ["resetPagination"],
    tableData: ["removeLoadingState"],
    filter: {
      handler: "updateQueryParams",
      deep: true
    }
  }
};
