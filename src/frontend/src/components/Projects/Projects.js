// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

/* eslint-disable */
import Table from "@/components/Table/Table.vue";
import Pagination from "@/components/Pagination/Pagination.vue";
import PerPage from "@/components/PerPage/PerPage.vue";

import createStudy from "@/services/createStudy";
import editProject from "@/services/editProject";
import getCountEntitiesInStudy from "@/services/getCountEntitiesInStudy";

import { deleteProjectFromUserMap, getProjectUserMapId } from "@/services/deleteProjectFromUserMap";
import deleteProject from "@/services/deleteProject";
import deleteEntity from "@/services/deleteEntity";
import deleteStudy from "@/services/deleteStudy";
import getAllEntitiesByStudy from "@/services/getAllEntitiesByStudy";
import getStudiesByProjectId from "@/services/getStudiesByProjectId";
import getStudiesOfProjectIntegration from "@/services/getStudiesOfProjectIntegration";
import { deleteEntityInModule, getModulesList } from "@/common/services";
import getPaginateProjects from "@/services/getPaginateProjects";
import createProject from "@/services/createProject";

import { ProjectsFilterTableFabric, ProjectsFilterTableWhereFabric } from "@/api/filterTable";
import { mapGetters } from "vuex";

import {
    humanReadableTitle
} from "@/api/utils";

const projectsFilterTableFabric = new ProjectsFilterTableFabric;
const projectsFilterTableWhereFabric = new ProjectsFilterTableWhereFabric;

export default {
    data() {
        return {
            tableData: [],
            tableHeader: {
                id: true,
                name: true,
                technology: true,
                // typeOfDevice: true,
                createdAt: true
            },

            loading: true,
            projectsCount: 0,
            filterWhereCopy: projectsFilterTableWhereFabric.createFilterTableWhereFromQuery(this.$route.query.where),
            filter: projectsFilterTableFabric.createFilterTableFromQuery(this.$route.query),
            defaultSort: {
                prop: this.$route.query.order ? this.$route.query.order.split(" ")[0] : "",
                order: this.$route.query.order ? this.$route.query.order.split(" ")[1] === `DESC` ? `descending` : `ascending` : "",
            },
            loadingDeletingProjectBtn: false,
            loadingEditingProjectBtn: false,
            projectsListLoading: false,
            projectsList: [],
            loadingCreatingProjectBtn: false,
            formProject: {
                name: "",
                description: "",
                createdAt: "",
                logo: "",
                technology: "Wave",
                typeOfDevice: "Fixed",
            },
            rules: {
                name: [
                    {
                        required: true,
                        message: "Please input project name",
                        trigger: "blur",
                    },
                ],
            },
            actionProject: "create",
            editingProjectId: null,
            projectsHeaders: {
                id: true,
                name: true,
                technology: true,
                createdAt: true,
            },

            studyForm: {
                name: "Default Study",
                template: "Design and assessment",
                description: "SC MC Study",
                projectId: null,
                createdAt: new Date().toISOString(),
            },

            removingProjectDialog: false,
            creatingFormInUse: false,
            userRoleList: [],
            userRole: "",
            deleteProjectId: null,
            deleteFormInUse: false,
            errorMessage: "",
            responseCode: null,

            removingStudiesArray: [],
            removingStudiesCount: null,
            removingEntitiesCount: null,
            removingProject: {
                id: null,
                name: "",
            },
        }
    },
    async mounted() {
        await this.getPaginateProjects();
    },
    methods: {
        async getPaginateProjects() {
            const paginateProjects = await getPaginateProjects(this.currentUser.id, this.filter);
            this.projectsCount = paginateProjects.projectsCount;
            this.tableData = paginateProjects.projects;
        },
        async loadProjects() {
            this.setLoadingState();
            await this.getPaginateProjects();
        },
        filterData() {
            for (let prop in this.filterWhereCopy) {
                if (this.filterWhereCopy[prop] == "") this.filterWhereCopy[prop] = undefined;
            }
            this.filter.where == this.$set(this.filter, "where", JSON.parse(JSON.stringify(this.filterWhereCopy)));
        },
        resetFilterData() {
            for (let prop in this.filterWhereCopy) {
                this.filterWhereCopy[prop] = undefined;
            }
            this.filter.where == this.$set(this.filter, "where", JSON.parse(JSON.stringify(this.filterWhereCopy)));
        },
        changePage(goToPage) {
            this.filter.offset = (goToPage - 1) * this.filter.limit;
        },
        changeOrderItems({ prop, order }) {
            this.filter.order =
                order === null
                    ? ""
                    : order === `descending`
                        ? `${prop} DESC`
                        : `${prop} ASC`;
        },
        updateQueryParams() {
            this.$router.replace({
                query: this.queryObj
            });
        },
        setLoadingState() {
            this.loading = true;
        },
        removeLoadingState() {
            this.loading = false;
        },
        resetPagination() {
            if (this.filter.offset === 0) return this.loadProjects();
            this.filter.offset = 0;
        },
        humanReadableTitle,


        resetForm() {
            this.formProject.name = "";
            this.formProject.description = "";
            this.formProject.createdAt = "";
            this.formProject.logo = "";
            this.formProject.technology = "Wave";
            this.formProject.typeOfDevice = "Fixed";
        },

        async createProject(formName) {
            const valid = await this.$refs[formName].validate();
            if (valid) {
                this.loadingCreatingProjectBtn = true;
                try {
                    this.formProject.createdAt = new Date().toISOString();
                    const newProject = await createProject(
                        this.currentUser.id,
                        this.formProject
                    );
                    this.creatingFormInUse = false;
                    this.studyForm.projectId = newProject.id;
                    await createStudy(this.studyForm);
                    this.$router.push(`project/${newProject.id}`);
                } catch (e) {
                    this.$notify({
                        title: "error",
                        message: "Error.",
                        type: "error",
                    });
                } finally {
                    this.resetForm();
                    this.loadingCreatingProjectBtn = false;
                }
            } else {
                return false;
            }
        },
        createProjectConfirm() {
            delete this.formProject.id;
            this.creatingFormInUse = true;
            this.actionProject = "create";
            this.resetForm();
        },
        editProjectConfirm(projectObj) {
            this.creatingFormInUse = true;
            this.actionProject = "edit";
            this.formProject = { ...this.formProject, ...projectObj };
            this.editingProjectId = this.formProject.id;
        },
        async editProject() {
            this.loadingEditingProjectBtn = true;
            try {
                await editProject(this.editingProjectId, this.formProject);
                await this.getPaginateProjects();
                this.$notify({
                    title: "Success",
                    message: "Project was edited.",
                    type: "success",
                });
            } catch (e) {
                this.$notify({
                    title: "error",
                    message: "Error.",
                    type: "error",
                });
            } finally {
                this.creatingFormInUse = false;
                this.loadingEditingProjectBtn = false;
                this.resetForm();
            }
        },
        async removeEntityOfProject() {
            const moduleIdList = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]; //we do not remove existing SC and MC
            const fullListModules = getModulesList();
            var listOfModuleEntities = [];
            var listOfModuleEntitiesTemp = [];
            const listOfStudies = await getStudiesOfProjectIntegration(this.removingProject.id);
            const loopGetEntities = async () => {
                for (let i = 0; i < listOfStudies.data.length; i++) {
                    var tempList = await getAllEntitiesByStudy(listOfStudies.data[i].id);
                    listOfModuleEntities = listOfModuleEntitiesTemp.concat(tempList.data);
                    listOfModuleEntitiesTemp = listOfModuleEntities;
                }
            }
            await loopGetEntities();
            const loopRemoveEntitiesInModules = async () => {
                for (let i = 0; i < listOfModuleEntities.length; i++) {
                    if (moduleIdList.includes(listOfModuleEntities[i].moduleId)) {
                        var moduleData = fullListModules.filter((el) => el.id == listOfModuleEntities[i].moduleId)
                        try {
                            await deleteEntityInModule(moduleData[0].name, listOfModuleEntities[i].entityId);
                        } catch (err) { }
                    }
                    await deleteEntity(listOfModuleEntities[i].id);
                }
            }
            await loopRemoveEntitiesInModules();
            listOfStudies.data.forEach(async (el) => {
                await deleteStudy(el.id);
            })
        },
        async deleteProjectBtn() {
            this.loadingDeletingProjectBtn = true;
            try {
                const projectUserMapId = await getProjectUserMapId(
                    this.currentUser.id,
                    this.removingProject.id
                );
                await this.removeEntityOfProject();
                await deleteProjectFromUserMap(projectUserMapId.id);
                await deleteProject(this.removingProject.id);
                await this.getPaginateProjects();
                this.removingProjectDialog = false;
                this.$notify({
                    title: "Success",
                    message: "Project was deleted.",
                    type: "success",
                });
            } catch (e) {
                this.$notify({
                    title: "error",
                    message: "Error.",
                    type: "error",
                });
            } finally {
                this.loadingDeletingProjectBtn = false;
            }
        },
        async deleteProjectConfirm(obj) {
            this.removingProject = { ...{ id: obj.id }, ...{ name: obj.name } };
            //reset counter studies & entities
            this.removingEntitiesCount = 0;
            this.removingStudiesCount = 0;

            this.removingStudiesArray = await getStudiesByProjectId(obj.id);
            this.removingStudiesCount = this.removingStudiesArray.length;
            if (this.removingStudiesArray.length > 0) { 
                for (let i = 0; i < this.removingStudiesArray.length; i++) {
                    this.removingEntitiesCount += await getCountEntitiesInStudy(
                        this.removingStudiesArray[i].id
                    );
                }
            }
            this.removingProjectDialog = true;
        },

        onloadImage(p) {
            return new Promise((resolve, reject) => {
                let reader = new FileReader();
                reader.readAsDataURL(p);
                reader.onload = function () {
                    resolve(reader.result);
                };
                reader.onerror = function (error) {
                    reject(error);
                };
            });
        },
        async uploaderLogo(p) {
            this.formProject.logo = await this.onloadImage(p.file);
        },
        beforeAvatarUpload(file) {
            const isJPG_PNG = file.type === "image/jpeg" || "image/png";
            const isLt2M = file.size / 1024 / 1024 < 1;

            if (!isJPG_PNG) {
                this.$message.error("Avatar picture must be JPG or PNG format!");
            }
            if (!isLt2M) {
                this.$message.error("Avatar picture size can not exceed 2MB!");
            }
            return isJPG_PNG && isLt2M;
        },

        goToSingleProjects(projectId) {
            this.$router.push({
                path: `/project/${projectId}`,
            });
        },
    },


    computed: {
        currPage() {
            return parseInt(this.filter.offset / this.filter.limit) + 1;
        },
        queryObj() {
            let filterCopy = JSON.parse(JSON.stringify(this.filter));
            delete filterCopy.offset;
            return {
                ...filterCopy, currPage: this.currPage
            }
        },
        ...mapGetters(["currentUser"]),
    },
    watch: {
        "filter.limit": [
            "resetPagination",
        ],
        "filter.offset": [
            "loadProjects"
        ],
        "filter.order": [
            "resetPagination"
        ],
        "filter.where": [
            "resetPagination"
        ],
        tableData: [
            "removeLoadingState"
        ],
        filter: {
            handler: "updateQueryParams",
            deep: true
        }
    },
    components: {
        Table,
        Pagination,
        PerPage,
    },
}
