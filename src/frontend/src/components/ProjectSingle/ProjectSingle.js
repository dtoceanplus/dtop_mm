// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

/* eslint-disable */
import CreateEntity from "@/components/CreateNewEntity";
import StudyData from "@/components/StudyData";
import ModulesData from "@/components/ModulesData";
import ProjectInformation from "@/components/ProjectInformation";
import ManageUsersInProject from "@/components/ManageUsersInProject";
import ComparisonStudies from "@/components/ComparisonStudies";
import ForkStudy from "@/components/ForkStudy";
import DRStudy from "@/components/DRStudy";
import ShowDRStudy from "@/components/ShowDRStudy";
import TableSimple from "@/components/TableSimple/TableSimple";

import getProjects from "@/services/getProjects";
import getStudiesByProjectId from "@/services/getStudiesByProjectId";
import checkModuleEntityUsedInStudy from "@/services/checkModuleEntityUsedInStudy";
import getEntity from "@/services/getEntity";
import getDefaultStudy from "@/services/getDefaultStudy";
import createEntity from "@/services/createEntity";

import {
  deleteEntityInModule,
  getMandatoryModules,
  openSingleEntity,
} from "@/common/services";

import axios from "axios";
import { mapGetters } from "vuex";

import { DTOP_DOMAIN, DTOP_PROTOCOL } from "@/dtop/dtopConsts";

export default {
  components: {
    study: StudyData,
    module: ModulesData,
    ProjectInformation,
    ManageUsersInProject,
    TableSimple,
    CreateEntity,
    ComparisonStudies,
    ForkStudy,
    DRStudy,
    ShowDRStudy,
  },
  data() {
    return {
      comparingStudiesIds: [],
      drStudyDialog: false,
      drShowStudyDialog: false,
      selectingModuleDialog: false,
      selectingModule: "",
      moduleEntites: [],
      moduleEntitesHeaders: {},
      forkStudyDialog: false,
      activeNames: ["req"],
      selectedTemplateStudy: "Standalone",
      moduleReqData: [],
      moduleReqHeader: {
        description: true,
        type: true,
        entityId: true,
        entityName: true,
      },
      creatingNewEntityFormInUse: false,
      creatingNewModule: {
        name: "",
        id: null,
      },
      selectedScModule: {},
      selectedMcModule: {},
      defaultStudy: {},
      tabs: "Design and assessment",
      project: "",
      selectedModuleReq: {},
      selectedEntityReq: {
        entityId: null,
        entityName: "",
        entityStatus: "",
      },
      selectedStudy: {
        name: "",
        id: null,
        template: "",
      },
      selectedModule: {},
      selectedEntity: {
        entityId: null,
        entityName: "",
        entityStatus: "",
        id: null,
      },
      entitiesData: {
        order: "",
        orderDir: "",
        itemsTotal: 0,
        tableData: [],
        tableDataFiltred: [],
        where: {},
        fields: {
          id: true,
          name: true,
          status: true,
        },
        limit: 5,
        currentPage: 1,
      },
      currentComponentStudies: "study",
    };
  },
  watch: {
    moduleReqData: function (newVal) {
      this.selectedMcModule = newVal.filter((el) => el.name == "MC")[0];
      this.selectedScModule = newVal.filter((el) => el.name == "SC")[0];
    },
    tabs: function () {
      this.currentComponentStudies = "study";
      this.selectedStudy.id = null;
    },
  },
  computed: {
    currentPropertiesStudies: function () {
      if (this.currentComponentStudies === "study") {
        return {
          defaultEntities: [this.selectedScModule, this.selectedMcModule],
          selectedStudy: this.selectedStudy.id,
          template: this.tabs,
          disableCreatingBnt: !(
            this.selectedScModule.entityName && this.selectedMcModule.entityName
          )
            ? true
            : false,
        };
      } else if (this.currentComponentStudies === "module") {
        return {
          studyProps: this.selectedStudy,
        };
      }
    },
    ...mapGetters(["currentUser", "isAuthenticated"]),
  },
  async mounted() {
    this.defaultStudy = await getDefaultStudy(+this.$route.params.id);
    await this.getEntitiesForMandatoryModules();
  },
  methods: {
    async deleteSingleEntity(name, entityId) {
      var projectsList = await getProjects(this.currentUser.id);
      var studiesList = [];
      var studiesListTemp = [];

      const loopGetStudies = async () => {
        for (let i = 0; i < projectsList.length; i++) {
          var tempList = await getStudiesByProjectId(projectsList[i].id);
          studiesList = studiesListTemp.concat(tempList);
          studiesListTemp = studiesList;
        }
      };
      await loopGetStudies();
      let moduleReqData = getMandatoryModules();
      let j = 0;
      var moduleData = moduleReqData.filter((el) => el.name == name);
      const loopCheckStudies = async () => {
        for (let i = 0; i < studiesList.length; i++) {
          var rep;
          try {
            rep = await checkModuleEntityUsedInStudy(
              moduleData[0].id,
              entityId,
              studiesList[i].id
            );
            if (rep.data.length > 0) {
              this.$notify({
                title: "error",
                message: "Entity is used and cannot be deleted.",
                type: "error",
              });
              break;
            } else {
              j++;
            }
          } catch (err) { }
        }
      };
      await loopCheckStudies();
      if (j == studiesList.length) {
        try {
          await deleteEntityInModule(name, entityId);
          if (name == "MC") {
            const checkEmpty = await this.getEntitiesFromMc();
            if (checkEmpty != "No Machine Project saved into the DB") {
              this.moduleEntites = checkEmpty;
            } else {
              this.moduleEntites = [];
            }
          } else if (name == "SC") {
            this.moduleEntites = await this.getEntitiesFromSc();
          }
          this.$notify({
            title: "Success",
            message: "Entity was removed.",
            type: "success",
          });
        } catch (err) {
          if (err != "Error: Request failed with status code 404") {
            //workaround no entities in MC DB
            this.$notify({
              title: "error",
              message: "Error when removing entity.",
              type: "error",
            });
          }
        }
      }
    },
    async selectExistingEntity(props) {
      try {
        let formData;
        if (this.selectingModule == "MC") {
          formData = {
            studyId: this.defaultStudy.id,
            moduleId: 2, //MC
            entityId: props.id,
            entityName: props.title,
          };
        } else if (this.selectingModule == "SC") {
          formData = {
            studyId: this.defaultStudy.id,
            moduleId: 1, //SC
            entityId: props.id,
            entityName: props.name,
          };
        }
        await createEntity(formData);
        await this.getEntitiesForMandatoryModules();
        this.$notify({
          title: "Success",
          message: "Entity was selected and registred.",
          type: "success",
        });
      } catch (e) {
        this.$notify({
          title: "error",
          message: e,
          type: "error",
        });
      } finally {
        this.selectingModuleDialog = false;
      }
    },
    filterMCListByType(list, type) {
      let filteredListOfMC = [];
      for (let i = 0; i < list.length; i++) {
        if (list[i].type == type) {
          filteredListOfMC.push(list[i]);
        }
      }
      return filteredListOfMC;
    },
    async getEntitiesFromMc() {
      var ret = "";
      const listOfMC = (await axios.get(`${DTOP_PROTOCOL}//mc.${DTOP_DOMAIN}/mc`)).data;
      let typeOfTechnology = "";
      let getPromise = axios
        .get(`projects/${this.$route.params.id}`)
        .then((response) => {
          if (response.data.technology == "Wave") {
            typeOfTechnology = "WEC";
          } else if (response.data.technology == "Tidal") {
            typeOfTechnology = "TEC";
          }
        })
        .catch((err) => {
          ret = err.response.data.message;
        });
      await getPromise;
      if (ret == "") {
        const filteredListOfMC = this.filterMCListByType(
          listOfMC,
          typeOfTechnology
        );
        return filteredListOfMC;
      } else {
        return ret;
      }
    },
    async getEntitiesFromSc() {
      return (
        await axios.get(
          `${DTOP_PROTOCOL}//sc.${DTOP_DOMAIN}/sc/GetProjectsList`
        )
      ).data;
    },
    async selectEntityInModule(props) {
      this.selectingModule = props.name;
      this.moduleEntites = [];
      this.selectingModuleDialog = true;

      if (props.nickname == "mc") {
        this.moduleEntitesHeaders = {
          id: true,
          title: true,
          type: true,
          complexity: true,
          status: true,
        };
        try {
          const checkEmpty = await this.getEntitiesFromMc();
          if (checkEmpty != "No Machine Project saved into the DB") {
            this.moduleEntites = checkEmpty;
          } else {
            this.moduleEntites = [];
          }
        } catch (e) {
          this.$notify({
            title: "error",
            message: e.response.status == 404 ? e.response.data.message : e,
            type: "error",
          });
        } finally {
        }
      } else if (props.nickname == "sc") {
        this.moduleEntitesHeaders = {
          id: true,
          name: true,
          time: true,
        };
        try {
          const moduleEntites = await this.getEntitiesFromSc();
          if (moduleEntites.length == 0) {
            this.$notify({
              title: "error",
              message: "No Site Project saved into the DB",
              type: "error",
            });
          } else {
            moduleEntites.forEach((el) => {
              this.moduleEntites.push({
                id: +el.name.split("_")[el.name.split("_").length - 1],
                name: el.name.split("_").slice(0, -1).join("_"),
                time: el.time,
              });
            });
          }
        } catch (e) {
          this.$notify({
            title: "error",
            message: e,
            type: "error",
          });
        } finally {
        }
      }
    },
    tabClick(tab) {
      (this.selectedTemplateStudy = tab.name),
        (this.currentComponentStudies = "study");
      this.selectedStudy.id = null;
    },
    async getEntitiesForMandatoryModules() {
      let moduleReqData = getMandatoryModules();
      for (let i = 0; i < moduleReqData.length; i++) {
        let entityData = await getEntity(
          moduleReqData[i].id,
          this.defaultStudy.id
        );
        moduleReqData[i].entityId =
          entityData.length > 0 ? entityData[0].entityId : null;
        moduleReqData[i].entityName =
          entityData.length > 0 ? entityData[0].entityName : null;
      }
      this.moduleReqData = moduleReqData;
    },
    async createdEntity() {
      await this.getEntitiesForMandatoryModules();
    },
    openSingleEntity,
    createEntity(obj) {
      this.creatingNewEntityFormInUse = true;
      this.creatingNewModule.name = obj.name;
      this.creatingNewModule.id = obj.id;
    },
    //ent
    resetEntity() {
      this.selectedEntity.entityId = null;
      this.selectedEntity.entityName = "";
      this.selectedEntity.entityStatus = "";
      this.selectedEntity.id = null;
      this.entitiesData.tableData = [];
      this.entitiesData.tableDataFiltred = [];
    },
    //studies
    selectStudy(obj) {
      this.selectedStudy = obj;
      this.selectedModule = {};
      this.resetEntity();
      this.currentComponentStudies = "module";
    },
    compareStudy(studyId) {
      if (this.comparingStudiesIds.length > 2) this.comparingStudiesIds.shift();
      this.comparingStudiesIds.push(studyId);
      this.$notify({
        title: "Success",
        message: "Added to comparison.",
        type: "success",
      });
    }
  },
};