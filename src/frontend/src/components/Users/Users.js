// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import Table from "@/components/Table/Table.vue";
import Pagination from "@/components/Pagination/Pagination.vue";
import PerPage from "@/components/PerPage/PerPage.vue";

import getPaginateUsers from "@/services/getPaginateUsers";
import {
  UsersFilterTableFabric,
  UsersFilterTableWhereFabric
} from "@/api/filterTable";

import getUserRoles from "@/services/getUserRoles";
import createUser from "@/services/createUser";
import editUser from "@/services/editUser";
import getUserRoleMapId from "@/services/getUserRoleMapId";
import deleteUserFromRoleMap from "@/services/deleteUserFromRoleMap";
import deleteUser from "@/services/deleteUser";
import getProjectIdsFromUserProjectsMap from "@/services/getProjectIdsFromUserProjectsMap";
import deleteUserFromProjectMap from "@/services/deleteUserFromProjectMap";

import { mapGetters } from "vuex";

const usersFilterTableFabric = new UsersFilterTableFabric();
const usersFilterTableWhereFabric = new UsersFilterTableWhereFabric();

export default {
  components: {
    Table,
    Pagination,
    PerPage
  },
  data() {
    var validatePass = (rule, value, callback) => {
      if (value === "") {
        callback(new Error("Please input user password"));
      } else {
        if (this.formUser.confirmPassword !== "") {
          this.$refs.formUser.validateField("confirmPassword");
        }
        callback();
      }
    };
    var validatePassConfirm = (rule, value, callback) => {
      if (value === "") {
        callback(new Error("Please input user confirm password"));
      } else if (value !== this.formUser.password) {
        callback(new Error("Passwords must be identical"));
      } else {
        callback();
      }
    };
    return {
      tableData: [],
      tableHeader: {
        id: true,
        name: true,
        email: true,
        createdAt: true
      },

      loading: true,
      usersCount: 0,
      filterWhereCopy: usersFilterTableWhereFabric.createFilterTableWhereFromQuery(
        this.$route.query.where
      ),
      filter: usersFilterTableFabric.createFilterTableFromQuery(
        this.$route.query
      ),
      userRoles: [],
      formUser: {
        name: "",
        email: "",
        password: "",
        createdAt: "",
        confirmPassword: "",
        userRole: ""
      },
      rules: {
        name: [
          {
            required: true,
            message: "Please input user name",
            trigger: "change"
          }
        ],
        email: [
          {
            required: true,
            message: "Please input user email",
            trigger: "change"
          },
          {
            type: "email",
            message: "Please input correct email address",
            trigger: ["blur", "change"]
          }
        ],
        password: [
          {
            validator: validatePass,
            trigger: "change"
          },
          {
            min: 8,
            message: "Password must have at least 8 symbols",
            trigger: "blur"
          }
        ],
        confirmPassword: [
          {
            validator: validatePassConfirm,
            trigger: "change"
          },
          {
            min: 8,
            message: "Confirm password must have at least 8 symbols",
            trigger: "blur"
          }
        ],
        userRole: [
          {
            required: true,
            message: "Please select user user role",
            trigger: "change"
          }
        ]
      },
      actionUser: "createUser",
      creatingFormInUse: false,
      loadingCreatingUserBtn: false,
      editingUserId: null,
      projectsIds: [],

      defaultSort: {
        prop: this.$route.query.order
          ? this.$route.query.order.split(" ")[0]
          : "",
        order: this.$route.query.order
          ? this.$route.query.order.split(" ")[1] === `DESC`
            ? `descending`
            : `ascending`
          : ""
      }
    };
  },
  async mounted() {
    await this.getPaginateUsers();
    await this.getUserRoles();
  },
  methods: {
    async getPaginateUsers() {
      const paginateUsers = await getPaginateUsers(this.filter);
      this.usersCount = paginateUsers.usersCount;
      this.tableData = paginateUsers.users;
    },
    async loadUsers() {
      this.setLoadingState();
      await this.getPaginateUsers();
    },
    filterData() {
      for (let prop in this.filterWhereCopy) {
        if (this.filterWhereCopy[prop] == "")
          this.filterWhereCopy[prop] = undefined;
      }
      this.filter.where ==
        this.$set(
          this.filter,
          "where",
          JSON.parse(JSON.stringify(this.filterWhereCopy))
        );
    },
    resetFilterData() {
      for (let prop in this.filterWhereCopy) {
        this.filterWhereCopy[prop] = undefined;
      }
      this.filter.where ==
        this.$set(
          this.filter,
          "where",
          JSON.parse(JSON.stringify(this.filterWhereCopy))
        );
    },
    changePage(goToPage) {
      this.filter.offset = (goToPage - 1) * this.filter.limit;
    },
    changeOrderItems({ prop, order }) {
      this.filter.order =
        order === null
          ? ""
          : order === `descending`
          ? `${prop} DESC`
          : `${prop} ASC`;
    },
    updateQueryParams() {
      this.$router.replace({
        query: this.queryObj
      });
    },
    setLoadingState() {
      this.loading = true;
    },
    removeLoadingState() {
      this.loading = false;
    },
    resetPagination() {
      if (this.filter.offset === 0) return this.loadUsers();
      this.filter.offset = 0;
    },
    async getUsers() {
      await this.getPaginateUsers(this.$route.query);
    },
    async getUserRoles() {
      this.userRoles = await getUserRoles();
    },
    createUserConfirm() {
      this.creatingFormInUse = true;
      this.actionUser = "createUser";
      this.resetForm();
    },
    async createUserSubmit() {
      const valid = await this.$refs["formUser"].validate();
      if (valid) {
        this.loadingCreatingUserBtn = true;
        try {
          let newUserForm = { ...this.formUser };
          const newUserRoleId = this.formUser.userRole;
          delete newUserForm.confirmPassword;
          delete newUserForm.userRole;
          newUserForm.createdAt = new Date().toISOString();
          await createUser(newUserRoleId, newUserForm);
          await this.getUsers();
          this.$notify({
            title: "Success",
            message: "User was created.",
            type: "success"
          });
          this.resetForm();
          this.creatingFormInUse = false;
        } catch (e) {
          this.$notify({
            title: "error",
            message: `Error. ${e.response.data.error.message}`,
            type: "error"
          });
        } finally {
          this.loadingCreatingUserBtn = false;
        }
      }
    },
    editUserInfoConfirm(props) {
      this.creatingFormInUse = true;
      this.actionUser = "editInfo";
      this.editingUserId = props.id;
      delete props.id;
      this.formUser = { ...this.formUser, ...props };
    },
    async editUserInfoSubmit() {
      const valid = await this.$refs["formUser"].validate();
      if (valid) {
        this.loadingCreatingUserBtn = true;
        try {
          await editUser(this.editingUserId, {
            name: this.formUser.name,
            email: this.formUser.email
          });
          await this.getUsers();
          this.$notify({
            title: "Success",
            message: "User was edited.",
            type: "success"
          });
          this.resetForm();
          this.creatingFormInUse = false;
        } catch (e) {
          this.$notify({
            title: "error",
            message: `Error. ${e.response.data.error.message}`,
            type: "error"
          });
        } finally {
          this.loadingCreatingUserBtn = false;
        }
      }
    },
    editUserPassConfirm(props) {
      this.creatingFormInUse = true;
      this.actionUser = "editPass";
      this.editingUserId = props.id;
    },
    async editUserPassSubmit() {
      const valid = await this.$refs["formUser"].validate();
      if (valid) {
        this.loadingCreatingUserBtn = true;
        try {
          await editUser(this.editingUserId, {
            password: this.formUser.password
          });
          await this.getUsers();
          this.$notify({
            title: "Success",
            message: "User password was edited.",
            type: "success"
          });
          this.resetForm();
          this.creatingFormInUse = false;
        } catch (e) {
          this.$notify({
            title: "error",
            message: `Error. ${e.response.data.error.message}`,
            type: "error"
          });
        } finally {
          this.loadingCreatingUserBtn = false;
        }
      }
    },
    async deleteUserConfirm(props) {
      this.creatingFormInUse = true;
      this.actionUser = "deleteUser";
      this.editingUserId = props.id;
      this.loadingCreatingUserBtn = true;
      this.projectsIds = [];
      try {
        this.projectsIds = await getProjectIdsFromUserProjectsMap(
          this.editingUserId
        );
      } catch (e) {
        this.$notify({
          title: "error",
          message: `Error.`,
          type: "error"
        });
      } finally {
        this.loadingCreatingUserBtn = false;
      }
    },
    async deleteUserSubmit() {
      this.loadingCreatingUserBtn = true;
      try {
        for (let i = 0; i < this.projectsIds.length; i++) {
          await deleteUserFromProjectMap(this.projectsIds[i].id);
        }
        const userRoleMapId = await getUserRoleMapId(this.editingUserId);
        await deleteUserFromRoleMap(userRoleMapId);
        await deleteUser(this.editingUserId);
        await this.getUsers();
        this.$notify({
          title: "Success",
          message: "User was deleted.",
          type: "success"
        });
        this.resetForm();
        this.creatingFormInUse = false;
      } catch (e) {
        this.$notify({
          title: "error",
          message: `Error. ${e.response.data.error.message}`,
          type: "error"
        });
      } finally {
        this.loadingCreatingUserBtn = false;
      }
    },
    resetForm() {
      this.formUser.name = "";
      this.formUser.email = "";
      this.formUser.password = "";
      this.formUser.confirmPassword = "";
      this.formUser.createdAt = "";
      this.formUser.userRole = "";
      if (typeof this.$refs["formUser"] !== "undefined")
        this.$refs["formUser"].resetFields();
    }
  },
  computed: {
    currPage() {
      return parseInt(this.filter.offset / this.filter.limit) + 1;
    },
    queryObj() {
      let filterCopy = JSON.parse(JSON.stringify(this.filter));
      delete filterCopy.offset;
      return {
        ...filterCopy,
        currPage: this.currPage
      };
    },
    ...mapGetters(["currentUser"])
  },
  watch: {
    "filter.limit": ["resetPagination"],
    "filter.offset": ["loadUsers"],
    "filter.order": ["resetPagination"],
    "filter.where": ["resetPagination"],
    tableData: ["removeLoadingState"],
    filter: {
      handler: "updateQueryParams",
      deep: true
    }
  }
};
