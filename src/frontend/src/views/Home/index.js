// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
/* eslint-disable */
// import * as THREE from "three";
// import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';


import skyPic from '../../assets/1b.jpeg';
import waterNormalsImage from '../../assets/waternormals.jpg';
import TheLogin from "@/components/TheLogin";
import { mapGetters } from "vuex";

export default {
	components: {
		TheLogin,
	},
	data() {
		return {
			dialogVisible: false,
			email: "",
			formData: {
				email: "",
				password: "",
			},
			responseCode: null,
		};
	},

	computed: {
		...mapGetters(["isAuthenticated"]),
	},
	mounted() {
		if (document.getElementById('js-app') !== null) {
			const waveHeight = 30
			const waveDistance = 220
			const debug = true // toggles debug objects to confirm CPU and GPU wave algorithms are in sync
			const scaleFactor = 29
			const boatLength = 8 * scaleFactor

			setTimeout(() => {
				const loader = new THREE.OBJLoader()
				loader.crossOrigin = 'Anonymous';
				const debugSphereA = new DebugSphere(5, debug)
				const debugSphereB = new DebugSphere(5, debug)
				const material = new THREE.MeshBasicMaterial({ color: 0xffdf04 })
				const box = new THREE.Mesh(new THREE.CylinderGeometry(35, 95, 100, 152), material);
				box.visible = debug
				if (document.getElementById('js-app') !== null) {
					new Simulation('js-app', [debugSphereA, debugSphereB, box])
				}
			}, 0)

			class Simulation {
				constructor(domId, entities) {
					const camera = this.createCamera(110, 100, 100, 2000, document.getElementById('js-app').clientWidth, document.getElementById('js-app').clientHeight)
					camera.target = new THREE.Vector3(0, 0, 0)
					camera.lookAt(camera.target)
					const scene = new THREE.Scene()
					scene.fog = new THREE.Fog(0x5886b3, 200, 900)
					const light = this.createLights(scene)
					const renderer = this.createRenderer(0x5886b3)
					document.getElementById(domId).appendChild(renderer.domElement)
					const handleWindowResize = this.onWindowResize(camera, renderer)
					handleWindowResize()
					window.addEventListener('resize', handleWindowResize, false)
					entities.map((e, i) => {
						scene.add(e)
					})
					const controls = this.createControls(camera)
					const ocean = new Ocean(renderer, camera, scene, 2048, 2048, light, 256, 256)
					scene.add(ocean)
					const skyBox = new SkyBox()
					scene.add(skyBox)
					this.animate(renderer, scene, camera, controls, entities, ocean, +(new Date()))
				}

				createControls(camera) {
					const controls = new THREE.OrbitControls(camera)
					controls.minDistance = 30
					controls.maxDistance = 200
					controls.maxPolarAngle = Math.PI * 0.4
					controls.target.set(0, 0, 0)
					controls.enableZoom = false;
					controls.enabled = false
					return controls
				}

				onWindowResize(camera, renderer) {
					return event => {
						if (document.getElementById('js-app') !== null) {
							const width = document.getElementById('js-app').clientWidth
							const height = document.getElementById('js-app').clientHeight
							camera.aspect = width / height
							camera.updateProjectionMatrix()
							renderer.setSize(width, height)
						}

					}
				}

				animate(renderer, scene, camera, controls, entities, ocean, lastTime) {
					const currentTime = +(new Date())
					const timeDelta = currentTime - lastTime
					// entities.forEach(e => e.time += timeDelta / 1000)

					requestAnimationFrame(() => {
						this.animate(renderer, scene, camera, controls, entities, ocean, currentTime)
					})
					// const mobius = entities[3]
					const t = (currentTime / (1000 * 60)) % 1
					const a = (Math.PI * 2) * t
					const r = 0
					const frontLength = -boatLength / 2
					const backLength = boatLength / 2
					const rotation = 0 // -a + Math.PI
					controls.update()
					const offset = [Math.sin(a) * r, Math.cos(a) * r]
					ocean.water.material.uniforms.time.value += timeDelta / 1000
					ocean.water.material.uniforms.uOffset.value = offset
					ocean.water.render()
					// mobius.uOffset = offset
					const uTime = ocean.water.material.uniforms.time.value
					const originSample = this.calculateSurface(uTime, offset[0], offset[1])
					const dSA = entities[0]
					dSA.position.x = Math.cos(rotation) * backLength
					dSA.position.z = Math.sin(rotation) * backLength
					const backSample = this.calculateSurface(uTime, offset[0] + dSA.position.x, offset[1] + dSA.position.z) - originSample
					dSA.position.y = backSample
					const dSB = entities[1]
					dSB.position.x = Math.cos(rotation) * frontLength
					dSB.position.z = Math.sin(rotation) * frontLength
					const frontSample = this.calculateSurface(uTime, offset[0] + dSB.position.x, offset[1] + dSB.position.z) - originSample
					dSB.position.y = frontSample
					entities[2].rotation.z = Math.atan2(frontSample - backSample, frontLength * 2) + Math.PI
					entities[2].position.y = 0 // 0 // (frontSample + backSample) / 2
					// entities[3].time = uTime

					renderer.render(scene, camera)
				}

				calculateSurface(uTime, x, z) { // make sure this matches the vertex shader function of the same name
					const strength = waveHeight // height of waves
					const uVertexScale = waveDistance // distance between waves
					let y = 0
					y += (Math.sin(x * 1.0 / uVertexScale + uTime * 1.0) + Math.sin(x * 2.3 / uVertexScale + uTime * 1.5) + Math.sin(x * 3.3 / uVertexScale + uTime * 0.4)) / 3.0
					y += (Math.sin(z * 0.2 / uVertexScale + uTime * 1.8) + Math.sin(z * 1.8 / uVertexScale + uTime * 1.8) + Math.sin(z * 2.8 / uVertexScale + uTime * 0.8)) / 3.0
					return y * strength
				}

				createCamera(fov, x = 0, y = 0, z = 0, width, height) {
					const camera = new THREE.PerspectiveCamera(fov, width / height, 0.5, 30000)
					camera.position.x = x
					camera.position.y = y
					camera.position.z = z
					return camera
				}

				createLights(scene) {
					scene.add(new THREE.AmbientLight(0x444444))
					const light = new THREE.DirectionalLight()
					light.position.set(-1, 1, -1)
					scene.add(light)
					return light
				}

				createRenderer(clearColor = 0x000000) {
					const renderer = new THREE.WebGLRenderer({
						antialias: true,
						alpha: true
					})
					renderer.setPixelRatio(window.devicePixelRatio)
					renderer.autoClear = false
					renderer.setClearColor(clearColor, 0)
					return renderer
				}
			}

			class SkyBox extends THREE.Mesh {
				constructor() {
					const cubeMap = new THREE.CubeTexture([])
					cubeMap.format = THREE.RGBFormat
					const loader = new THREE.ImageLoader()
					loader.crossOrigin = 'Anonymous'
					loader.load(skyPic, image => {
						const getSide = (x, y) => {
							const size = 1024
							const canvas = document.createElement('canvas')
							canvas.width = size
							canvas.height = size
							const context = canvas.getContext('2d')
							context.drawImage(image, -x * size, -y * size)
							return canvas
						}
						cubeMap.images[0] = getSide(2, 1) // px
						cubeMap.images[1] = getSide(0, 1) // nx
						cubeMap.images[2] = getSide(1, 0) // py
						cubeMap.images[3] = getSide(1, 2) // ny
						cubeMap.images[4] = getSide(1, 1) // pz
						cubeMap.images[5] = getSide(3, 1) // nz
						cubeMap.needsUpdate = true
					})
					const cubeShader = THREE.ShaderLib['cube']
					cubeShader.uniforms['tCube'].value = cubeMap
					const skyBoxMaterial = new THREE.ShaderMaterial({
						fragmentShader: cubeShader.fragmentShader,
						vertexShader: cubeShader.vertexShader,
						uniforms: cubeShader.uniforms,
						depthWrite: false,
						side: THREE.BackSide
					})
					const geometry = new THREE.BoxGeometry(10000, 10000, 10000)
					super(geometry, skyBoxMaterial)
				}
			}

			const calculateSurfaceVertexShader = `
		  const float seed = 5625463739.0;
		  float calculateSurface (float t, float x, float z) {
			  float strength = ${waveHeight}.0; // height of waves
			  float uVertexScale = ${waveDistance}.0; // distance between waves
			  float y = 0.0;
			  y += (sin(x * 1.0 / uVertexScale + t * 1.0) + sin(x * 2.3 / uVertexScale + t * 1.5) + sin(x * 3.3 / uVertexScale + t * 0.4)) / 3.0;
			  y += (sin(z * 0.2 / uVertexScale + t * 1.8) + sin(z * 1.8 / uVertexScale + t * 1.8) + sin(z * 2.8 / uVertexScale + t * 0.8)) / 3.0;
			  return y * strength;
		  }
		`

			class DebugSphere extends THREE.Mesh {
				constructor(radius, debug) {
					const geometry = new THREE.SphereGeometry(radius, 20, 20)
					const material = new THREE.MeshBasicMaterial({ color: 0xe4a4b6 })
					super(geometry, material)
					this.time = 0
					this.visible = debug
				}
			}

			class Ocean extends THREE.Mesh {
				constructor(renderer, camera, scene, width, height, light, horizontalSegments = 128, verticalSegments = 128) {
					const loader = new THREE.TextureLoader()
					loader.crossOrigin = 'Anonymous'
					const waterNormals = loader.load(waterNormalsImage)
					waterNormals.wrapS = waterNormals.wrapT = THREE.RepeatWrapping
					const water = new THREE.Water(renderer, camera, scene, {
						textureWidth: 512,
						textureHeight: 512,
						waterNormals: waterNormals,
						alpha: 1.0,
						sunDirection: light.position.clone().normalize(),
						sunColor: 0x022c5f,
						waterColor: 0x022c5f,
						distortionScale: 20.0,
						fog: scene.fog != undefined
					})
					// const geometry = new THREE.PlaneBufferGeometry(width * 500, height * 500)
					const geometry = new THREE.PlaneBufferGeometry(width, height, horizontalSegments, verticalSegments)
					super(geometry, water.material)
					this.add(water)
					this.rotation.x = -Math.PI * 0.5
					this.water = water
				}
			}

			class MobiusRot extends THREE.Mesh {
				static createMaterial(radius, size) {
					const spacingFactor = 1.5
					return new THREE.BAS.BasicAnimationMaterial({
						shading: THREE.FlatShading,
						side: THREE.FrontSide,
						uniforms: {
							uTime: { value: 0 },
							uRadius: { value: radius * 2 },
							uOffset: { value: new THREE.Vector2(0, 0) },
							uSpacing: { value: new THREE.Vector3(size * spacingFactor, size * spacingFactor, size * spacingFactor) }
						},
						uniformValues: {},
						varyingParameters: [
							'varying vec3 vColor;'
						],
						vertexFunctions: [
							THREE.BAS.ShaderChunk['quaternion_rotation'],
							calculateSurfaceVertexShader
						],
						vertexParameters: [
							'uniform float uTime;',
							'uniform float uRadius;',
							'uniform vec3 uSpacing;',
							'uniform vec2 uOffset;',
							'attribute vec4 aRotation;',
							'attribute vec3 aVOffset;',
							'attribute vec4 aTwist;',
							'attribute vec4 aOrient;',
							'attribute vec3 aColor;'
						],
						vertexInit: [
							'float timeScale = 0.333;',
							'float seed = 5625463739.0;',
							'float time = (uTime * timeScale);',
							'vec4 tQuatRotation = quatFromAxisAngle(aRotation.xyz, aRotation.w + time);',
							'vec4 tQuatOrient = quatFromAxisAngle(aOrient.xyz, aOrient.w + time);',
							'vec4 tQuatTwist = quatFromAxisAngle(aTwist.xyz, aTwist.w + time);'
						],
						vertexNormal: [],
						vertexPosition: [
							'vec3 center = rotateVector(tQuatRotation, vec3(uRadius, 0.0, 0.0));', // place columns in a large circle
							'transformed += aVOffset * uSpacing;', // stack tiles into a vertical column
							'transformed = rotateVector(tQuatTwist, transformed);', // twist/lean columns, rotate along center axis -\|/-
							'transformed = rotateVector(tQuatOrient, transformed);', // rotate column faces inwards
							'transformed += rotateVector(tQuatRotation, vec3(uRadius, 0.0, 0.0));', // place columns in a large circle
							'transformed.y += calculateSurface(uTime, center.x, -center.z);', // i don't understand why the -z is required, maybe because the ocean plane is rotated?
							'transformed.y -= calculateSurface(uTime, 0.0, 0.0);'
						],
						vertexColor: [
							'vColor = aColor;'
						],
						fragmentFunctions: [],
						fragmentParameters: [],
						fragmentInit: [],
						fragmentMap: [],
						fragmentDiffuse: [
							'diffuseColor.xyz = vColor;'
						]
					})
				}

				static assignProps(geometry, prefabCount, radius, layers, twists, verticalSpacing) {
					const aRotation = geometry.createAttribute('aRotation', 4)
					const aColor = geometry.createAttribute('aColor', 3)
					const aVOffset = geometry.createAttribute('aVOffset', 3)
					const aTwist = geometry.createAttribute('aTwist', 4)
					const aOrient = geometry.createAttribute('aOrient', 4)
					const ringSize = prefabCount / layers
					for (let i = 0; i < prefabCount; i++) {
						const y = Math.floor(i / ringSize)
						const x = i % ringSize
						const range = Math.PI * 2
						const theta = THREE.Math.mapLinear(x, 0, ringSize, -range / 2, range / 2)
						const w = (theta / 2) * twists
						// place tiles in circle
						const position = new THREE.Vector3(Math.cos(theta), 0, Math.sin(theta))
						geometry.setPrefabData(aRotation, i, [0, 1, 0, theta])
						// stack the tiles vertically into columns
						const verticalOffset = new THREE.Vector3(0, (Math.ceil(y / 2) * (y % 2 ? 1 : -1)) / layers, 0)
						geometry.setPrefabData(aVOffset, i, verticalOffset.toArray())
						// set lean
						geometry.setPrefabData(aTwist, i, [0, 0, 1, w])
						// set orient
						geometry.setPrefabData(aOrient, i, [0, 1, 0, theta])
						const color = new THREE.Color()
						if (verticalOffset.y === 0) {
							color.setHSL((theta + Math.PI) / (Math.PI * 2), 0.8, 0.5)
						} else {
							color.setHSL((theta + Math.PI) / (Math.PI * 2), 0.8, 0.2 + 0.4 * (y / layers))
						}
						geometry.setPrefabData(aColor, i, color.toArray())
					}
				}

				constructor(size, tileCount, radius, layers, twists) {
					const prefabCount = tileCount * layers
					const boxGeometry = new THREE.BoxGeometry(size, size, size)
					const geometry = new THREE.BAS.PrefabBufferGeometry(boxGeometry, prefabCount)
					geometry.computeVertexNormals()
					geometry.bufferUvs()
					// MobiusRot.assignProps(geometry, prefabCount, radius, layers, twists, size)
					// const material = MobiusRot.createMaterial(radius, size * layers)
					// super(geometry, material)
					this.frustumCulled = false
				}

				get time() {
					return this.material.uniforms.uTime.value
				}

				set time(newTime) {
					this.material.uniforms.uTime.value = newTime
				}

				get offset() {
					return this.material.uniforms.uOffset.value
				}

				set offset(newOffset) {
					this.material.uniforms.uOffset.value = newOffset
				}
			}

			THREE.Water = function (renderer, camera, scene, options) {

				THREE.Object3D.call(this);
				this.name = 'water_' + this.id;

				function optionalParameter(value, defaultValue) {

					return value !== undefined ? value : defaultValue;

				}

				options = options || {};

				this.matrixNeedsUpdate = true;

				var width = optionalParameter(options.textureWidth, 512);
				var height = optionalParameter(options.textureHeight, 512);
				this.clipBias = optionalParameter(options.clipBias, 0.0);
				this.alpha = optionalParameter(options.alpha, 1.0);
				this.time = optionalParameter(options.time, 0.0);
				this.normalSampler = optionalParameter(options.waterNormals, null);
				this.sunDirection = optionalParameter(options.sunDirection, new THREE.Vector3(0.70707, 0.70707, 0.0));
				this.sunColor = new THREE.Color(optionalParameter(options.sunColor, 0x022c5f));
				this.waterColor = new THREE.Color(optionalParameter(options.waterColor, 0x022c5f));
				this.eye = optionalParameter(options.eye, new THREE.Vector3(0, 0, 0));
				this.distortionScale = optionalParameter(options.distortionScale, 20.0);
				this.side = optionalParameter(options.side, THREE.FrontSide);
				this.fog = optionalParameter(options.fog, false);

				this.renderer = renderer;
				this.scene = scene;
				this.mirrorPlane = new THREE.Plane();
				this.normal = new THREE.Vector3(0, 0, 1);
				this.mirrorWorldPosition = new THREE.Vector3();
				this.cameraWorldPosition = new THREE.Vector3();
				this.rotationMatrix = new THREE.Matrix4();
				this.lookAtPosition = new THREE.Vector3(0, 0, - 1);
				this.clipPlane = new THREE.Vector4();

				if (camera instanceof THREE.PerspectiveCamera) {

					this.camera = camera;

				} else {
					this.camera = new THREE.PerspectiveCamera();
				}

				this.textureMatrix = new THREE.Matrix4();

				this.mirrorCamera = this.camera.clone();

				this.renderTarget = new THREE.WebGLRenderTarget(width, height);
				this.renderTarget2 = new THREE.WebGLRenderTarget(width, height);

				var mirrorShader = {

					uniforms: THREE.UniformsUtils.merge([
						THREE.UniformsLib['fog'],
						{
							normalSampler: { value: null },
							mirrorSampler: { value: null },
							alpha: { value: 1.0 },
							uOffset: { value: new THREE.Vector2(0, 0) },
							time: { value: 0.0 },
							distortionScale: { value: 20.0 },
							noiseScale: { value: 1.0 },
							textureMatrix: { value: new THREE.Matrix4() },
							sunColor: { value: new THREE.Color(0x022c5f) },
							sunDirection: { value: new THREE.Vector3(0.70707, 0.70707, 0) },
							eye: { value: new THREE.Vector3() },
							waterColor: { value: new THREE.Color(0x022c5f) }
						}
					]),

					vertexShader: [
						'uniform mat4 textureMatrix;',
						'uniform float time;',
						'uniform vec2 uOffset;',

						'varying vec4 mirrorCoord;',
						'varying vec3 worldPosition;',

						THREE.ShaderChunk['fog_pars_vertex'],

						calculateSurfaceVertexShader,
						'void main() {',
						' vec3 pos = position;',
						' pos.z += calculateSurface(time, pos.x + uOffset.x, pos.y + uOffset.y);',
						' pos.z -= calculateSurface(time, uOffset.x, uOffset.y);',
						'	mirrorCoord = modelMatrix * vec4( pos, 1.0 );',
						'	worldPosition = mirrorCoord.xyz;',
						'	mirrorCoord = textureMatrix * mirrorCoord;',
						'	vec4 mvPosition =  modelViewMatrix * vec4( pos, 1.0 );',
						'	gl_Position = projectionMatrix * mvPosition;',

						THREE.ShaderChunk['fog_vertex'],

						'}'
					].join('\n'),

					fragmentShader: [
						'precision highp float;',

						'uniform sampler2D mirrorSampler;',
						'uniform float alpha;',
						'uniform float time;',
						'uniform float distortionScale;',
						'uniform sampler2D normalSampler;',
						'uniform vec3 sunColor;',
						'uniform vec3 sunDirection;',
						'uniform vec3 eye;',
						'uniform vec3 waterColor;',

						'varying vec4 mirrorCoord;',
						'varying vec3 worldPosition;',

						'vec4 getNoise( vec2 uv ) {',
						'	vec2 uv0 = ( uv / 103.0 ) + vec2(time / 17.0, time / 29.0);',
						'	vec2 uv1 = uv / 107.0-vec2( time / -19.0, time / 31.0 );',
						'	vec2 uv2 = uv / vec2( 8907.0, 9803.0 ) + vec2( time / 101.0, time / 97.0 );',
						'	vec2 uv3 = uv / vec2( 1091.0, 1027.0 ) - vec2( time / 109.0, time / -113.0 );',
						'	vec4 noise = texture2D( normalSampler, uv0 ) +',
						'		texture2D( normalSampler, uv1 ) +',
						'		texture2D( normalSampler, uv2 ) +',
						'		texture2D( normalSampler, uv3 );',
						'	return noise * 0.5 - 1.0;',
						'}',

						'void sunLight( const vec3 surfaceNormal, const vec3 eyeDirection, float shiny, float spec, float diffuse, inout vec3 diffuseColor, inout vec3 specularColor ) {',
						'	vec3 reflection = normalize( reflect( -sunDirection, surfaceNormal ) );',
						'	float direction = max( 0.0, dot( eyeDirection, reflection ) );',
						'	specularColor += pow( direction, shiny ) * sunColor * spec;',
						'	diffuseColor += max( dot( sunDirection, surfaceNormal ), 0.0 ) * sunColor * diffuse;',
						'}',

						THREE.ShaderChunk['common'],
						THREE.ShaderChunk['fog_pars_fragment'],

						'void main() {',
						'	vec4 noise = getNoise( worldPosition.xz );',
						'	vec3 surfaceNormal = normalize( noise.xzy * vec3( 1.5, 1.0, 1.5 ) );',

						'	vec3 diffuseLight = vec3(0.0);',
						'	vec3 specularLight = vec3(0.0);',

						'	vec3 worldToEye = eye-worldPosition;',
						'	vec3 eyeDirection = normalize( worldToEye );',
						'	sunLight( surfaceNormal, eyeDirection, 100.0, 2.0, 0.5, diffuseLight, specularLight );',

						'	float distance = length(worldToEye);',

						'	vec2 distortion = surfaceNormal.xz * ( 0.001 + 1.0 / distance ) * distortionScale;',
						'	vec3 reflectionSample = vec3( texture2D( mirrorSampler, mirrorCoord.xy / mirrorCoord.z + distortion ) );',

						'	float theta = max( dot( eyeDirection, surfaceNormal ), 0.0 );',
						'	float rf0 = 0.3;',
						'	float reflectance = rf0 + ( 1.0 - rf0 ) * pow( ( 1.0 - theta ), 5.0 );',
						'	vec3 scatter = max( 0.0, dot( surfaceNormal, eyeDirection ) ) * waterColor;',
						'	vec3 albedo = mix( sunColor * diffuseLight * 0.3 + scatter, ( vec3( 0.1 ) + reflectionSample * 0.9 + reflectionSample * specularLight ), reflectance );',
						'	vec3 outgoingLight = albedo;',
						'	gl_FragColor = vec4( outgoingLight, alpha );',

						THREE.ShaderChunk['fog_fragment'],

						'}'
					].join('\n')

				};

				var mirrorUniforms = THREE.UniformsUtils.clone(mirrorShader.uniforms);

				this.material = new THREE.ShaderMaterial({
					fragmentShader: mirrorShader.fragmentShader,
					vertexShader: mirrorShader.vertexShader,
					uniforms: mirrorUniforms,
					transparent: true,
					side: this.side,
					fog: this.fog
				});

				this.material.uniforms.mirrorSampler.value = this.renderTarget.texture;
				this.material.uniforms.textureMatrix.value = this.textureMatrix;
				this.material.uniforms.alpha.value = this.alpha;
				this.material.uniforms.time.value = this.time;
				this.material.uniforms.normalSampler.value = this.normalSampler;
				this.material.uniforms.sunColor.value = this.sunColor;
				this.material.uniforms.waterColor.value = this.waterColor;
				this.material.uniforms.sunDirection.value = this.sunDirection;
				this.material.uniforms.distortionScale.value = this.distortionScale;

				this.material.uniforms.eye.value = this.eye;

				if (!THREE.Math.isPowerOfTwo(width) || !THREE.Math.isPowerOfTwo(height)) {

					this.renderTarget.texture.generateMipmaps = false;
					this.renderTarget.texture.minFilter = THREE.LinearFilter;
					this.renderTarget2.texture.generateMipmaps = false;
					this.renderTarget2.texture.minFilter = THREE.LinearFilter;

				}

				this.updateTextureMatrix();
				this.render();

			};

			THREE.Water.prototype = Object.create(THREE.Object3D.prototype);
			THREE.Water.prototype.constructor = THREE.Water;

			THREE.Water.prototype.render = function () {

				if (this.matrixNeedsUpdate) this.updateTextureMatrix();

				this.matrixNeedsUpdate = true;

				// Render the mirrored view of the current scene into the target texture
				var scene = this;

				while (scene.parent !== null) {

					scene = scene.parent;

				}

				if (scene !== undefined && scene instanceof THREE.Scene) {

					this.material.visible = false;

					this.renderer.render(scene, this.mirrorCamera, this.renderTarget, true);

					this.material.visible = true;

				}

			};

			THREE.Water.prototype.updateTextureMatrix = function () {

				this.updateMatrixWorld();
				this.camera.updateMatrixWorld();

				this.mirrorWorldPosition.setFromMatrixPosition(this.matrixWorld);
				this.cameraWorldPosition.setFromMatrixPosition(this.camera.matrixWorld);

				this.rotationMatrix.extractRotation(this.matrixWorld);

				this.normal.set(0, 0, 1);
				this.normal.applyMatrix4(this.rotationMatrix);

				var view = this.mirrorWorldPosition.clone().sub(this.cameraWorldPosition);
				view.reflect(this.normal).negate();
				view.add(this.mirrorWorldPosition);

				this.rotationMatrix.extractRotation(this.camera.matrixWorld);

				this.lookAtPosition.set(0, 0, - 1);
				this.lookAtPosition.applyMatrix4(this.rotationMatrix);
				this.lookAtPosition.add(this.cameraWorldPosition);

				var target = this.mirrorWorldPosition.clone().sub(this.lookAtPosition);
				target.reflect(this.normal).negate();
				target.add(this.mirrorWorldPosition);

				this.up.set(0, - 1, 0);
				this.up.applyMatrix4(this.rotationMatrix);
				this.up.reflect(this.normal).negate();

				this.mirrorCamera.position.copy(view);
				this.mirrorCamera.up = this.up;
				this.mirrorCamera.lookAt(target);
				this.mirrorCamera.aspect = this.camera.aspect;

				this.mirrorCamera.updateProjectionMatrix();
				this.mirrorCamera.updateMatrixWorld();

				// Update the texture matrix
				this.textureMatrix.set(
					0.5, 0.0, 0.0, 0.5,
					0.0, 0.5, 0.0, 0.5,
					0.0, 0.0, 0.5, 0.5,
					0.0, 0.0, 0.0, 1.0
				);
				this.textureMatrix.multiply(this.mirrorCamera.projectionMatrix);
				this.textureMatrix.multiply(this.mirrorCamera.matrixWorldInverse);

				// Now update projection matrix with new clip plane, implementing code from: http://www.terathon.com/code/oblique.html
				// Paper explaining this technique: http://www.terathon.com/lengyel/Lengyel-Oblique.pdf
				this.mirrorPlane.setFromNormalAndCoplanarPoint(this.normal, this.mirrorWorldPosition);
				this.mirrorPlane.applyMatrix4(this.mirrorCamera.matrixWorldInverse);

				this.clipPlane.set(this.mirrorPlane.normal.x, this.mirrorPlane.normal.y, this.mirrorPlane.normal.z, this.mirrorPlane.constant);

				var q = new THREE.Vector4();
				var projectionMatrix = this.mirrorCamera.projectionMatrix;

				q.x = (Math.sign(this.clipPlane.x) + projectionMatrix.elements[8]) / projectionMatrix.elements[0];
				q.y = (Math.sign(this.clipPlane.y) + projectionMatrix.elements[9]) / projectionMatrix.elements[5];
				q.z = - 1.0;
				q.w = (1.0 + projectionMatrix.elements[10]) / projectionMatrix.elements[14];

				// Calculate the scaled plane vector
				var c = this.clipPlane.multiplyScalar(2.0 / this.clipPlane.dot(q));

				// Replacing the third row of the projection matrix
				projectionMatrix.elements[2] = c.x;
				projectionMatrix.elements[6] = c.y;
				projectionMatrix.elements[10] = c.z + 1.0 - this.clipBias;
				projectionMatrix.elements[14] = c.w;

				var worldCoordinates = new THREE.Vector3();
				worldCoordinates.setFromMatrixPosition(this.camera.matrixWorld);
				this.eye = worldCoordinates;
				this.material.uniforms.eye.value = this.eye;

			};
		}
	},
	methods: {
		login() {
			this.dialogVisible = false;
			setTimeout(() => {
				this.$router.push('/dashboard')
			}, 100);
		}
	},


}