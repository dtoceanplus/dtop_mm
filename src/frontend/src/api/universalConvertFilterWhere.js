// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

const universalConvertFilterWhere = (filter, map) => {
  for (let prop in filter.where) {
    if (filter.where[prop]) {
      if (map[prop] == "date") {
        let ltDate = new Date(filter.where[prop]);
        ltDate.setUTCDate(ltDate.getUTCDate() + 1);
        ltDate.setTime(
          ltDate.getTime() - ltDate.getTimezoneOffset() * 60000 - 1
        );

        let gteDate = new Date(filter.where[prop]);
        gteDate.setMinutes(gteDate.getMinutes() - gteDate.getTimezoneOffset());

        filter.where[prop] = { between: [gteDate, ltDate] };
      } else if (map[prop] == "like") {
        filter.where[prop] = { like: `%${filter.where[prop]}%` };
      } else if (map[prop] == "eq") {
        filter.where[prop] = { eq: filter.where[prop] };
      }
    } else {
      filter.where[prop] = undefined;
    }
  }
  return filter;
};

export default universalConvertFilterWhere;
