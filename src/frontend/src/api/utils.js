export function humanReadableTitle(title) {
  if (title == "entityId") return "Entity Id";
  else if (title == "entityName") return "Entity Name";
  else if (title == "name") return "Name";
  else if (title == "description") return "Description";
  else if (title == "type") return "Type";
  else if (title == "createdAt") return "Created";
  else if (title == "template") return "Template";
  else if (title == "email") return "Email";
  else if (title == "technology") return "Technology";
  else if (title == "typeOfDevice") return "Type of Device";
  else if (title == "id") return "Id";
  else return title;
}
