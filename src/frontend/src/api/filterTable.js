// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import { merge, isEmpty } from "lodash";

function mergeObjects(obj1, obj2) {
  if (typeof obj2 === "object") {
    merge(obj1, obj2);
  } else {
    return false;
  }
}

class FilterTable {
  offset = 0;
  limit = 10;
  order = "";
  where = new FilterTableWhere();
}

class UsersFilterTable extends FilterTable {
  where = new UsersFilterTableWhere();
  constructor(filterTable) {
    super();
    mergeObjects(this, filterTable);
  }
}

class ProjectsFilterTable extends FilterTable {
  where = new ProjectsFilterTableWhere();
  constructor(filterTable) {
    super();
    mergeObjects(this, filterTable);
  }
}

class StudiesFilterTable extends FilterTable {
  where = new StudiesFilterTableWhere();
  constructor(filterTable) {
    super();
    mergeObjects(this, filterTable);
  }
}

class EventsFilterTable extends FilterTable {
  where = new EventsFilterTableWhere();
  constructor(filterTable) {
    super();
    mergeObjects(this, filterTable);
  }
}


class FilterTableWhere {
  id = undefined;
  createdAt = undefined;
}

class UsersFilterTableWhere extends FilterTableWhere {
  name = undefined;
  email = undefined;
  constructor(filterTableWhere) {
    super();
    mergeObjects(this, filterTableWhere);
  }
}

class ProjectsFilterTableWhere extends FilterTableWhere {
  name = undefined;
  technology = undefined;
  typeOfDevice = undefined;
  description = undefined;

  constructor(filterTableWhere) {
    super();
    mergeObjects(this, filterTableWhere);
  }
}

class StudiesFilterTableWhere extends FilterTableWhere {
  name = undefined;
  description = undefined;

  constructor(filterTableWhere) {
    super();
    mergeObjects(this, filterTableWhere);
  }
}

class EventsFilterTableWhere extends FilterTableWhere {
  name = undefined;
  description = undefined;

  constructor(filterTableWhere) {
    super();
    mergeObjects(this, filterTableWhere);
  }
}

export class UsersFilterTableFabric {
  createFilterTable(filterTable) {
    return new UsersFilterTable(filterTable);
  }
  createFilterTableFromQuery(queryObj) {
    if (!isEmpty(queryObj)) {
      const offset = (+queryObj.currPage - 1) * queryObj.limit;
      queryObj.limit = +queryObj.limit;
      delete queryObj.currPage;
      return this.createFilterTable({ ...queryObj, offset: offset });
    } else {
      return this.createFilterTable();
    }
  }
}

export class ProjectsFilterTableFabric {
  createFilterTable(filterTable) {
    return new ProjectsFilterTable(filterTable);
  }
  createFilterTableFromQuery(queryObj) {
    if (!isEmpty(queryObj)) {
      const offset = (+queryObj.currPage - 1) * queryObj.limit;
      queryObj.limit = +queryObj.limit;
      delete queryObj.currPage;
      return this.createFilterTable({ ...queryObj, offset: offset });
    } else {
      return this.createFilterTable();
    }
  }
}

export class StudiesFilterTableFabric {
  createFilterTable(filterTable) {
    return new StudiesFilterTable(filterTable);
  }
  createFilterTableFromQuery(queryObj) {
    if (!isEmpty(queryObj)) {
      const offset = (+queryObj.currPage - 1) * queryObj.limit;
      queryObj.limit = +queryObj.limit;
      delete queryObj.currPage;
      return this.createFilterTable({ ...queryObj, offset: offset });
    } else {
      return this.createFilterTable();
    }
  }
}

export class EventsFilterTableFabric {
  createFilterTable(filterTable) {
    return new EventsFilterTable(filterTable);
  }
  createFilterTableFromQuery(queryObj) {
    if (!isEmpty(queryObj)) {
      const offset = (+queryObj.currPage - 1) * queryObj.limit;
      queryObj.limit = +queryObj.limit;
      delete queryObj.currPage;
      return this.createFilterTable({ ...queryObj, offset: offset });
    } else {
      return this.createFilterTable();
    }
  }
}

export class UsersFilterTableWhereFabric {
  createFilterTableWhere(filterTableWhere) {
    return new UsersFilterTableWhere(filterTableWhere);
  }
  createFilterTableWhereFromQuery(queryWhereObj) {
    return this.createFilterTableWhere(queryWhereObj);
  }
}

export class ProjectsFilterTableWhereFabric {
  createFilterTableWhere(filterTableWhere) {
    return new ProjectsFilterTableWhere(filterTableWhere);
  }
  createFilterTableWhereFromQuery(queryWhereObj) {
    return this.createFilterTableWhere(queryWhereObj);
  }
}

export class StudiesFilterTableWhereFabric {
  createFilterTableWhere(filterTableWhere) {
    return new StudiesFilterTableWhere(filterTableWhere);
  }
  createFilterTableWhereFromQuery(queryWhereObj) {
    return this.createFilterTableWhere(queryWhereObj);
  }
}

export class EventsFilterTableWhereFabric {
  createFilterTableWhere(filterTableWhere) {
    return new EventsFilterTableWhere(filterTableWhere);
  }
  createFilterTableWhereFromQuery(queryWhereObj) {
    return this.createFilterTableWhere(queryWhereObj);
  }
}
