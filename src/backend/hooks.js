// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
const hooks = require('hooks')

const after = hooks.after
const before = hooks.before

before("DigitalrepController > /api/digitalreps > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("DigitalrepController > /api/digitalreps > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("DigitalrepController > /api/digitalreps > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("DigitalrepController > /api/digitalreps/count > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("DigitalrepController > /api/digitalreps/{id} > DELETE > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("DigitalrepController > /api/digitalreps/{id} > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("DigitalrepController > /api/digitalreps/{id} > PATCH > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("DigitalrepController > /api/digitalreps/{id} > PUT > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("EventController > /api/events > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("EventController > /api/events > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("EventController > /api/events > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("EventController > /api/events/count > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("EventController > /api/events/{id} > DELETE > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("EventController > /api/events/{id} > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("EventController > /api/events/{id} > PATCH > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("EventController > /api/events/{id} > PUT > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("EventUserMapController > /api/events-users-map > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("EventUserMapController > /api/events-users-map > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("EventUserMapController > /api/events-users-map > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("EventUserMapController > /api/events-users-map/count > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("EventUserMapController > /api/events-users-map/{id} > DELETE > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("EventUserMapController > /api/events-users-map/{id} > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("EventUserMapController > /api/events-users-map/{id} > PATCH > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("EventUserMapController > /api/events-users-map/{id} > PUT > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("IntegrationController > /api/integration/modules/{mname}/entities/{meid}/projects > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("IntegrationController > /api/integration/modules/{mname}/entities/{meid}/projects > GET > 500 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("IntegrationController > /api/integration/projects > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("IntegrationController > /api/integration/projects/{id} > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("IntegrationController > /api/integration/projects/{id}/studies > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});
before("IntegrationController > /api/integration/projects/{pid}/modules/{mname}/entities > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("IntegrationController > /api/integration/projects/{pid}/modules/{mname}/entities > GET > 500 > application/json", function (transaction) {
  transaction.skip = true;
});

before("IntegrationController > /api/integration/projects/{pid}/modules/{mname}/entities/{meid}/studies > GET > 200 > application/json", function (transaction) { 
//  transaction.skip = true;
});

before("IntegrationController > /api/integration/projects/{pid}/modules/{mname}/entities/{meid}/studies > GET > 500 > application/json", function (transaction) { 
  transaction.skip = true;
});

before("IntegrationController > /api/integration/projects/{pid}/studies/{sid}/entities > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("IntegrationController > /api/integration/projects/{pid}/studies/{sid}/entities > GET > 404 > application/json", function (transaction) {
  transaction.skip = true;
});

before("IntegrationController > /api/integration/projects/{pid}/studies/{sid}/entities > GET > 500 > application/json", function (transaction) {
  transaction.skip = true;
});

before("IntegrationController > /api/integration/projects/{pid}/studies/{sid}/modules > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("IntegrationController > /api/integration/projects/{pid}/studies/{sid}/modules > GET > 404 > application/json", function (transaction) {
  transaction.skip = true;
});

before("IntegrationController > /api/integration/projects/{pid}/studies/{sid}/modules > GET > 500 > application/json", function (transaction) {
  transaction.skip = true;
});

before("IntegrationController > /api/integration/projects/{pid}/studies/{sid}/modules/{mname}/entities > GET > 200 > application/json", function (transaction) {  
//  transaction.skip = true;
});

before("IntegrationController > /api/integration/projects/{pid}/studies/{sid}/modules/{mname}/entities > GET > 404 > application/json", function (transaction) {  
  transaction.skip = true;
});

before("IntegrationController > /api/integration/projects/{pid}/studies/{sid}/modules/{mname}/entities > GET > 500 > application/json", function (transaction) {  
  transaction.skip = true;
});

before("IntegrationController > /api/integration/studies > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("IntegrationController > /api/integration/studies/{id} > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("IntegrationController > /api/integration/studies/{id}/modules > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("ModuleController > /api/modules > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("ModuleController > /api/modules > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("ModuleController > /api/modules > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("ModuleController > /api/modules/count > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("ModuleController > /api/modules/{id} > DELETE > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("ModuleController > /api/modules/{id} > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("ModuleController > /api/modules/{id} > PATCH > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("ModuleController > /api/modules/{id} > PUT > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("PingController > /api/ping > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("ProjectController > /api/projects > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("ProjectController > /api/projects > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("ProjectController > /api/projects > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("ProjectController > /api/projects/count > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("ProjectController > /api/projects/{id} > DELETE > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("ProjectController > /api/projects/{id} > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("ProjectController > /api/projects/{id} > PATCH > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("ProjectController > /api/projects/{id} > PUT > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("ProjectUserMapController > /api/projects-users-map > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("ProjectUserMapController > /api/projects-users-map > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("ProjectUserMapController > /api/projects-users-map > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("ProjectUserMapController > /api/projects-users-map/count > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("ProjectUserMapController > /api/projects-users-map/{id} > DELETE > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("ProjectUserMapController > /api/projects-users-map/{id} > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("ProjectUserMapController > /api/projects-users-map/{id} > PATCH > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("ProjectUserMapController > /api/projects-users-map/{id} > PUT > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("ProjectStudyController > /api/projects/{id}/studies > DELETE > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("ProjectStudyController > /api/projects/{id}/studies > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("ProjectStudyController > /api/projects/{id}/studies > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("ProjectStudyController > /api/projects/{id}/studies > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("RoleController > /api/roles > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("RoleController > /api/roles > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("RoleController > /api/roles > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("RoleController > /api/roles/count > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("RoleController > /api/roles/{id} > DELETE > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("RoleController > /api/roles/{id} > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("RoleController > /api/roles/{id} > PATCH > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("RoleController > /api/roles/{id} > PUT > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("RoleUserMapController > /api/roles-users-map > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("RoleUserMapController > /api/roles-users-map > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("RoleUserMapController > /api/roles-users-map > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("RoleUserMapController > /api/roles-users-map/count > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("RoleUserMapController > /api/roles-users-map/{id} > DELETE > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("RoleUserMapController > /api/roles-users-map/{id} > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("RoleUserMapController > /api/roles-users-map/{id} > PATCH > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("RoleUserMapController > /api/roles-users-map/{id} > PUT > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("RoleUserController > /api/roles/{id}/users > DELETE > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("RoleUserController > /api/roles/{id}/users > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("RoleUserController > /api/roles/{id}/users > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("RoleUserController > /api/roles/{id}/users > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyController > /api/studies > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("StudyController > /api/studies > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyController > /api/studies > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyController > /api/studies/count > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("StudyController > /api/studies/{id} > DELETE > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyController > /api/studies/{id} > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("StudyController > /api/studies/{id} > PATCH > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyController > /api/studies/{id} > PUT > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyModuleEntityController > /api/studies-modules-entities > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("StudyModuleEntityController > /api/studies-modules-entities > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyModuleEntityController > /api/studies-modules-entities > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyModuleEntityController > /api/studies-modules-entities/count > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("StudyModuleEntityController > /api/studies-modules-entities/{id} > DELETE > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyModuleEntityController > /api/studies-modules-entities/{id} > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("StudyModuleEntityController > /api/studies-modules-entities/{id} > PATCH > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyModuleEntityController > /api/studies-modules-entities/{id} > PUT > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyUserMapController > /api/studies-users-map > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("StudyUserMapController > /api/studies-users-map > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyUserMapController > /api/studies-users-map > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyUserMapController > /api/studies-users-map/count > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("StudyUserMapController > /api/studies-users-map/{id} > DELETE > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyUserMapController > /api/studies-users-map/{id} > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("StudyUserMapController > /api/studies-users-map/{id} > PATCH > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyUserMapController > /api/studies-users-map/{id} > PUT > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyDigitalrepController > /api/studies/{id}/digitalrep > DELETE > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyDigitalrepController > /api/studies/{id}/digitalrep > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("StudyDigitalrepController > /api/studies/{id}/digitalrep > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyDigitalrepController > /api/studies/{id}/digitalrep > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyModuleController > /api/studies/{id}/modules > DELETE > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyModuleController > /api/studies/{id}/modules > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("StudyModuleController > /api/studies/{id}/modules > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("StudyModuleController > /api/studies/{id}/modules > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("UserController > /api/users > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("UserController > /api/users > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("UserController > /api/users > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("UserController > /api/users/count > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("UserController > /api/users/{id} > DELETE > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("UserController > /api/users/{id} > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("UserController > /api/users/{id} > PATCH > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("UserController > /api/users/{id} > PUT > 204 > application/json", function (transaction) {
  transaction.skip = true;
});

before("UserEventController > /api/users/{id}/events > DELETE > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("UserEventController > /api/users/{id}/events > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("UserEventController > /api/users/{id}/events > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("UserEventController > /api/users/{id}/events > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("UserProjectController > /api/users/{id}/projects > DELETE > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("UserProjectController > /api/users/{id}/projects > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("UserProjectController > /api/users/{id}/projects > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("UserProjectController > /api/users/{id}/projects > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("UserStudyController > /api/users/{id}/studies > DELETE > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("UserStudyController > /api/users/{id}/studies > GET > 200 > application/json", function (transaction) {
//  transaction.skip = true;
});

before("UserStudyController > /api/users/{id}/studies > PATCH > 200 > application/json", function (transaction) {
  transaction.skip = true;
});

before("UserStudyController > /api/users/{id}/studies > POST > 200 > application/json", function (transaction) {
  transaction.skip = true;
});
