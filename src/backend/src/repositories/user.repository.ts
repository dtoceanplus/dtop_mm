// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import {DefaultCrudRepository, repository, HasManyThroughRepositoryFactory} from '@loopback/repository';
import {User, UserRelations, Event, EventUser, Project, ProjectUser, Study, StudyUser} from '../models';
import {MainDbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {EventUserRepository} from './event-user.repository';
import {EventRepository} from './event.repository';
import {ProjectUserRepository} from './project-user.repository';
import {ProjectRepository} from './project.repository';
import {StudyUserRepository} from './study-user.repository';
import {StudyRepository} from './study.repository';

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id,
  UserRelations
> {

  public readonly events: HasManyThroughRepositoryFactory<Event, typeof Event.prototype.id,
          EventUser,
          typeof User.prototype.id
        >;

  public readonly projects: HasManyThroughRepositoryFactory<Project, typeof Project.prototype.id,
          ProjectUser,
          typeof User.prototype.id
        >;

  public readonly studies: HasManyThroughRepositoryFactory<Study, typeof Study.prototype.id,
          StudyUser,
          typeof User.prototype.id
        >;

  constructor(
    @inject('datasources.main_db') dataSource: MainDbDataSource, @repository.getter('EventUserRepository') protected eventUserRepositoryGetter: Getter<EventUserRepository>, @repository.getter('EventRepository') protected eventRepositoryGetter: Getter<EventRepository>, @repository.getter('ProjectUserRepository') protected projectUserRepositoryGetter: Getter<ProjectUserRepository>, @repository.getter('ProjectRepository') protected projectRepositoryGetter: Getter<ProjectRepository>, @repository.getter('StudyUserRepository') protected studyUserRepositoryGetter: Getter<StudyUserRepository>, @repository.getter('StudyRepository') protected studyRepositoryGetter: Getter<StudyRepository>,
  ) {
    super(User, dataSource);
    this.studies = this.createHasManyThroughRepositoryFactoryFor('studies', studyRepositoryGetter, studyUserRepositoryGetter,);
    this.registerInclusionResolver('studies', this.studies.inclusionResolver);
    this.projects = this.createHasManyThroughRepositoryFactoryFor('projects', projectRepositoryGetter, projectUserRepositoryGetter,);
    this.registerInclusionResolver('projects', this.projects.inclusionResolver);
    this.events = this.createHasManyThroughRepositoryFactoryFor('events', eventRepositoryGetter, eventUserRepositoryGetter,);
    this.registerInclusionResolver('events', this.events.inclusionResolver);
  }
}
