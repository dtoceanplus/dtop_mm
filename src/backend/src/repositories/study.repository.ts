// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import {DefaultCrudRepository, repository, HasOneRepositoryFactory, HasManyThroughRepositoryFactory} from '@loopback/repository';
import {Study, StudyRelations, Digitalrep, Module, StudyModule} from '../models';
import {MainDbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {DigitalrepRepository} from './digitalrep.repository';
import {StudyModuleRepository} from './study-module.repository';
import {ModuleRepository} from './module.repository';

export class StudyRepository extends DefaultCrudRepository<
  Study,
  typeof Study.prototype.id,
  StudyRelations
> {

  public readonly digitalrep: HasOneRepositoryFactory<Digitalrep, typeof Study.prototype.id>;

  public readonly modules: HasManyThroughRepositoryFactory<Module, typeof Module.prototype.id,
          StudyModule,
          typeof Study.prototype.id
        >;

  constructor(
    @inject('datasources.main_db') dataSource: MainDbDataSource, @repository.getter('DigitalrepRepository') protected digitalrepRepositoryGetter: Getter<DigitalrepRepository>, @repository.getter('StudyModuleRepository') protected studyModuleRepositoryGetter: Getter<StudyModuleRepository>, @repository.getter('ModuleRepository') protected moduleRepositoryGetter: Getter<ModuleRepository>,
  ) {
    super(Study, dataSource);
    this.modules = this.createHasManyThroughRepositoryFactoryFor('modules', moduleRepositoryGetter, studyModuleRepositoryGetter,);
    this.registerInclusionResolver('modules', this.modules.inclusionResolver);
    this.digitalrep = this.createHasOneRepositoryFactoryFor('digitalrep', digitalrepRepositoryGetter);
    this.registerInclusionResolver('digitalrep', this.digitalrep.inclusionResolver);
  }
}
