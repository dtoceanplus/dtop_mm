// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import {
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';

import {
  param,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  requestBody,
  HttpErrors
} from '@loopback/rest';

import {
  Project,
  Study,
  Module,
  StudyModule,
} from '../models';

import {
  ProjectRepository,
  StudyRepository,
  ModuleRepository,
  StudyModuleRepository,
} from '../repositories';

//export class EntityMap {
export type EntityMap = {
  projectId: number;
  studyId: number;
  moduleId: number;
  entityId: number;
  entityName: string;
}
// entityStatus: string;

export class IntegrationController {
  constructor(
    @repository(ProjectRepository)
    public projectRepository : ProjectRepository,
    @repository(StudyRepository)
    public studyRepository : StudyRepository,
    @repository(ModuleRepository)
    public moduleRepository : ModuleRepository,
    @repository(StudyModuleRepository)
    public studyModuleRepository : StudyModuleRepository,
  ) {}


  @get('/integration/projects', {
    responses: {
      '200': {
        description: 'Array of Project model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Project, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findProjects(
    @param.filter(Project) filter?: Filter<Project>,
  ): Promise<Project[]> {
    return this.projectRepository.find(filter);
  }


  @get('/integration/projects/{id}', {
    responses: {
      '200': {
        description: 'Project model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Project, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findProjectById(
    @param.path.number('id') id: number,
    @param.filter(Project, {exclude: 'where'}) filter?: FilterExcludingWhere<Project>
  ): Promise<Project> {
    return this.projectRepository.findById(id, filter);
  }


  @get('/integration/studies', {
    responses: {
      '200': {
        description: 'Array of Study model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Study, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findStudies(
    @param.filter(Study) filter?: Filter<Study>,
  ): Promise<Study[]> {
    return this.studyRepository.find(filter);
  }


  @get('/integration/studies/{id}', {
    responses: {
      '200': {
        description: 'Study model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Study, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findStudyById(
    @param.path.number('id') id: number,
    @param.filter(Study, {exclude: 'where'}) filter?: FilterExcludingWhere<Study>
  ): Promise<Study> {
    return this.studyRepository.findById(id, filter);
  }


  @get('/integration/studies/{id}/modules', {
    responses: {
      '200': {
        description: 'Array of Study has many Module through StudyModule',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Module)},
          },
        },
      },
    },
  })
  async findStudyModules(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Module>,
  ): Promise<Module[]> {
    return this.studyRepository.modules(id).find(filter);
  }


  @get('/integration/projects/{id}/studies', {
    responses: {
      '200': {
        description: 'Array of Project has many Study',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Study)},
          },
        },
      },
    },
  })
  async findProjectStudies(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Study>,
  ): Promise<Study[]> {
    return this.projectRepository.studies(id).find(filter);
  }


  @get('/integration/modules/{mname}/entities/{meid}/projects', {
    responses: {
      '200': {
        description: 'Array of the Projects for Module with module nickname = {mname} and registered Modules Entities Id = {meid}',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Project),
            },
          },
        },
      },
      '500': {
        description: 'Module mname (nickname) is incorrect',
      },
    },
  })
  async findAllModuleModuleEntityProjects(
    @param.path.string('mname') mname: string,
    @param.path.number('meid') meid: number,
  ): Promise<Project[]> {

    let mid = await this.moduleRepository.find({
      "where": {
        "nickname": mname,
      },
      "fields": {
        "id": true, // only 1 field
      }
    });

    //  console.log('mid == ' + JSON.stringify(mid));    // mid == [{"id":1}]
    //  console.log('');

    let sm_res = await this.studyModuleRepository.find({
      "where": {
        "moduleId": mid[0].id,
        "entityId": meid,
      },
      "fields": {
        "studyId": true,
      }
    });

    // console.log('sm_res == ' + JSON.stringify(sm_res));
    // console.log('');

    let smap = sm_res.map(element => element.studyId);
    // console.log('smap == ' + JSON.stringify(smap));
    // console.log('');

    let s_res = await this.studyRepository.find({
      "where": {
        "id": {"inq": smap},
      },
      "fields": {
        "projectId": true,
      }
    });

    // console.log('s_res == ' + JSON.stringify(s_res));
    // console.log('');

    let pmap = s_res.map(element => element.projectId);
    // console.log('pmap == ' + JSON.stringify(pmap));
    // console.log('');

    let projects: Project[] = await this.projectRepository.find({
      "where": {
        "id": {"inq": pmap},
      },
      "fields": {
        "id": true,
        "name": true,
      }
    });

    console.log('projects == ' + JSON.stringify(projects));
    console.log('');

    // let projects: Project[] = result[0]['studies'][0]['modules'];
    // let projects: Project[] = result;

    return projects
  }


  @get('/integration/projects/{pid}/modules/{mname}/entities/{meid}/studies', {
    responses: {
      '200': {
        description: 'Array of the Studies in the Project with project Id = {pid} for Module with module nickname = {mname} and registered Modules Entities Id = {meid}',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Study),
            },
          },
        },
      },
      '500': {
        description: 'Project with pid does not exist or Module mname (nickname) is incorrect',
      },
    },
  })
  async findAllProjectModuleEntityModules(
    @param.path.number('pid') pid: number,
    @param.path.string('mname') mname: string,
    @param.path.number('meid') meid: number,
  ): Promise<Study[]> {

    let res = await this.projectRepository.find({
      "where": {
        "id": pid
      },
      "include": [
        {
          "relation": "studies",
        }
      ]
    });

    // console.log(' !res[0][studies] == ' + !res[0]['studies']);
    // console.log(' res[0][studies] === null ' + (res[0]['studies'] === null));

    let smap = res[0]['studies'].map(element => element.id);
    // console.log('smap == ' + JSON.stringify(smap));
    // console.log('');

    let sm_res = await this.studyModuleRepository.find({
      "where": {
        "studyId": {"inq": smap},
        "entityId": meid,
      },
      "fields": {
        "studyId": true,
      }
    });

    // console.log('sm_res == ' + JSON.stringify(sm_res));
    // console.log('');

    let smap1 = sm_res.map(element => element.studyId);
    // console.log('smap1 == ' + JSON.stringify(smap1));
    // console.log('');

    // let result = await this.studyRepository.find({
    let studies: Study[] = await this.studyRepository.find({
      "where": {
        "id": {"inq": smap1},
      },
      "fields": {
        "id": true,
        "name": true,
        "projectId": true,
      }
    });

    // console.log('studies == ' + JSON.stringify(studies));
    // console.log('');

    return studies
  }


  @get('/integration/projects/{pid}/studies/{sid}/modules', {
    responses: {
      '200': {
        description: 'Array of the all Modules nicknames registered in the Project with project Id = {pid} and Study with study Id = {sid} ',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Module),
            },
          },
        },
      },
      '404': {
        description: 'Study with sid does not belong to project with pid',
      },
      '500': {
        description: 'Project with pid does not exist',
      },
    },
  })
  async findAllProjectStudyModules(
    @param.path.number('pid') pid: number,
    @param.path.number('sid') sid: number,
  ): Promise<Module[]> {

    let result = await this.projectRepository.find({
       "where": {
         "id": pid
       },
       "include": [
         {
           "relation": "studies",
           "scope": {
             "where": {
               "id": sid
             },
             "include": [
               {
                 "relation": "modules",
                 scope: {
                  fields: ['nickname'], // only one field
                 }
               }
             ]
           }
         }
       ]
    });

    if (
      !result[0]['studies']
    ) {
      throw new HttpErrors.NotFound(`Study ${sid} does not belong to project ${pid}`);
    }

    let modules: Module[] = result[0]['studies'][0]['modules'];

    return modules
  }


  @get('/integration/projects/{pid}/studies/{sid}/entities', {
    responses: {
      '200': {
        description: 'Array of the all Modules Entities registered in the Project with project Id = {pid} and Study with study Id = {sid} ',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(StudyModule),
            },
          },
        },
      },
      '404': {
        description: 'Study with sid does not belong to project with pid',
      },
      '500': {
        description: 'Project with pid does not exist',
      },
    },
  })
  async findAllProjectStudyModuleEntities(
    @param.path.number('pid') pid: number,
    @param.path.number('sid') sid: number,
  ): Promise<StudyModule[]> {

    let res = await this.projectRepository.find({
      "where": {
        "id": pid
      },
      "include": [
        {
          "relation": "studies",
          "scope": {
            "where": {
              "id": sid
            }
          }
        }
      ]
    });

    if (
      !res[0]['studies']
    ) {
      throw new HttpErrors.NotFound(`Study ${sid} does not belong to project ${pid}`);
    }

    // console.log('res[0][studies][0] == ' + JSON.stringify(res[0]['studies'][0]));
    // console.log('');

    let result = await this.studyModuleRepository.find({
      "where": {
        "studyId": sid,
      },
      "fields": {
        "studyId": true,
        "moduleId": true,
        "entityId": true,
        "entityName": true,
        "entityStatus": true,
      }
    });

    // console.log('result == ' + JSON.stringify(result));
    // console.log('');

    return result
  }


  @get('/integration/projects/{pid}/modules/{mname}/entities', {
    responses: {
      '200': {
        description: 'Array of the all Modules Entities registered in the Project with project Id = {pid} for Module with module nickname = {mname}',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(StudyModule),
            },
          },
        },
      },
      '500': {
        description: 'Project with pid does not exist or Module mname (nickname) is incorrect',
      },
    },
  })
  async findAllProjectModuleEntities(
    @param.path.number('pid') pid: number,
    @param.path.string('mname') mname: string,
  ): Promise<StudyModule[]> {

    let res = await this.projectRepository.find({
      "where": {
        "id": pid
      },
      "include": [
        {
          "relation": "studies",
        }
      ]
    });

    // console.log('res[0][studies] == ' + JSON.stringify(res[0]['studies']));
    // console.log('');

    let smap = res[0]['studies'].map(element => element.id);
    // console.log('smap == ' + JSON.stringify(smap));
    // console.log('');

    let mid = await this.moduleRepository.find({
      "where": {
        "nickname": mname,
      },
      "fields": {
        "id": true, // only 1 field
      }
    });

    let result = await this.studyModuleRepository.find({
      "where": {
        "studyId": {"inq": smap},
        "moduleId": mid[0].id,
      },
      "fields": {
        "studyId": true,
        "moduleId": true,
        "entityId": true,
        "entityName": true,
        "entityStatus": true,
      }
    });

    // console.log('result == ' + JSON.stringify(result));
    // console.log('');

    return result
  }


  @get('/integration/projects/{pid}/studies/{sid}/modules/{mname}/entities', {
    responses: {
      '200': {
        description: 'Array of the all Modules Entities registered in the Project with project Id = {pid} and Study with study Id = {sid} for Module with module nickname = {mname}',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(StudyModule),
            },
          },
        },
      },
      '404': {
        description: 'Study with sid does not belong to project with pid',
      },
      '500': {
        description: 'Project with pid does not exist or Module mname (nickname) is incorrect',
      },
    },
  })
  async findAllProjectStudyModuleModuleEntities(
    @param.path.number('pid') pid: number,
    @param.path.number('sid') sid: number,
    @param.path.string('mname') mname: string,
  ): Promise<StudyModule[]> {

    let res = await this.projectRepository.find({
      "where": {
        "id": pid
      },
      "include": [
        {
          "relation": "studies",
          "scope": {
            "where": {
              "id": sid
            }
          }
        }
      ]
    });

    if (
      !res[0]['studies']
    ) {
      throw new HttpErrors.NotFound(`Study ${sid} does not belong to project ${pid}`);
    }

    // console.log('res[0][studies][0] == ' + JSON.stringify(res[0]['studies'][0]));
    // console.log('');

    let mid = await this.moduleRepository.find({
      "where": {
        "nickname": mname,
      },
      "fields": {
        "id": true, // only 1 field
      }
    });

    let result = await this.studyModuleRepository.find({
      "where": {
        "studyId": sid,
        "moduleId": mid[0].id,
      },
      "fields": {
        "studyId": true,
        "moduleId": true,
        "entityId": true,
        "entityName": true,
        "entityStatus": true,
      }
    });

    // console.log('result == ' + JSON.stringify(result));
    // console.log('');

    return result
  }


  @get('/integration/modules/{mname}/entities/{meid}', {
    responses: {
      '200': {
        description: 'Array of the registered Entities of all modules (projectId, studyId, moduleId, entityId, entityName) for Module with module nickname = {mname} and registered Modules Entities Id = {meid}',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  projectId: {type: 'number'},
                  studyId: {type: 'number'},
                  moduleId: {type: 'number'},
                  entityId: {type: 'number'},
                  entityName: {type: 'string'},
                },
              },
            },
          },
        },
      },
      '500': {
        description: 'Module mname (nickname) is incorrect',
      },
    },
  })
  async findAllModuleModuleEntities(
    @param.path.string('mname') mname: string,
    @param.path.number('meid') meid: number,
  ): Promise<EntityMap[]> {

    let mid = await this.moduleRepository.find({
      "where": {
        "nickname": mname,
      },
      "fields": {
        "id": true, // only 1 field
      }
    });

    //  console.log('mid == ' + JSON.stringify(mid));    // mid == [{"id":2}]
    //  console.log('');

    let sidmap = await this.studyModuleRepository.find({
      "where": {
        "moduleId": mid[0].id,
        "entityId": meid,
      },
      "fields": {
        "studyId": true,
      }
    });

    // console.log('sidmap == ' + JSON.stringify(sidmap));  // sidmap == [{"studyId":2},{"studyId":4}]
    // console.log('sidmap[0].studyId == ' + JSON.stringify(sidmap[0].studyId));  // sidmap[0].studyId == 2
    // console.log('');

    let smap = sidmap.map(element => element.studyId);
    // console.log('smap == ' + JSON.stringify(smap));   // smap == [2,2]
    // console.log('');

    let sm_res = await this.studyModuleRepository.find({
      "where": {
        "studyId": {"inq": smap},
      },
      "fields": {
        "studyId": true,
        "moduleId": true,
        "entityId": true,
        "entityName": true,
      }
    });

    // console.log('sm_res == ' + JSON.stringify(sm_res));
    // console.log('');
    /// sm_res == [{"studyId":2,"moduleId":2,"entityId":2222,"entityName":"entity name for study 2 for module 2 (SG) and entity id  2222","entityStatus":"Finished"},{"studyId":2,"moduleId":11,"entityId":3333,"entityName":"entity name for study 2 for module 11 (RAMS) and entity id  3333","entityStatus":"Finished"}]

    // let smap = sm_res.map(element => element.studyId);
    // console.log('smap == ' + JSON.stringify(smap));   // smap == [2,2]
    // console.log('');

    let s_res = await this.studyRepository.find({
      "where": {
        "id": {"inq": smap},
      },
      "fields": {
        "id": true,
        "projectId": true,
      }
    });

    // console.log('s_res == ' + JSON.stringify(s_res));   // s_res == [{"projectId":1}]
    // console.log('');

    let pid_sid = s_res.map((element) => {
      return {
        projectId: element.projectId,
        studyId: element.id,
      };
    });

    // console.log('pid_sid == ' + JSON.stringify(pid_sid));   // pid_sid == [{"projectId":1,"studyId":2}]
    // console.log('');

    let entitymap = sm_res.map((item, index) => {
      let entitymap_item: any = {};

      // console.log('index == ' + index);
      // console.log('item.studyId == ' + JSON.stringify(item.studyId));

      let obj = pid_sid.find(o => o.studyId === item.studyId);

      // console.log('obj == ' + JSON.stringify(obj));
      // console.log('projectId == ' + JSON.stringify(obj?.projectId));
      // console.log('studyId == ' + JSON.stringify(obj?.studyId));

      entitymap_item.projectId = obj?.projectId
      entitymap_item.studyId = item.studyId;
      entitymap_item.moduleId = item.moduleId;
      entitymap_item.entityId = item.entityId;
      entitymap_item.entityName = item.entityName;

      return entitymap_item;
    })

    // console.log(entitymap);
    // console.log('entitymap == ' + JSON.stringify(entitymap));

    return entitymap
  }

}
