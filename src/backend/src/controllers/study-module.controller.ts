// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
  import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
Study,
StudyModule,
Module,
} from '../models';
import {StudyRepository} from '../repositories';

export class StudyModuleController {
  constructor(
    @repository(StudyRepository) protected studyRepository: StudyRepository,
  ) { }

  @get('/studies/{id}/modules', {
    responses: {
      '200': {
        description: 'Array of Study has many Module through StudyModule',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Module)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Module>,
  ): Promise<Module[]> {
    return this.studyRepository.modules(id).find(filter);
  }

  @post('/studies/{id}/modules', {
    responses: {
      '200': {
        description: 'create a Module model instance',
        content: {'application/json': {schema: getModelSchemaRef(Module)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Study.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Module, {
            title: 'NewModuleInStudy',
            exclude: ['id'],
          }),
        },
      },
    }) module: Omit<Module, 'id'>,
  ): Promise<Module> {
    return this.studyRepository.modules(id).create(module);
  }

  @patch('/studies/{id}/modules', {
    responses: {
      '200': {
        description: 'Study.Module PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Module, {partial: true}),
        },
      },
    })
    module: Partial<Module>,
    @param.query.object('where', getWhereSchemaFor(Module)) where?: Where<Module>,
  ): Promise<Count> {
    return this.studyRepository.modules(id).patch(module, where);
  }

  @del('/studies/{id}/modules', {
    responses: {
      '200': {
        description: 'Study.Module DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Module)) where?: Where<Module>,
  ): Promise<Count> {
    return this.studyRepository.modules(id).delete(where);
  }
}
