// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Study} from '../models';
import {StudyRepository} from '../repositories';

export class StudyController {
  constructor(
    @repository(StudyRepository)
    public studyRepository : StudyRepository,
  ) {}

  @post('/studies', {
    responses: {
      '200': {
        description: 'Study model instance',
        content: {'application/json': {schema: getModelSchemaRef(Study)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Study, {
            title: 'NewStudy',
            exclude: ['id'],
          }),
        },
      },
    })
    study: Omit<Study, 'id'>,
  ): Promise<Study> {
    return this.studyRepository.create(study);
  }

  @get('/studies/count', {
    responses: {
      '200': {
        description: 'Study model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Study) where?: Where<Study>,
  ): Promise<Count> {
    return this.studyRepository.count(where);
  }

  @get('/studies', {
    responses: {
      '200': {
        description: 'Array of Study model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Study, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Study) filter?: Filter<Study>,
  ): Promise<Study[]> {
    return this.studyRepository.find(filter);
  }

  @patch('/studies', {
    responses: {
      '200': {
        description: 'Study PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Study, {partial: true}),
        },
      },
    })
    study: Study,
    @param.where(Study) where?: Where<Study>,
  ): Promise<Count> {
    return this.studyRepository.updateAll(study, where);
  }

  @get('/studies/{id}', {
    responses: {
      '200': {
        description: 'Study model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Study, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Study, {exclude: 'where'}) filter?: FilterExcludingWhere<Study>
  ): Promise<Study> {
    return this.studyRepository.findById(id, filter);
  }

  @patch('/studies/{id}', {
    responses: {
      '204': {
        description: 'Study PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Study, {partial: true}),
        },
      },
    })
    study: Study,
  ): Promise<void> {
    await this.studyRepository.updateById(id, study);
  }

  @put('/studies/{id}', {
    responses: {
      '204': {
        description: 'Study PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() study: Study,
  ): Promise<void> {
    await this.studyRepository.replaceById(id, study);
  }

  @del('/studies/{id}', {
    responses: {
      '204': {
        description: 'Study DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.studyRepository.deleteById(id);
  }
}
