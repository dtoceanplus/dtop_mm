// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {StudyModule} from '../models';
import {StudyModuleRepository} from '../repositories';

export class StudyModuleEntityController {
  constructor(
    @repository(StudyModuleRepository)
    public studyModuleRepository : StudyModuleRepository,
  ) {}

  @post('/studies-modules-entities', {
    responses: {
      '200': {
        description: 'StudyModule model instance',
        content: {'application/json': {schema: getModelSchemaRef(StudyModule)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudyModule, {
            title: 'NewStudyModule',
            exclude: ['id'],
          }),
        },
      },
    })
    studyModule: Omit<StudyModule, 'id'>,
  ): Promise<StudyModule> {
    return this.studyModuleRepository.create(studyModule);
  }

  @get('/studies-modules-entities/count', {
    responses: {
      '200': {
        description: 'StudyModule model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(StudyModule) where?: Where<StudyModule>,
  ): Promise<Count> {
    return this.studyModuleRepository.count(where);
  }

  @get('/studies-modules-entities', {
    responses: {
      '200': {
        description: 'Array of StudyModule model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(StudyModule, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(StudyModule) filter?: Filter<StudyModule>,
  ): Promise<StudyModule[]> {
    return this.studyModuleRepository.find(filter);
  }

  @patch('/studies-modules-entities', {
    responses: {
      '200': {
        description: 'StudyModule PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudyModule, {partial: true}),
        },
      },
    })
    studyModule: StudyModule,
    @param.where(StudyModule) where?: Where<StudyModule>,
  ): Promise<Count> {
    return this.studyModuleRepository.updateAll(studyModule, where);
  }

  @get('/studies-modules-entities/{id}', {
    responses: {
      '200': {
        description: 'StudyModule model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(StudyModule, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(StudyModule, {exclude: 'where'}) filter?: FilterExcludingWhere<StudyModule>
  ): Promise<StudyModule> {
    return this.studyModuleRepository.findById(id, filter);
  }

  @patch('/studies-modules-entities/{id}', {
    responses: {
      '204': {
        description: 'StudyModule PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudyModule, {partial: true}),
        },
      },
    })
    studyModule: StudyModule,
  ): Promise<void> {
    await this.studyModuleRepository.updateById(id, studyModule);
  }

  @put('/studies-modules-entities/{id}', {
    responses: {
      '204': {
        description: 'StudyModule PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() studyModule: StudyModule,
  ): Promise<void> {
    await this.studyModuleRepository.replaceById(id, studyModule);
  }

  @del('/studies-modules-entities/{id}', {
    responses: {
      '204': {
        description: 'StudyModule DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.studyModuleRepository.deleteById(id);
  }
}
