// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {ProjectUser} from '../models';
import {ProjectUserRepository} from '../repositories';

export class ProjectUserMapController {
  constructor(
    @repository(ProjectUserRepository)
    public projectUserRepository : ProjectUserRepository,
  ) {}

  @post('/projects-users-map', {
    responses: {
      '200': {
        description: 'ProjectUser model instance',
        content: {'application/json': {schema: getModelSchemaRef(ProjectUser)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProjectUser, {
            title: 'NewProjectUser',
            exclude: ['id'],
          }),
        },
      },
    })
    projectUser: Omit<ProjectUser, 'id'>,
  ): Promise<ProjectUser> {
    return this.projectUserRepository.create(projectUser);
  }

  @get('/projects-users-map/count', {
    responses: {
      '200': {
        description: 'ProjectUser model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(ProjectUser) where?: Where<ProjectUser>,
  ): Promise<Count> {
    return this.projectUserRepository.count(where);
  }

  @get('/projects-users-map', {
    responses: {
      '200': {
        description: 'Array of ProjectUser model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ProjectUser, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(ProjectUser) filter?: Filter<ProjectUser>,
  ): Promise<ProjectUser[]> {
    return this.projectUserRepository.find(filter);
  }

  @patch('/projects-users-map', {
    responses: {
      '200': {
        description: 'ProjectUser PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProjectUser, {partial: true}),
        },
      },
    })
    projectUser: ProjectUser,
    @param.where(ProjectUser) where?: Where<ProjectUser>,
  ): Promise<Count> {
    return this.projectUserRepository.updateAll(projectUser, where);
  }

  @get('/projects-users-map/{id}', {
    responses: {
      '200': {
        description: 'ProjectUser model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ProjectUser, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ProjectUser, {exclude: 'where'}) filter?: FilterExcludingWhere<ProjectUser>
  ): Promise<ProjectUser> {
    return this.projectUserRepository.findById(id, filter);
  }

  @patch('/projects-users-map/{id}', {
    responses: {
      '204': {
        description: 'ProjectUser PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProjectUser, {partial: true}),
        },
      },
    })
    projectUser: ProjectUser,
  ): Promise<void> {
    await this.projectUserRepository.updateById(id, projectUser);
  }

  @put('/projects-users-map/{id}', {
    responses: {
      '204': {
        description: 'ProjectUser PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() projectUser: ProjectUser,
  ): Promise<void> {
    await this.projectUserRepository.replaceById(id, projectUser);
  }

  @del('/projects-users-map/{id}', {
    responses: {
      '204': {
        description: 'ProjectUser DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.projectUserRepository.deleteById(id);
  }
}
