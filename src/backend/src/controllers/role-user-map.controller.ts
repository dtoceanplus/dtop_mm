// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {RoleUser} from '../models';
import {RoleUserRepository} from '../repositories';

export class RoleUserMapController {
  constructor(
    @repository(RoleUserRepository)
    public roleUserRepository : RoleUserRepository,
  ) {}

  @post('/roles-users-map', {
    responses: {
      '200': {
        description: 'RoleUser model instance',
        content: {'application/json': {schema: getModelSchemaRef(RoleUser)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(RoleUser, {
            title: 'NewRoleUser',
            exclude: ['id'],
          }),
        },
      },
    })
    roleUser: Omit<RoleUser, 'id'>,
  ): Promise<RoleUser> {
    return this.roleUserRepository.create(roleUser);
  }

  @get('/roles-users-map/count', {
    responses: {
      '200': {
        description: 'RoleUser model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(RoleUser) where?: Where<RoleUser>,
  ): Promise<Count> {
    return this.roleUserRepository.count(where);
  }

  @get('/roles-users-map', {
    responses: {
      '200': {
        description: 'Array of RoleUser model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(RoleUser, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(RoleUser) filter?: Filter<RoleUser>,
  ): Promise<RoleUser[]> {
    return this.roleUserRepository.find(filter);
  }

  @patch('/roles-users-map', {
    responses: {
      '200': {
        description: 'RoleUser PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(RoleUser, {partial: true}),
        },
      },
    })
    roleUser: RoleUser,
    @param.where(RoleUser) where?: Where<RoleUser>,
  ): Promise<Count> {
    return this.roleUserRepository.updateAll(roleUser, where);
  }

  @get('/roles-users-map/{id}', {
    responses: {
      '200': {
        description: 'RoleUser model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(RoleUser, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(RoleUser, {exclude: 'where'}) filter?: FilterExcludingWhere<RoleUser>
  ): Promise<RoleUser> {
    return this.roleUserRepository.findById(id, filter);
  }

  @patch('/roles-users-map/{id}', {
    responses: {
      '204': {
        description: 'RoleUser PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(RoleUser, {partial: true}),
        },
      },
    })
    roleUser: RoleUser,
  ): Promise<void> {
    await this.roleUserRepository.updateById(id, roleUser);
  }

  @put('/roles-users-map/{id}', {
    responses: {
      '204': {
        description: 'RoleUser PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() roleUser: RoleUser,
  ): Promise<void> {
    await this.roleUserRepository.replaceById(id, roleUser);
  }

  @del('/roles-users-map/{id}', {
    responses: {
      '204': {
        description: 'RoleUser DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.roleUserRepository.deleteById(id);
  }
}
