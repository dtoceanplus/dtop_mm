// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Digitalrep} from '../models';
import {DigitalrepRepository} from '../repositories';

export class DigitalrepController {
  constructor(
    @repository(DigitalrepRepository)
    public digitalrepRepository : DigitalrepRepository,
  ) {}

  @post('/digitalreps', {
    responses: {
      '200': {
        description: 'Digitalrep model instance',
        content: {'application/json': {schema: getModelSchemaRef(Digitalrep)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Digitalrep, {
            title: 'NewDigitalrep',
            exclude: ['id'],
          }),
        },
      },
    })
    digitalrep: Omit<Digitalrep, 'id'>,
  ): Promise<Digitalrep> {
    return this.digitalrepRepository.create(digitalrep);
  }

  @get('/digitalreps/count', {
    responses: {
      '200': {
        description: 'Digitalrep model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Digitalrep) where?: Where<Digitalrep>,
  ): Promise<Count> {
    return this.digitalrepRepository.count(where);
  }

  @get('/digitalreps', {
    responses: {
      '200': {
        description: 'Array of Digitalrep model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Digitalrep, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Digitalrep) filter?: Filter<Digitalrep>,
  ): Promise<Digitalrep[]> {
    return this.digitalrepRepository.find(filter);
  }

  @patch('/digitalreps', {
    responses: {
      '200': {
        description: 'Digitalrep PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Digitalrep, {partial: true}),
        },
      },
    })
    digitalrep: Digitalrep,
    @param.where(Digitalrep) where?: Where<Digitalrep>,
  ): Promise<Count> {
    return this.digitalrepRepository.updateAll(digitalrep, where);
  }

  @get('/digitalreps/{id}', {
    responses: {
      '200': {
        description: 'Digitalrep model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Digitalrep, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Digitalrep, {exclude: 'where'}) filter?: FilterExcludingWhere<Digitalrep>
  ): Promise<Digitalrep> {
    return this.digitalrepRepository.findById(id, filter);
  }

  @patch('/digitalreps/{id}', {
    responses: {
      '204': {
        description: 'Digitalrep PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Digitalrep, {partial: true}),
        },
      },
    })
    digitalrep: Digitalrep,
  ): Promise<void> {
    await this.digitalrepRepository.updateById(id, digitalrep);
  }

  @put('/digitalreps/{id}', {
    responses: {
      '204': {
        description: 'Digitalrep PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() digitalrep: Digitalrep,
  ): Promise<void> {
    await this.digitalrepRepository.replaceById(id, digitalrep);
  }

  @del('/digitalreps/{id}', {
    responses: {
      '204': {
        description: 'Digitalrep DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.digitalrepRepository.deleteById(id);
  }
}
