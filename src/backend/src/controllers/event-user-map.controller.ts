// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {EventUser} from '../models';
import {EventUserRepository} from '../repositories';

export class EventUserMapController {
  constructor(
    @repository(EventUserRepository)
    public eventUserRepository : EventUserRepository,
  ) {}

  @post('/events-users-map', {
    responses: {
      '200': {
        description: 'EventUser model instance',
        content: {'application/json': {schema: getModelSchemaRef(EventUser)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(EventUser, {
            title: 'NewEventUser',
            exclude: ['id'],
          }),
        },
      },
    })
    eventUser: Omit<EventUser, 'id'>,
  ): Promise<EventUser> {
    return this.eventUserRepository.create(eventUser);
  }

  @get('/events-users-map/count', {
    responses: {
      '200': {
        description: 'EventUser model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(EventUser) where?: Where<EventUser>,
  ): Promise<Count> {
    return this.eventUserRepository.count(where);
  }

  @get('/events-users-map', {
    responses: {
      '200': {
        description: 'Array of EventUser model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(EventUser, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(EventUser) filter?: Filter<EventUser>,
  ): Promise<EventUser[]> {
    return this.eventUserRepository.find(filter);
  }

  @patch('/events-users-map', {
    responses: {
      '200': {
        description: 'EventUser PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(EventUser, {partial: true}),
        },
      },
    })
    eventUser: EventUser,
    @param.where(EventUser) where?: Where<EventUser>,
  ): Promise<Count> {
    return this.eventUserRepository.updateAll(eventUser, where);
  }

  @get('/events-users-map/{id}', {
    responses: {
      '200': {
        description: 'EventUser model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(EventUser, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(EventUser, {exclude: 'where'}) filter?: FilterExcludingWhere<EventUser>
  ): Promise<EventUser> {
    return this.eventUserRepository.findById(id, filter);
  }

  @patch('/events-users-map/{id}', {
    responses: {
      '204': {
        description: 'EventUser PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(EventUser, {partial: true}),
        },
      },
    })
    eventUser: EventUser,
  ): Promise<void> {
    await this.eventUserRepository.updateById(id, eventUser);
  }

  @put('/events-users-map/{id}', {
    responses: {
      '204': {
        description: 'EventUser PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() eventUser: EventUser,
  ): Promise<void> {
    await this.eventUserRepository.replaceById(id, eventUser);
  }

  @del('/events-users-map/{id}', {
    responses: {
      '204': {
        description: 'EventUser DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.eventUserRepository.deleteById(id);
  }
}
