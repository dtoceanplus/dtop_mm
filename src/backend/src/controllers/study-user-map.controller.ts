// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {StudyUser} from '../models';
import {StudyUserRepository} from '../repositories';

export class StudyUserMapController {
  constructor(
    @repository(StudyUserRepository)
    public studyUserRepository : StudyUserRepository,
  ) {}

  @post('/studies-users-map', {
    responses: {
      '200': {
        description: 'StudyUser model instance',
        content: {'application/json': {schema: getModelSchemaRef(StudyUser)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudyUser, {
            title: 'NewStudyUser',
            exclude: ['id'],
          }),
        },
      },
    })
    studyUser: Omit<StudyUser, 'id'>,
  ): Promise<StudyUser> {
    return this.studyUserRepository.create(studyUser);
  }

  @get('/studies-users-map/count', {
    responses: {
      '200': {
        description: 'StudyUser model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(StudyUser) where?: Where<StudyUser>,
  ): Promise<Count> {
    return this.studyUserRepository.count(where);
  }

  @get('/studies-users-map', {
    responses: {
      '200': {
        description: 'Array of StudyUser model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(StudyUser, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(StudyUser) filter?: Filter<StudyUser>,
  ): Promise<StudyUser[]> {
    return this.studyUserRepository.find(filter);
  }

  @patch('/studies-users-map', {
    responses: {
      '200': {
        description: 'StudyUser PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudyUser, {partial: true}),
        },
      },
    })
    studyUser: StudyUser,
    @param.where(StudyUser) where?: Where<StudyUser>,
  ): Promise<Count> {
    return this.studyUserRepository.updateAll(studyUser, where);
  }

  @get('/studies-users-map/{id}', {
    responses: {
      '200': {
        description: 'StudyUser model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(StudyUser, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(StudyUser, {exclude: 'where'}) filter?: FilterExcludingWhere<StudyUser>
  ): Promise<StudyUser> {
    return this.studyUserRepository.findById(id, filter);
  }

  @patch('/studies-users-map/{id}', {
    responses: {
      '204': {
        description: 'StudyUser PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudyUser, {partial: true}),
        },
      },
    })
    studyUser: StudyUser,
  ): Promise<void> {
    await this.studyUserRepository.updateById(id, studyUser);
  }

  @put('/studies-users-map/{id}', {
    responses: {
      '204': {
        description: 'StudyUser PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() studyUser: StudyUser,
  ): Promise<void> {
    await this.studyUserRepository.replaceById(id, studyUser);
  }

  @del('/studies-users-map/{id}', {
    responses: {
      '204': {
        description: 'StudyUser DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.studyUserRepository.deleteById(id);
  }
}
