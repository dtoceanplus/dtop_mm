// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Project,
  Study,
} from '../models';
import {ProjectRepository} from '../repositories';

export class ProjectStudyController {
  constructor(
    @repository(ProjectRepository) protected projectRepository: ProjectRepository,
  ) { }

  @get('/projects/{id}/studies', {
    responses: {
      '200': {
        description: 'Array of Project has many Study',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Study)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Study>,
  ): Promise<Study[]> {
    return this.projectRepository.studies(id).find(filter);
  }

  @post('/projects/{id}/studies', {
    responses: {
      '200': {
        description: 'Project model instance',
        content: {'application/json': {schema: getModelSchemaRef(Study)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Project.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Study, {
            title: 'NewStudyInProject',
            exclude: ['id'],
            optional: ['projectId']
          }),
        },
      },
    }) study: Omit<Study, 'id'>,
  ): Promise<Study> {
    return this.projectRepository.studies(id).create(study);
  }

  @patch('/projects/{id}/studies', {
    responses: {
      '200': {
        description: 'Project.Study PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Study, {partial: true}),
        },
      },
    })
    study: Partial<Study>,
    @param.query.object('where', getWhereSchemaFor(Study)) where?: Where<Study>,
  ): Promise<Count> {
    return this.projectRepository.studies(id).patch(study, where);
  }

  @del('/projects/{id}/studies', {
    responses: {
      '200': {
        description: 'Project.Study DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Study)) where?: Where<Study>,
  ): Promise<Count> {
    return this.projectRepository.studies(id).delete(where);
  }
}
