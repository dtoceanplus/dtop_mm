// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Study,
  Digitalrep,
} from '../models';
import {StudyRepository} from '../repositories';

export class StudyDigitalrepController {
  constructor(
    @repository(StudyRepository) protected studyRepository: StudyRepository,
  ) { }

  @get('/studies/{id}/digitalrep', {
    responses: {
      '200': {
        description: 'Study has one Digitalrep',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Digitalrep),
          },
        },
      },
    },
  })
  async get(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Digitalrep>,
  ): Promise<Digitalrep> {
    return this.studyRepository.digitalrep(id).get(filter);
  }

  @post('/studies/{id}/digitalrep', {
    responses: {
      '200': {
        description: 'Study model instance',
        content: {'application/json': {schema: getModelSchemaRef(Digitalrep)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Study.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Digitalrep, {
            title: 'NewDigitalrepInStudy',
            exclude: ['id'],
            optional: ['studyId']
          }),
        },
      },
    }) digitalrep: Omit<Digitalrep, 'id'>,
  ): Promise<Digitalrep> {
    return this.studyRepository.digitalrep(id).create(digitalrep);
  }

  @patch('/studies/{id}/digitalrep', {
    responses: {
      '200': {
        description: 'Study.Digitalrep PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Digitalrep, {partial: true}),
        },
      },
    })
    digitalrep: Partial<Digitalrep>,
    @param.query.object('where', getWhereSchemaFor(Digitalrep)) where?: Where<Digitalrep>,
  ): Promise<Count> {
    return this.studyRepository.digitalrep(id).patch(digitalrep, where);
  }

  @del('/studies/{id}/digitalrep', {
    responses: {
      '200': {
        description: 'Study.Digitalrep DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Digitalrep)) where?: Where<Digitalrep>,
  ): Promise<Count> {
    return this.studyRepository.digitalrep(id).delete(where);
  }
}
