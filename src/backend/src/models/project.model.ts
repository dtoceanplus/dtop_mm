// This is the Main Module for the DTOceanPlus suite of Tools.
// It is the main entry point to create and manage projects.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import {Entity, model, property, hasMany} from '@loopback/repository';
import {Study} from './study.model';

@model({settings: {idInjection: false, mysql: {schema: 'main_db', table: 'project'}}})
export class Project extends Entity {
  @property({
    type: 'number',
//    required: true,
    precision: 10,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'int', dataLength: null, dataPrecision: 10, dataScale: 0, nullable: 'N'},
  })
//  id: number;
  id?: number;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'name', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  name: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'description', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  description?: string;

  @property({
    type: 'date',
    mysql: {columnName: 'created_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  createdAt?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'logo', dataType: 'longtext', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  logo?: string;

  @property({
    type: 'string',
    length: 5,
    mysql: {columnName: 'technology', dataType: 'enum', dataLength: 5, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  technology?: string;

  @property({
    type: 'string',
    length: 8,
    mysql: {columnName: 'type_of_device', dataType: 'enum', dataLength: 8, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  typeOfDevice?: string;

  @hasMany(() => Study)
  studies: Study[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Project>) {
    super(data);
  }
}

export interface ProjectRelations {
  // describe navigational properties here
}

export type ProjectWithRelations = Project & ProjectRelations;
