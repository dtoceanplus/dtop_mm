#  be sure that the necessary environment and files
#   are preliminarily prepared by running `prepare_dr_schemas.sh`
#

FROM python:3.8-slim-buster

WORKDIR /app

COPY prod-requirements.txt .
COPY src ./src
#COPY tests ./tests
COPY api-cooperative/dr.py ./api-cooperative/dr.py
COPY api-cooperative/dr-schema.yaml ./api-cooperative/dr-schema.yaml
COPY api-cooperative/dr-document.yaml ./api-cooperative/dr-document.yaml
COPY api-cooperative/src/dtop-shared-library ./api-cooperative/src/dtop-shared-library

RUN apt-get update \
    && apt-get install --yes --no-install-recommends gcc libc6-dev git \
    && pip install --requirement prod-requirements.txt \
#    && pip install --requirement requirements.txt \
    && pip install --no-cache-dir 'git+https://github.com/kennknowles/python-jsonpath-rw.git@b008a97c247cd0f309e85db98e37d0c14e8dc81c#egg=jsonpath_rw' \
    && apt-get install --yes curl \
    && curl --silent --location https://deb.nodesource.com/setup_14.x | bash - \
    && apt-get install -y nodejs \
    && npm install --global swagger-cli \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get purge --auto-remove --yes gcc libc6-dev git

ENV FLASK_APP .

EXPOSE 5000
