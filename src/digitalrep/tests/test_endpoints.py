# This is the Main Module for the DTOceanPlus suite of Tools.
# It is the main entry point to create and manage projects.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import os
import json

import requests
from openapi_spec_validator import validate_spec_url

from urllib.parse import urljoin

post_req_dict = {}

try:
    with open('tests/dr_post_request_body__slc_et_sample__test_2.json') as json_file:
        post_req_sample_dict = json.load(json_file)
except FileNotFoundError:
    print ("the test json data file if not found")
    exit(1)

try:
    with open('tests/dr_post_request_body__slc_et_full__test_2.json') as json_file:
        post_req_full_dict = json.load(json_file)
except FileNotFoundError:
    print ("the test json data file if not found")
    exit(1)

def test_study_digitalrep_availability(api_dr_host):

    endpoint = urljoin(api_dr_host, 'dr_test')

    response = requests.get(endpoint)
    assert response.status_code == 200
    json = response.json()
    assert 'msg' in json
    assert json['msg'] == "DR OpenAPI service is available"


def test_sample_study_digitalrep(api_dr_host):

    endpoint = urljoin(api_dr_host, 'study_digitalrep')
    payload = post_req_sample_dict

    response = requests.post(endpoint, json=payload)

    assert response.status_code == 201
    json = response.json()

    assert 'dr_auto_doc' in json
    assert 'dr_auto_schema' in json
    assert 'error_msg' in json
    assert 'exit_code' in json
    assert 'study_id' in json

    assert {'x1': [{'id': 1, 'y1': 'Y'}, {'id': 'abc', 'y2': 'Y'}]} == json['dr_auto_doc']
    assert 'Reading _dtop-et_0.0.1_dr_schema_doc.yml...' in json['error_msg']
    assert 'Reading _dtop-slc_0.0.1_dr_schema_doc.yml...' in json['error_msg']
    assert 0 == json['exit_code']


def test_full_study_digitalrep(api_dr_host):

    endpoint = urljoin(api_dr_host, 'study_digitalrep')
    payload = post_req_full_dict

    response = requests.post(endpoint, json=payload)

    assert response.status_code == 201
    json = response.json()

    assert 'dr_auto_doc' in json
    assert 'dr_auto_schema' in json
    assert 'error_msg' in json
    assert 'exit_code' in json
    assert 'study_id' in json

# ET
    assert 'mechanical_conversion' in json['dr_auto_doc']
# SLC
    assert 'economical_results' in json['dr_auto_doc']
    assert 'Reading _dtop-et_0.0.1_dr_schema_doc.yml...' in json['error_msg']
    assert 'Reading _dtop-slc_0.0.1_dr_schema_doc.yml...' in json['error_msg']
    assert 0 == json['exit_code']


def test_swagger_specification(host):
    endpoint = urljoin(host, 'api/dr_service/swagger.json')

    validate_spec_url(endpoint)
