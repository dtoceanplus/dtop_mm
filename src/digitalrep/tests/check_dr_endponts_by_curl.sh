#/bin/bash

# Description:
#  The test script toi check the endpoints of MM Digital Representation (DR) OpenAPI
#  using the running docker services
#
# Run Information:
#  chmod u+x ./check_dr_endponts_by_curl.sh
#  ./check_dr_endponts_by_curl.sh


set -e

# test GET request to check abailability if Digital Representation (DR) service

curl -X GET "http://localhost:5000/api/dr_service/dr_test" -H  "accept: application/json"


# POST request to generate the merged Digital Representation (DR) for all study modules

curl -X POST "http://localhost:5000/api/dr_service/study_digitalrep" -H  "accept: application/json" -H  "Content-Type: application/json" --data '{
  "study_id": 111,
  "modules": [
    {
      "module_name": "slc",
      "dr_doc": {
        "bom_compiled": {
          "id": [
            "Device",
            "Tot_Device",
            "CAT_turbine",
            "CAT_gen",
            "CAT_b2b",
            "Tot_ET",
            "CAT_Cable001",
            "CAT_Cable062",
            "CAT_colpoint",
            "CAT_con001",
            "Tot_onshoreinf",
            "Tot_transm",
            "Tot_network",
            "Tot_colpoint",
            "CAT_Anchor001",
            "CAT_ML001",
            "Tot_SK",
            "Tot_Inst_Dev",
            "Tot_Inst_Anc",
            "Tot_Inst_Moor",
            "Tot_Inst_Cable",
            "Tot_Inst_Col",
            "Tot_Other"
          ],
          "name": [
            "fixed",
            "Device total cost",
            "Air turbine",
            "Generator_x",
            "Back to back converter",
            "Total ET system",
            "Cable xyz",
            "Cable xyz239",
            "Subsea hub",
            "Connector wet-mate",
            "Total onshore infrastructure",
            "Total Transmission network",
            "Total Array network",
            "Total Collection point",
            "Anchor ",
            "Mooring line",
            "Total costs of SK system",
            "Total cost of installation of devices",
            "Total cost of installation of Anchors",
            "Total cost of installation of Moorings",
            "Total cost of installation of cables",
            "Total cost of installation of Collection points",
            "Other costs"
          ],
          "qnt": [
            50,
            0,
            20,
            20,
            20,
            0,
            300,
            9000,
            2,
            3,
            0,
            0,
            0,
            0,
            40,
            1500,
            0,
            0,
            0,
            0,
            0,
            0,
            0
          ],
          "uom": [
            "NA",
            "NA",
            "NA",
            "NA",
            "NA",
            "NA",
            "m",
            "m",
            "NA",
            "NA",
            "NA",
            "NA",
            "NA",
            "NA",
            "NA",
            "m",
            "NA",
            "NA",
            "NA",
            "NA",
            "NA",
            "NA",
            "NA"
          ],
          "unit_cost": [
            1000000,
            0,
            100000,
            70000,
            20000,
            0,
            2300,
            1100,
            1000000,
            1000000,
            0,
            0,
            0,
            0,
            70000,
            3000,
            0,
            0,
            0,
            0,
            0,
            0,
            0
          ],
          "total_cost": [
            50000000,
            50000000,
            0,
            0,
            0,
            1908099,
            0,
            0,
            0,
            0,
            0,
            0,
            43200,
            0,
            0,
            0,
            558491,
            872215,
            6128696,
            0,
            1628674,
            0,
            2000000
          ],
          "category": [
            "Device",
            "Device",
            "Device",
            "Device",
            "Device",
            "Device",
            "Grid",
            "Grid",
            "Grid",
            "Grid",
            "Grid",
            "Grid",
            "Grid",
            "Grid",
            "Moor_Found",
            "Moor_Found",
            "Moor_Found",
            "Installation",
            "Installation",
            "Installation",
            "Installation",
            "Installation",
            "Other"
          ]
        },
        "economical_results": {
          "capex": 5000000,
          "opex": {
            "total": 5000000,
            "per_year": 250000
          },
          "lcoe": 7.345,
          "ace": 132.463
        },
        "financial_results": {
          "pbp": 22,
          "irr": 24.3,
          "npv": -942256.41
        },
        "benchmark": {
          "capex_kw": 15945.24,
          "opex_kw": 6782.4,
          "lcoe_breakdown": {
            "other": 7.1,
            "grid": 19,
            "device": 24.9,
            "moors_founds": 13.8,
            "installation": 25,
            "opex": 10.2
          }
        }
      }
    },
    {
      "module_name": "et",
      "dr_doc": {
        "array": [
          {
            "id": "1",
            "properties": {
              "array_cost": {
                "description": "cost of the array",
                "value": 123,
                "unit": "�",
                "origin": "ET"
              },
              "array_mass": {
                "description": "mass of the array",
                "value": 123,
                "unit": "kg",
                "origin": "ET"
              }
            },
            "assessment": {
              "array_annual_mean_mechanical_power": {
                "description": "array_annual_mean_mechanical_power",
                "value": 123,
                "unit": "aaa",
                "origin": "ET"
              },
              "array_annual_mean_electrical_power": {
                "description": "array_annual_mean_electrical_power",
                "value": 123,
                "unit": "aaa",
                "origin": "ET"
              },
              "array_annual_mean_grid_conditioning_active_power": {
                "description": "array_annual_mean_grid_conditioning_active_power",
                "value": 123,
                "unit": "aaa",
                "origin": "ET"
              },
              "array_annual_mean_grid_conditioning_reactive_power": {
                "description": "array_annual_mean_grid_conditioning_reactive_power",
                "value": 123,
                "unit": "aaa",
                "origin": "ET"
              },
              "array_annual_transformed_mechanical_energy": {
                "description": "array_annual_transformed_mechanical_energy",
                "value": 123,
                "unit": "aaa",
                "origin": "ET"
              },
              "array_annual_transformed_electrical_energy": {
                "description": "array_annual_transformed_electrical_energy",
                "value": 123,
                "unit": "aaa",
                "origin": "ET"
              },
              "array_annual_grid_conditioning_energy": {
                "description": "array_annual_grid_conditioning_energy",
                "value": 123,
                "unit": "aaa",
                "origin": "ET"
              }
            }
          }
        ],
        "device": [
          {
            "id": "1_1",
            "properties": {
              "number_of_ptos_per_device": {
                "description": "number_of_ptos_per_device",
                "value": 2,
                "unit": "-",
                "origin": "ET"
              },
              "cut_in_out": {
                "description": "cut-in-cut-out",
                "value": [
                  0.5,
                  5
                ],
                "unit": "m",
                "origin": "ET"
              }
            },
            "assessment": {
              "device_annual_transformed_mechanical_power": {
                "description": "device_annual_transformed_mechanical_power",
                "value": 123,
                "unit": "aaa",
                "origin": "ET"
              },
              "device_annual_transformed_electrical_power": {
                "description": "device_annual_transformed_electrical_power",
                "value": 123,
                "unit": "aaa",
                "origin": "ET"
              },
              "device_annual_grid_conditioning_active_power": {
                "description": "device_annual_grid_conditioning_active_power",
                "value": 123,
                "unit": "-",
                "origin": "ET"
              },
              "device_annual_grid_conditioning_reactive_power": {
                "description": "device_annual_grid_conditioning_reactive_power",
                "value": 123,
                "unit": "-",
                "origin": "ET"
              },
              "device_annual_mechanical_transformed_energy": {
                "description": "device_annual_mechanical_transformed_energy",
                "value": 123,
                "unit": "-",
                "origin": "ET"
              },
              "device_annual_electrical_transformed_energy": {
                "description": "device_annual_electrical_transformed_energy",
                "value": 123,
                "unit": "-",
                "origin": "ET"
              }
            }
          },
          {
            "id": "1_2",
            "properties": {
              "number_of_ptos_per_device": {
                "description": "number_of_ptos_per_device",
                "value": 2,
                "unit": "-",
                "origin": "ET"
              },
              "cut_in_out": {
                "description": "cut-in-cut-out",
                "value": [
                  0.5,
                  5
                ],
                "unit": "m",
                "origin": "ET"
              }
            },
            "assessment": {
              "device_annual_transformed_mechanical_power": {
                "description": "device_annual_transformed_mechanical_power",
                "value": 123,
                "unit": "aaa",
                "origin": "ET"
              },
              "device_annual_transformed_electrical_power": {
                "description": "device_annual_transformed_electrical_power",
                "value": 123,
                "unit": "aaa",
                "origin": "ET"
              },
              "device_annual_grid_conditioning_active_power": {
                "description": "device_annual_grid_conditioning_active_power",
                "value": 123,
                "unit": "-",
                "origin": "ET"
              },
              "device_annual_grid_conditioning_reactive_power": {
                "description": "device_annual_grid_conditioning_reactive_power",
                "value": 123,
                "unit": "-",
                "origin": "ET"
              },
              "device_annual_mechanical_transformed_energy": {
                "description": "device_annual_mechanical_transformed_energy",
                "value": 123,
                "unit": "-",
                "origin": "ET"
              },
              "device_annual_electrical_transformed_energy": {
                "description": "device_annual_electrical_transformed_energy",
                "value": 123,
                "unit": "-",
                "origin": "ET"
              }
            }
          }
        ],
        "pto": [
          {
            "id": "1_1_1",
            "assessment": {
              "stress_load": {
                "description": "stress_load",
                "value": 1111,
                "unit": "aaa",
                "origin": "ET"
              },
              "stress_load_range": {
                "description": "stress_load_range",
                "value": 1111,
                "unit": "aaa",
                "origin": "ET"
              },
              "s_n": {
                "description": "s_n",
                "value": 1111,
                "unit": "aaa",
                "origin": "ET"
              }
            }
          }
        ],
        "mechanical_conversion": [
          {
            "id": "1_1_1_1",
            "properties": {
              "mechanical_conversion_size": {
                "description": "mechanical_conversion_size",
                "value": 111,
                "unit": "aaa",
                "origin": "ET"
              },
              "mechanical_conversion_cost": {
                "description": "mechanical_conversion_cost",
                "value": 123,
                "unit": "�",
                "origin": "ET"
              },
              "mechanical_conversion_mass": {
                "description": "mechanical_conversion_mass",
                "value": 123,
                "unit": "kg",
                "origin": "ET"
              }
            },
            "assessment": {
              "mechanical_mean_transformed_power": {
                "description": "mechanical_mean_transformed_power",
                "value": 111,
                "unit": "aaa",
                "origin": "ET"
              },
              "mechanical_transformed_energy": {
                "description": "mechanical_transformed_energy",
                "value": 123,
                "unit": "aaa",
                "origin": "ET"
              },
              "mechanical_conversion_loads_pdf": {
                "description": "mechanical_conversion_loads_pdf",
                "value": 123,
                "unit": "aaa",
                "origin": "ET"
              }
            }
          }
        ],
        "electrical_conversion": [
          {
            "id": "1_1_1_1",
            "properties": {
              "electrical_conversion_size": {
                "description": "electrical_conversion_size",
                "value": 111,
                "unit": "aaa",
                "origin": "ET"
              },
              "elecrical_conversion_cost": {
                "description": "electrical_conversion_cost",
                "value": 123,
                "unit": "�",
                "origin": "ET"
              },
              "electrical_conversion_mass": {
                "description": "electrical_conversion_mass",
                "value": 123,
                "unit": "kg",
                "origin": "ET"
              }
            },
            "assessment": {
              "electrical_mean_transformed_power": {
                "description": "electrical_mean_transformed_power",
                "value": 111,
                "unit": "aaa",
                "origin": "ET"
              },
              "electrical_transformed_energy": {
                "description": "electrical_transformed_energy",
                "value": 123,
                "unit": "aaa",
                "origin": "ET"
              },
              "electrical_conversion_loads_pdf": {
                "description": "electrical_conversion_loads_pdf",
                "value": 123,
                "unit": "aaa",
                "origin": "ET"
              }
            }
          }
        ],
        "grid_conditioning": [
          {
            "id": "1_1_1_1",
            "properties": {
              "grid_conditiong_rated_power": {
                "description": "grid_conditiong_rated_power",
                "value": 111,
                "unit": "aaa",
                "origin": "ET"
              },
              "grid_conditiong_conversion_cost": {
                "description": "grid_conditiong_conversion_cost",
                "value": 123,
                "unit": "�",
                "origin": "ET"
              },
              "grid_conditiong_conversion_mass": {
                "description": "grid_conditiong_conversion_mass",
                "value": 123,
                "unit": "kg",
                "origin": "ET"
              }
            },
            "assessment": {
              "grid_conditioning_mean_active_power": {
                "description": "grid_conditioning_mean_active_power",
                "value": 111,
                "unit": "aaa",
                "origin": "ET"
              },
              "grid_conditioning_mean_reactive_power": {
                "description": "grid_conditioning_mean_reactive_power",
                "value": 123,
                "unit": "aaa",
                "origin": "ET"
              },
              "grid_conditioning_energy": {
                "description": "grid_conditioning_energy",
                "value": 123,
                "unit": "aaa",
                "origin": "ET"
              }
            }
          }
        ]
      }
    }
  ]
}'
