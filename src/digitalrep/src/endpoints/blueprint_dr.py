# This is the Main Module for the DTOceanPlus suite of Tools.
# It is the main entry point to create and manage projects.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


"""DTOP MM DR blueprint and endpoints"""

from flask import Blueprint, jsonify, request

from marshmallow import ValidationError

from src.services.merge_dr_service import get_post_response_dict, DRPrepareError, DRMergeError
from src.api_spec import StudyDR, StudyMergedDR

blueprint_dr = Blueprint(name="blueprint_dr", import_name=__name__)

@blueprint_dr.route('/dr_test', methods=['GET'])
def test():
    """
    ---
    get:
      description: test endpoint
      responses:
        '200':
          description: call successful
          content:
            application/json:
              schema: OutputSchema
      tags:
          - 'Digital Representation'
    """
    output = {"msg": "DR OpenAPI service is available"}
    return jsonify(output)


@blueprint_dr.route('/study_digitalrep', methods=['POST'], strict_slashes=False)
def create_study_dr():
    """
    ---
    post:
      description: creates the merged Digital Representation (DR) for all study modules
      requestBody:
        required: true
        content:
            application/json:
                schema: StudyDR
      responses:
        '200':
          description: call successful
          content:
            application/json:
              schema: StudyMergedDR
      tags:
          - 'Digital Representation'
    """
    try:

        request_body = request.get_json()
        if not request_body:
            return {"message": "ERROR: No DR POST request body provided"}, 400

        try:

            post_req_dict = StudyDR().load(request_body)

        except ValidationError as error:
            return {"ERROR: the provided DR POST request data doc are invalid, errors": error.messages}, 422

        post_resp_dict = get_post_response_dict(post_req_dict)

        result = StudyMergedDR().dump(post_resp_dict)

        return result, 201

    except DRPrepareError as error:
        return jsonify(str(error)), 404

    except DRMergeError as error:
        return jsonify(str(error)), 404
