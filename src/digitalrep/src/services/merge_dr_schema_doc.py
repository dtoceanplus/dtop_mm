# This is the Main Module for the DTOceanPlus suite of Tools.
# It is the main entry point to create and manage projects.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import sys
import os

import yaml
import re

from pathlib import Path

from typing import Dict, TextIO

import argparse

from prepare_dr_schema_doc import save_to_yaml

def merge_openapi_dr(in_dr_schema_file_path: Path, in_dr_doc_file_path: Path, out_dr_schema_doc_file_path: Path):
    """
    Reads the digital representation schema file defined by 'in_dr_schema_file_path'.
    Reads the digital representation doc file defined by 'in_dr_doc_file_path'.
    Merges them into one dr schema and doc file defined by 'out_dr_schema_doc_file_path'.

    """

    if in_dr_schema_file_path.is_file():
      print ("the file " + str(in_dr_schema_file_path) + " exists")
    else:
      print ("the file " + str(in_dr_schema_file_path) + " doesn't exist")
      exit(1)

    if in_dr_doc_file_path.is_file():
      print ("the file " + str(in_dr_doc_file_path) + " exists")
    else:
      print ("the file " + str(in_dr_doc_file_path) + " doesn't exist")
      exit(1)

    with open(in_dr_schema_file_path, 'r') as dr_schema_yaml_file:
        dr_schema_dict = yaml.safe_load(dr_schema_yaml_file)

    with open(in_dr_doc_file_path, 'r') as dr_doc_yaml_file:
        dr_doc_dict = yaml.safe_load(dr_doc_yaml_file)


    for api_path, api_decl in dr_schema_dict['paths'].items():
        if re.match(r'/representation/\{\w+\}', api_path):

            try:
                dr_schema_dict['paths'][api_path]['get']['responses']['200']['content']['application/json']['schema']
                print('  Representation schema is Found.')
            except KeyError:
                print('  No representation schema is Found.', file=sys.stderr)

            try:
                dr_schema_doc_dict = dr_schema_dict.copy()

                dr_schema_doc_dict['paths'][api_path]['get']['responses']['200']['content']['application/json']['example']=dr_doc_dict

                dr_schema_doc_dict['paths'][api_path]['get']['responses']['200']['content']['application/json']['example']
                print('  Example for this representation schema is Found.')

                with open(out_dr_schema_doc_file_path, 'w') as dr_schema_doc_yaml_file:
                    save_to_yaml(dr_schema_doc_dict, dr_schema_doc_yaml_file)

            except KeyError:
                print('  No Example for this representation schema is Found.', file=sys.stderr)

def create_arg_parser():
    """
    Creates and returns the ArgumentParser object
    """

    parser = argparse.ArgumentParser(description='To merge DTOP module digital respesenatation schema and doc (example).')

    parser.add_argument('in_dr_schema_file_path',
                         type=Path,
                         help='Path to the input module digital respesenatation schema file.')
    parser.add_argument('in_dr_doc_file_path',
                         type=Path,
                         help='Path to the input module digital respesenatation doc file.')
    parser.add_argument('out_dr_schema_doc_file_path',
                         type=Path,
                         help='Path to the output module digital respesenatation schema and doc merged file.')
    return parser


if __name__ == "__main__":

#
# python merge_dr_schema_doc.py -h
# usage: merge_dr_schema_doc.py [-h]
#                              in_dr_schema_file_path in_dr_doc_file_path
#                              out_dr_schema_doc_file_path
#
# To merge DTOP module digital respesenatation schema and doc (example).
#
# positional arguments:
#   in_dr_schema_file_path
#                         Path to the input module digital respesenatation
#                         schema file.
#   in_dr_doc_file_path   Path to the input module digital respesenatation doc
#                         file.
#   out_dr_schema_doc_file_path
#                         Path to the output module digital respesenatation
#                         schema and doc merged file.
# 
# optional arguments:
#   -h, --help            show this help message and exit
#

    arg_parser = create_arg_parser()
    parsed_args = arg_parser.parse_args(sys.argv[1:])

    print("parsed_args.in_dr_schema_file_path = ", parsed_args.in_dr_schema_file_path)
    print("parsed_args.in_dr_doc_file_path = ", parsed_args.in_dr_doc_file_path)
    print("parsed_args.out_dr_schema_doc_file_path = ", parsed_args.out_dr_schema_doc_file_path)

    merge_openapi_dr(parsed_args.in_dr_schema_file_path, parsed_args.in_dr_doc_file_path, parsed_args.out_dr_schema_doc_file_path)

#
# Test Example :

# python merge_dr_schema_doc.py ./_test_openapi_slc_dr_schema.yml ./_test_openapi_slc_dr_doc.yml ./_test_dtop-slc_0.0.1_dr_schema_doc.yml
# python merge_dr_schema_doc.py ./_test_openapi_et_dr_schema.yml ./_test_openapi_et_dr_doc.yml ./_test_dtop-et_0.0.1_dr_schema_doc.yml
