# This is the Main Module for the DTOceanPlus suite of Tools.
# It is the main entry point to create and manage projects.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import sys
import os

import yaml
import re

from pathlib import Path

from typing import Dict, TextIO

import argparse


def extract_openapi_dr(in_openapi_file_path: Path, out_dr_schema_file_path: Path, out_dr_doc_file_path: Path):
    """
    Reads the dereferenced module openapi.yml defined by 'in_openapi_file_path' and previously generated by api-cooperative submodule 'make test'.
    Finds the representation schema and extracts the representation schema
       The schema must be declared for request `GET /representation/{id}`
    Creates extracted openapi digital representation schema file defined by 'out_dr_schema_file_path'
    Creates extracted openapi digital representation doc (example) file defined by 'out_dr_doc_file_path'
    """

    if in_openapi_file_path.is_file():
      print ("the file " + str(in_openapi_file_path) + " exists")
    else:
      print ("the file " + str(in_openapi_file_path) + " doesn't exist")
      exit(1)

    with open(in_openapi_file_path, 'r') as openapi_dr_yaml_file:
        openapi_dr_dict = yaml.safe_load(openapi_dr_yaml_file)

    extracted_dr_dict = {'openapi': openapi_dr_dict['openapi'], 'info': openapi_dr_dict['info']}

    for api_path, api_decl in openapi_dr_dict['paths'].items():

        if re.match(r'/representation/\{\w+\}', api_path):

            content = api_decl
            api_path_dict = {}
            api_path_dict[api_path] = content

            extracted_dr_dict['paths']=api_path_dict

            try:
                extracted_dr_dict['paths'][api_path]['get']['responses']['200']['content']['application/json']['schema']
                print('  Representation schema is Found.')
            except KeyError:
                print('  No representation schema is Found.', file=sys.stderr)

            try:
                extracted_dr_dict['paths'][api_path]['get']['responses']['200']['content']['application/json']['example']
                print('  Example for this representation schema is Found.')

                extracted_dr_doc_dict = {}
                extracted_dr_doc_dict=extracted_dr_dict['paths'][api_path]['get']['responses']['200']['content']['application/json']['example']

                extracted_dr_schema_dict = extracted_dr_dict.copy()
                del(extracted_dr_schema_dict['paths'][api_path]['get']['responses']['200']['content']['application/json']['example'])


                with open(out_dr_doc_file_path, 'w') as openapi_dr_doc_yaml_file:
                    save_to_yaml(extracted_dr_doc_dict, openapi_dr_doc_yaml_file)

                with open(out_dr_schema_file_path, 'w') as openapi_dr_schema_yaml_file:
                    save_to_yaml(extracted_dr_schema_dict, openapi_dr_schema_yaml_file)

            except KeyError:
                print('  No Example for this representation schema is Found.', file=sys.stderr)



def save_to_yaml(data: Dict, file: TextIO):
    """
    Save dictionary to yaml file
    If the file is actuall the stdout, then an extra empty line will be printed to stderr
    for human readability in console.
    """
    print(yaml.dump(data, allow_unicode=True, sort_keys=False), file=file, end='')

    if file is not sys.stdout:
        file.close()
    else:
        print('', file=sys.stderr)

def create_arg_parser():
    """
    Creates and returns the ArgumentParser object
    """

    parser = argparse.ArgumentParser(description='To prepare DTOP module openapi for digital respesenatation schema and doc (example) only.')

    parser.add_argument('in_module_openapi',
                         type=Path,
                         help='Path to the input module full openapi file.')
    parser.add_argument('out_module_openapi_dr_schema',
                         type=Path,
                         help='Path to the output module openapi schema file containing digital respesenatation endpoint only.')
    parser.add_argument('out_module_openapi_dr_doc',
                         type=Path,
                         help='Path to the output module openapi doc (example) for digital respesenatation endpoint.')
    return parser


if __name__ == "__main__":

#
# python prepare_dr_schema_doc.py -h
# usage: prepare_dr_schema_doc.py [-h]
#                                 in_module_openapi out_module_openapi_dr_schema
#                                 out_module_openapi_dr_doc

# To prepare DTOP module openapi for digital respesenatation schema and doc
# (example) only.

# positional arguments:
#   in_module_openapi     Path to the input module full openapi file.
#   out_module_openapi_dr_schema
#                         Path to the output module openapi schema file
#                         containing digital respesenatation endpoint only.
#   out_module_openapi_dr_doc
#                         Path to the output module openapi doc (example) for
#                         digital respesenatation endpoint.
# 
# optional arguments:
#   -h, --help            show this help message and exit
# 

    arg_parser = create_arg_parser()
    parsed_args = arg_parser.parse_args(sys.argv[1:])

    print("parsed_args.in_module_openapi = ", parsed_args.in_module_openapi)
    print("parsed_args.out_module_openapi_dr_schema = ", parsed_args.out_module_openapi_dr_schema)
    print("parsed_args.out_module_openapi_dr_doc = ", parsed_args.out_module_openapi_dr_doc)

    extract_openapi_dr(parsed_args.in_module_openapi, parsed_args.out_module_openapi_dr_schema, parsed_args.out_module_openapi_dr_doc)

#
# Test Example :

# python prepare_dr_schema_doc.py ./dtop-slc/0.0.1/openapi.yml ./_test_openapi_slc_dr_schema.yml ./_test_openapi_slc_dr_doc.yml
# python prepare_dr_schema_doc.py ./dtop-et/0.0.1/openapi.yml ./_test_openapi_et_dr_schema.yml ./_test_openapi_et_dr_doc.yml

