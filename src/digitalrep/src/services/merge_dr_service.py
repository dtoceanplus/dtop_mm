# This is the Main Module for the DTOceanPlus suite of Tools.
# It is the main entry point to create and manage projects.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import sys
import os

import json
import yaml
import re

from glob import glob
from io import StringIO
from typing import Tuple, List
from shutil import copyfile
from tempfile import TemporaryDirectory

from os.path import dirname, join, abspath

sys.path.append(abspath(join(dirname(__file__), '../../api-cooperative')))
sys.path.append(abspath(join(dirname(__file__), '../../api-cooperative/src/dtop-shared-library')))
sys.path.append(abspath(join(dirname(__file__), '.')))

from dr import main, run_merge
from prepare_dr_schema_doc import save_to_yaml

class DRError(Exception):
    """
    A custom exception for DTOP DR preparation and merging.
    """
    pass

class DRPrepareError(DRError):
    """
    A custom exception for preparation of DR with schema for DTOP module.
    """
    pass

class DRMergeError(DRError):
    """
    A custom exception for checking and merging of DR schema for DTOP module.
    """
    pass


def copy_ref_dr(dr_file: str, out_dir: str):

    try:
        copyfile(dr_file, out_dir)
        print("File ", dr_file, " copied successfully.")
    except PermissionError:
        print("Permission denied.")
    except:
        print("Error occurred while copying file ", dr_file)

def make_post_request_dict():

    try:
        with open('tests/dr_post_request_body__slc_et_full__test_2.json') as json_file:
#        with open('tests/dr_post_request_body__slc_et_sample__test_2.json') as json_file:

            post_req_dict = json.load(json_file)
            return post_req_dict
    except FileNotFoundError:
        print ("  make_post_request_dict() : the test json data file is not found")
        exit(1)

modules_list = ['sc', 'mc', 'ec', 'et', 'ed', 'sk', 'lmo', 'spey', 'rams', 'slc', 'esa', 'si', 'sg']

def prepare_dr(post_req_dict: dict, out_dir: str):

    for module in post_req_dict['modules']:

        module_name = module['module_name']
        print('module_name = ', module_name)

        if module_name in modules_list :
            print('module_name is correct ')
        else:
            msg = 'The module ' + module_name + ' is not in the DTOP modules list'
            print(msg)
            raise DRPrepareError (msg)

        dr_doc = module['dr_doc']
        print('DR doc is provided')

        if len(dr_doc) == 0:
            msg = 'The provided DR doc is Empty'
            print(msg)
            raise DRPrepareError (msg)

        dr_schema_name = 'src/services/dr_schemas/dtop-' + module_name + '_0.0.1_dr_schema.yml'

        try:
            with open(dr_schema_name) as file:
                module_dr_schema = yaml.load(file, Loader=yaml.SafeLoader)

                for api_path, api_decl in module_dr_schema['paths'].items():
                    if re.match(r'/representation/\{\w+\}', api_path):

                        try:
                            dr_schema_doc_dict = module_dr_schema.copy()
                            dr_schema_doc_dict['paths'][api_path]['get']['responses']['200']['content']['application/json']['example']=dr_doc

                            module_dr_schema['paths'][api_path]['get']['responses']['200']['content']['application/json']['example']
                            print('  ' + module_name + ' module : openapi including DR schema and DR document is prepared.')

                            with open(out_dir + '/_dtop-' + module_name + '_0.0.1_dr_schema_doc.yml', 'w') as dr_schema_doc_yaml_file:
                                save_to_yaml(dr_schema_doc_dict, dr_schema_doc_yaml_file)

                        except KeyError:
                            msg = '  ' + module_name + ' module : DR document cannot be set into openapi - no example element is found : ' + error
                            print(msg)
                            raise DRPrepareError(msg)

        except DRPrepareError as error:
            msg = 'The schema file ' + dr_schema_name + ' is not found : ' + str(error)
            print(msg)
            raise DRPrepareError(msg)

    print("Preparation of digital representations with schemas for modules is finished")


ref_dr_schema = "api-cooperative/dr-schema.yaml"
ref_dr_doc = "api-cooperative/dr-document.yaml"

def merge_openapi_dr(post_req_dict: dict, out_dir: str):

    old_cwd = os.getcwd()
    prepare_dr(post_req_dict, out_dir)

    copyfile(ref_dr_schema, out_dir+"/dr-schema.yaml")
    copyfile(ref_dr_doc, out_dir+"/dr-document.yaml")

    os.chdir(out_dir)

    exit_code, stderr = _run_dr('merge',
                                *sorted(glob('_dtop-*.yml')),
                                '--output-document', f'./_test_dr-auto-document.yaml',
                                '--output-schema', f'./_test_dr-auto-schema.yaml')

    with open(f'./_test_dr-auto-document.yaml') as file:
        dr_auto_doc_dict = yaml.load(file, Loader=yaml.SafeLoader)

    with open(f'./_test_dr-auto-schema.yaml') as file:
        dr_auto_schema_dict = yaml.load(file, Loader=yaml.SafeLoader)

    study_id=post_req_dict['study_id']

#    print(out_dir)
#    pause()

    os.chdir(old_cwd)

    print("Checking and Merging of digital representations with schemas for modules is finished")

    return study_id, dr_auto_doc_dict, dr_auto_schema_dict, exit_code, stderr


def pause():
    programPause = input("Press the <ENTER> key to continue...")


def merge_openapi_dr_in_tmp(post_req_dict: dict):

    try:
        with TemporaryDirectory() as out_dir:
            old_cwd = os.getcwd()
            prepare_dr(post_req_dict, out_dir)

            copyfile(ref_dr_schema, out_dir+"/dr-schema.yaml")
            copyfile(ref_dr_doc, out_dir+"/dr-document.yaml")

            os.chdir(out_dir)

            exit_code, stderr = _run_dr('merge',
                                        *sorted(glob('_dtop-*.yml')),
                                       '--output-document', f'{out_dir}/dr-auto-document.yaml',
                                       '--output-schema', f'{out_dir}/dr-auto-schema.yaml')

            with open(f'{out_dir}/dr-auto-document.yaml') as file:
                dr_auto_doc_dict = yaml.load(file, Loader=yaml.SafeLoader)

            with open(f'{out_dir}/dr-auto-schema.yaml') as file:
                dr_auto_schema_dict = yaml.load(file, Loader=yaml.SafeLoader)

            study_id=post_req_dict['study_id']

#            print(out_dir)
#            pause()

            os.chdir(old_cwd)
            print("Checking and Merging of digital representations with schemas for modules is finished")

            return study_id, dr_auto_doc_dict, dr_auto_schema_dict, exit_code, stderr

    except DRMergeError as error:
        msg = ' In merge_ dr_services :: Checking and Merging of DRs of study modules has error(s) : '
        print(msg)
        raise DRMergeError(msg)


def _run_dr(*args: str) -> Tuple[int, str]:

    old_argv = sys.argv
    sys.argv = ['dr', *args]
    sys.stderr = StringIO()

    try:
        main()
    except SystemExit as exc:
        return exc.code, sys.stderr.getvalue().strip()
    else:
        return 0, sys.stderr.getvalue().strip()
    finally:
        sys.argv = old_argv
        sys.stderr = sys.__stderr__


def get_post_response_test_dict():

    try:
        with open('tests/dr_post_response__slc_et_sample__test_1.json') as json_file:
            post_resp_test_dict = json.load(json_file)
            return post_resp_test_dict
    except FileNotFoundError:
        print ("  the file " + json_file + " doesn't exist")
        exit(1)


def get_post_response_dict(post_req_dict: dict):

    try:
        study_id, dr_auto_doc_dict, dr_auto_schema_dict, exit_code, error_msg = merge_openapi_dr_in_tmp(post_req_dict)

        post_resp_dict={}
        post_resp_dict['study_id']=study_id
        post_resp_dict['dr_auto_doc']=dr_auto_doc_dict
        post_resp_dict['dr_auto_schema']=dr_auto_schema_dict

        post_resp_dict['exit_code']=exit_code
        post_resp_dict['error_msg']=error_msg

        return post_resp_dict

    except DRMergeError as error:
        msg = 'Checking and Merging of DRs of study modules has error(s) : '
        print(msg)
        raise DRMergeError(msg)
