# This is the Main Module for the DTOceanPlus suite of Tools.
# It is the main entry point to create and manage projects.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


"""MM Digital Representation (DR) OpenAPI v3 Specification"""

from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin

from marshmallow import Schema, fields, pprint, ValidationError

spec = APISpec(
    title="DTOP Main Module 'dtop-mm' Digital Representation (DR) OpenAPI",
    version="1.0.0",
    openapi_version="3.0.2",
    plugins=[FlaskPlugin(), MarshmallowPlugin()],
)

# Define schemas

class ModuleDR(Schema):
    module_name = fields.Str(description="Module name", required=True)
    dr_doc = fields.Dict(description="Module DR doc", required=True)

class StudyDR(Schema):
    study_id = fields.Int(description="MM study id", required=True)
    modules = fields.List(fields.Nested(ModuleDR, required=True))

class StudyMergedDR(Schema):
    study_id = fields.Int(description="MM study id", required=True)

    dr_auto_doc = fields.Dict(description="Merged DR doc of the study modules", required=True)
    dr_auto_schema = fields.Dict(description="Merged DR schema of the study modules", required=True)

    exit_code = fields.Int(description="Exit code of DR merging process", required=True)
    error_msg = fields.Str(description="Message of DR merging process", required=True)

class OutputSchema(Schema):
    msg = fields.String(description="Service availability message", required=True)


# register schemas with spec
spec.components.schema("Output", schema=OutputSchema)
spec.components.schema("ModuleDR", schema=ModuleDR)
spec.components.schema("StudyDR", schema=StudyDR)
spec.components.schema("StudyMergedDR", schema=StudyMergedDR)

# add swagger tags that are used for endpoint annotation
tags = [
            {'name': 'Digital Representation',
             'description': 'Main Module DR BE OpenAPI for using by FE'
            }
       ]

for tag in tags:
    print(f"Adding tag: {tag['name']}")
    spec.tag(tag)
