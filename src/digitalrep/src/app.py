# This is the Main Module for the DTOceanPlus suite of Tools.
# It is the main entry point to create and manage projects.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Flask DTOP Main Module 'dtop-mm' Digital Representation (DR) Application"""

from flask import Flask, jsonify
import sys

from src.endpoints.blueprint_dr import blueprint_dr
from src.endpoints.swagger import swagger_ui_blueprint, SWAGGER_URL


def create_flask_app(test_config=None):

    app = Flask(__name__)

    app.register_blueprint(blueprint_dr, url_prefix="/api/dr_service")

    from src.api_spec import spec

    with app.test_request_context():
        for fn_name in app.view_functions:
            if fn_name == 'static':
                continue
            print(f"Loading swagger docs for function: {fn_name}")
            view_fn = app.view_functions[fn_name]
            spec.path(view=view_fn)

    @app.route("/api/dr_service/swagger.json")
    def create_swagger_spec():
        """
        Swagger API definition.
        """
        return jsonify(spec.to_dict())

    app.register_blueprint(swagger_ui_blueprint, url_prefix=SWAGGER_URL)

    return app
