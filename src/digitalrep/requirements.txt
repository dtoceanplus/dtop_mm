Flask==1.1.2

# testing
pytest==6.0.2
requests==2.23.0

# swagger api docs
apispec==3.3.1
apispec-webframeworks==0.5.2
marshmallow==3.7.1
flask_swagger_ui==3.36.0
openapi_spec_validator==0.3.0

# production
gunicorn==20.0.4
