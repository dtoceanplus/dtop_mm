#/bin/bash

# This is the Main Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


# Description:
#  The script restores MM database from a dump 
#  in running docker stack service container
#
# Run Information:
#  chmod u+x ./restore_db.sh
#  ./restore_db.sh

set -e

db_dump=mm_db_dump_2021_03_06_03_59_04_sql
echo " the dump file = ${db_dump}"

if [ -f "${db_dump}" ]; then
    echo "dump file ${db_dump} exists."
else 
    echo "dump file ${db_dump} doen't exist."
    exit 1
fi

docker exec $(docker ps -q -f name=mm_database) bash -c "mysql -umm_user -pmm_pass --database=main_db < /home/${db_dump}"
