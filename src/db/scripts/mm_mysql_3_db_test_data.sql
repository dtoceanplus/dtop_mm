USE main_db;


INSERT INTO `user` (`id`, `name`, `password`, `email`, `created_at`) VALUES
	(2, 'admin_1', 'admin_1admin_1', 'admin_1@dtop.com', '2020-12-30 21:17:04'),
	(3, 'user_1', 'user_1user_1', 'user_1@dtop.com', '2020-12-30 21:27:11'),
	(4, 'user_2', 'user_2user_2', 'user_2@dtop.com', '2020-12-30 21:37:11'),
	(5, 'user_3', 'user_3user_3', 'user_3@dtop.com', '2020-12-30 21:47:11');


INSERT INTO `project` (`id`, `name`, `description`, `created_at`, `logo`, `technology`, `type_of_device`) VALUES
	(1, 'project 1', 'desc of project 1 created by user_1', '2020-12-30 11:15:23', 'logo 1', 'Wave', 'Fixed'),
	(2, 'project 2', 'desc of project 2 created by user_2', '2020-12-30 12:25:23', 'logo 2', 'Tidal', 'Floating'),
	(3, 'project 3', 'desc of project 3 created by user_3', '2020-12-30 12:35:23', 'logo 3', 'Wave', 'Floating');


INSERT INTO `study` (`id`, `name`, `description`, `template`, `created_at`, `project_id`) VALUES
	(1, 'study 1', 'desc of study 1 for project 1 created by user_1', 'Design and assessment', '2020-12-30 10:52:11', 1),
	(2, 'study 2', 'desc of study 2 for project 1 created by user_1', 'Design and assessment', '2020-12-30 11:52:21', 1),
	(3, 'study 3', 'desc of study 3 for project 2 created by user_2', 'Design and assessment', '2020-12-30 11:52:31', 2),
	(4, 'Default Study', 'SC MC Study', 'Design and assessment', '2020-12-30 10:52:11', 1),
	(5, 'Default Study', 'SC MC Study', 'Design and assessment', '2020-12-30 11:52:21', 2),
	(6, 'Default Study', 'SC MC Study', 'Design and assessment', '2020-12-30 11:52:31', 3);


INSERT INTO `digitalrep` (`id`, `name`, `description`, `created_at`, `data`, `study_id`) VALUES
	(1, 'digital representation 1 (DR1)', 'desc of digital representation 1 (DR1) for study_1 of the project_1', '2020-12-30 11:00:42', '{"name": "DR1", "param1": 30, "param2": "estimated"}', 1),
	(2, 'digital representation 2 (DR2)', 'desc of digital representation 1 (DR2) for study_2 of the project_1', '2020-12-30 11:10:52', '{"name": "DR2", "param1": 50, "param2": "estimated"}', 2);


INSERT INTO `event` (`id`, `name`, `description`, `created_at`) VALUES
	(1, 'create the user', 'creation of the admin_1 by admin', '2020-12-28 10:12:41'),
	(2, 'create the user', 'creation of the user_1 by admin_1', '2020-12-28 10:22:41'),
	(3, 'create the user', 'creation of the user_2 by admin_1', '2020-12-28 10:32:41'),
	(4, 'create the user', 'creation of the user_3 by admin_1', '2020-12-28 10:42:41'),
	(5, 'create the project', 'creation of the project 1 by user_1', '2020-12-29 11:12:41'),
	(6, 'create the project', 'creation of the project 2 by user_2', '2020-12-29 11:22:41'),
	(7, 'create the project', 'creation of the project 3 by user_3', '2020-12-29 11:32:41'),
	(8, 'create the project', 'creation of the project 4 by user_3', '2020-12-29 11:42:41'),
	(9, 'create the study', 'creation of the study 1 for project 1 by user_1', '2020-12-30 12:12:41'),
	(10, 'create the study', 'creation of the study 2 for project 1 by user_1', '2020-12-30 12:22:41'),
	(11, 'create the study', 'creation of the study 3 for project 2 by user_2', '2020-12-30 12:32:41'),
	(12, 'create the study', 'creation of the study 4 for project 2 by user_3', '2020-12-30 12:42:41'),
	(13, 'delete the study', 'deletion of the study 4 by user_3', '2020-12-30 10:52:11'),
	(14, 'delete the project', 'deletion of the project 4 by user_3', '2020-12-29 10:42:21');


INSERT INTO `role_user` (`id`, `user_id`, `role_id`) VALUES
	(2, 2, 1),
	(3, 3, 2),
	(4, 4, 2),
	(5, 5, 2);


INSERT INTO `event_user` (`id`, `user_id`, `event_id`) VALUES
	(1, 1, 1),
	(2, 2, 2),
	(3, 2, 3),
	(4, 2, 4),
	(5, 3, 5),
	(6, 4, 6),
	(7, 5, 7),
	(8, 5, 8),
	(9, 3, 9),
	(10, 3, 10),
	(11, 4, 11),
	(12, 5, 12),
	(13, 5, 13),
	(14, 5, 14);


INSERT INTO `project_user` (`id`, `project_id`, `user_id`) VALUES
	(1, 1, 3),
	(2, 2, 4),
	(3, 3, 5);


INSERT INTO `study_user` (`id`, `study_id`, `user_id`) VALUES
	(1, 1, 3),
	(2, 2, 3),
	(3, 3, 4);


INSERT INTO `study_module` (`id`, `study_id`, `module_id`, `entity_id`, `entity_name`, `entity_status`) VALUES
	(1, 1, 12, 1111, 'entity name for study 1 for module 12 (SI) and entity id  1111', 'Finished'),
	(2, 1, 8, 101010, 'entity name for study 1 for module 8 (SPEY) and entity id  101010', 'Finished'),
	(3, 2, 13, 2222, 'entity name for study 2 for module 13 (SG) and entity id  2222', 'Finished'),
	(4, 2, 9, 3333, 'entity name for study 2 for module 9 (RAMS) and entity id  3333', 'Finished'),
	(5, 3, 12, 1112, 'entity name for study 3 for module 12 (SI) and entity id  1112', 'Finished'),
	(6, 3, 8, 101011, 'entity name for study 3 for module 8 (SPEY) and entity id  101011', 'Finished');
