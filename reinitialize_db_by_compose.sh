#/bin/bash

# This is the Main Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


# Description:
#  The script reinitializes MM database 
#  in running docker compose sevice container

#
# Run Information:
#  chmod u+x ./reinitialize_db_by_compose.sh
#  ./reinitialize_db_by_compose.sh

set -e

docker-compose exec mm_database bash -c "mysql -umm_user -pmm_pass < /docker-entrypoint-initdb.d/mm_mysql_6_db_reinitialize_sql"
