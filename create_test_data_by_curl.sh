#/bin/bash

# This is the Main Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


# Description:
#  The script creates test data in initialized MM database using curl
#
# Run Information:
#  chmod u+x ./create_test_data_by_curl.sh
#  ./create_test_data_by_curl.sh


set -e

# 1. Create users:
#	1 admin : admin_1 by admin
#	3 users : users_1, users_2, users_3 by admin_1

#	RoleUserController

#	POST
#	/roles/{id}/users


curl -X POST "http://mm.dto.opencascade.com/api/roles/1/users" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"createdAt\":\"2020-12-30T21:17:03.728Z\",\"email\":\"admin_1@dtop.com\",\"name\":\"admin_1\",\"password\":\"admin_1admin_1\"}"

curl -X POST "http://mm.dto.opencascade.com/api/roles/2/users" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"createdAt\":\"2020-12-30T21:27:10.728Z\",\"email\":\"user_1@dtop.com\",\"name\":\"user_1\",\"password\":\"user_1user_1\"}"
curl -X POST "http://mm.dto.opencascade.com/api/roles/2/users" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"createdAt\":\"2020-12-30T21:37:10.728Z\",\"email\":\"user_2@dtop.com\",\"name\":\"user_2\",\"password\":\"user_2user_2\"}"
curl -X POST "http://mm.dto.opencascade.com/api/roles/2/users" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"createdAt\":\"2020-12-30T21:47:10.728Z\",\"email\":\"user_3@dtop.com\",\"name\":\"user_3\",\"password\":\"user_3user_3\"}"



# 2. Create Events for user actions

#	Events : create and delete of user, project, study

#	The events should be created during the execution of the correspondent actions by an user.

#	UserEventController

#	POST
#	/users/{id}/events


curl -X POST "http://mm.dto.opencascade.com/api/users/1/events" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"description\":\"creation of the admin_1 by admin\",\"name\":\"create the user\",\"createdAt\":\"2020-12-28T10:12:40.800Z\"}"

curl -X POST "http://mm.dto.opencascade.com/api/users/2/events" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"description\":\"creation of the user_1 by admin_1\",\"name\":\"create the user\",\"createdAt\":\"2020-12-28T10:22:40.800Z\"}"
curl -X POST "http://mm.dto.opencascade.com/api/users/2/events" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"description\":\"creation of the user_2 by admin_1\",\"name\":\"create the user\",\"createdAt\":\"2020-12-28T10:32:40.800Z\"}"
curl -X POST "http://mm.dto.opencascade.com/api/users/2/events" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"description\":\"creation of the user_3 by admin_1\",\"name\":\"create the user\",\"createdAt\":\"2020-12-28T10:42:40.800Z\"}"



curl -X POST "http://mm.dto.opencascade.com/api/users/3/events" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"description\":\"creation of the project 1 by user_1\",\"name\":\"create the project\",\"createdAt\":\"2020-12-29T11:12:40.800Z\"}"
curl -X POST "http://mm.dto.opencascade.com/api/users/4/events" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"description\":\"creation of the project 2 by user_2\",\"name\":\"create the project\",\"createdAt\":\"2020-12-29T11:22:40.800Z\"}"
curl -X POST "http://mm.dto.opencascade.com/api/users/5/events" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"description\":\"creation of the project 3 by user_3\",\"name\":\"create the project\",\"createdAt\":\"2020-12-29T11:32:40.800Z\"}"
curl -X POST "http://mm.dto.opencascade.com/api/users/5/events" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"description\":\"creation of the project 4 by user_3\",\"name\":\"create the project\",\"createdAt\":\"2020-12-29T11:42:40.800Z\"}"



curl -X POST "http://mm.dto.opencascade.com/api/users/3/events" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"description\":\"creation of the study 1 for project 1 by user_1\",\"name\":\"create the study\",\"createdAt\":\"2020-12-30T12:12:40.800Z\"}"
curl -X POST "http://mm.dto.opencascade.com/api/users/3/events" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"description\":\"creation of the study 2 for project 1 by user_1\",\"name\":\"create the study\",\"createdAt\":\"2020-12-30T12:22:40.800Z\"}"
curl -X POST "http://mm.dto.opencascade.com/api/users/4/events" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"description\":\"creation of the study 3 for project 2 by user_2\",\"name\":\"create the study\",\"createdAt\":\"2020-12-30T12:32:40.800Z\"}"
curl -X POST "http://mm.dto.opencascade.com/api/users/5/events" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"description\":\"creation of the study 4 for project 2 by user_3\",\"name\":\"create the study\",\"createdAt\":\"2020-12-30T12:42:40.800Z\"}"


curl -X POST "http://mm.dto.opencascade.com/api/users/5/events" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"description\":\"deletion of the study 4 by user_3\",\"name\":\"delete the study\",\"createdAt\":\"2020-12-30T10:52:10.800Z\"}"

curl -X POST "http://mm.dto.opencascade.com/api/users/5/events" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"description\":\"deletion of the project 4 by user_3\",\"name\":\"delete the project\",\"createdAt\":\"2020-12-29T10:42:20.800Z\"}"



# 3. Create projects by users

#	the project 1 by user_1 (user id 3)
#	the project 2 by user_2 (user id 4)
#	the project 3 and 4 by user_3 (user id 5)

#	At the end the project 4 for user 3 is deleted - see below.

#	UserProjectController

#	POST
#	/users/{id}/projects


curl -X POST "http://mm.dto.opencascade.com/api/users/3/projects" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"createdAt\":\"2020-12-30T11:15:23.386Z\",\"description\":\"desc of project 1 created by user_1\",\"logo\":\"logo 1\",\"name\":\"project 1\",\"technology\":\"Wave\",\"typeOfDevice\":\"Fixed\"}"
curl -X POST "http://mm.dto.opencascade.com/api/users/4/projects" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"createdAt\":\"2020-12-30T12:25:23.386Z\",\"description\":\"desc of project 2 created by user_2\",\"logo\":\"logo 2\",\"name\":\"project 2\",\"technology\":\"Tidal\",\"typeOfDevice\":\"Floating\"}"
curl -X POST "http://mm.dto.opencascade.com/api/users/5/projects" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"createdAt\":\"2020-12-30T12:35:23.386Z\",\"description\":\"desc of project 3 created by user_3\",\"logo\":\"logo 3\",\"name\":\"project 3\",\"technology\":\"Wave\",\"typeOfDevice\":\"Floating\"}"
curl -X POST "http://mm.dto.opencascade.com/api/users/5/projects" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"createdAt\":\"2020-12-30T12:45:23.386Z\",\"description\":\"desc of project 4 created by user_3\",\"logo\":\"logo 4\",\"name\":\"project 4\",\"technology\":\"Tidal\",\"typeOfDevice\":\"Floating\"}"

# DELETE one required project (of numerous projects) created by user

#project id=4
curl -X DELETE "http://mm.dto.opencascade.com/api/users/5/projects?where=%7B%22id%22%3A4%7D" -H  "accept: application/json"


# 4. Create studies by users

# 	2 studies : study_1 and study_2 for the project_1 create by by user_1
# 	1 study :   study_3 for the project_2 create by user_2
# 	1 study :   study_4 for the project_3 create by user_3

# 	At the end the study_5 and study_4 for the project_3 for user_3 are deleted - see below.


# 	UserStudyController

# 	POST
# 	/users/{id}/studies

curl -X POST "http://mm.dto.opencascade.com/api/users/3/studies" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"createdAt\":\"2020-12-30T10:52:10.800Z\",\"description\":\"desc of study 1 for project 1 created by user_1\",\"name\":\"study 1\",\"projectId\":1,\"template\":\"Design and assessment\"}"
curl -X POST "http://mm.dto.opencascade.com/api/users/3/studies" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"createdAt\":\"2020-12-30T11:52:20.800Z\",\"description\":\"desc of study 2 for project 1 created by user_1\",\"name\":\"study 2\",\"projectId\":1,\"template\":\"Design and assessment\"}"
curl -X POST "http://mm.dto.opencascade.com/api/users/4/studies" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"createdAt\":\"2020-12-30T11:52:30.800Z\",\"description\":\"desc of study 3 for project 2 created by user_2\",\"name\":\"study 3\",\"projectId\":2,\"template\":\"Design and assessment\"}"
curl -X POST "http://mm.dto.opencascade.com/api/users/5/studies" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"createdAt\":\"2020-12-30T11:52:40.800Z\",\"description\":\"desc of study 4 for project 3 created by user_3\",\"name\":\"study 4\",\"projectId\":3,\"template\":\"Design and assessment\"}"

curl -X POST "http://mm.dto.opencascade.com/api/users/5/studies" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"createdAt\":\"2020-12-30T11:52:50.800Z\",\"description\":\"desc of study 5 for project 3 created by user_3\",\"name\":\"study 5\",\"projectId\":3,\"template\":\"Design and assessment\"}"


# DELETE one required study 5 and then study 4 (of numerous studies) created by user

#study id=5  ?where={"id":5}"
curl -X DELETE "http://mm.dto.opencascade.com/api/users/5/studies?where=%7B%22id%22%3A5%7D" -H  "accept: application/json"
#study id=4  ?where={"id":4}"
curl -X DELETE "http://mm.dto.opencascade.com/api/users/5/studies?where=%7B%22id%22%3A4%7D" -H  "accept: application/json"



# 5. Create digital representation (DR) for study

#	DR1 for study_1 of the project_1
#	DR2 for study_2 of the project_1


#	StudyDigitalrepController

#	/studies/{id}/digitalrep


curl -X POST "http://mm.dto.opencascade.com/api/studies/1/digitalrep" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"createdAt\":\"2020-12-30T11:00:42.372Z\",\"data\":\"{\\\"name\\\":\\\"DR1\\\",\\\"param1\\\":30,\\\"param2\\\":\\\"estimated\\\"}\",\"description\":\"desc of digital representation 1 (DR1) for study_1 of the project_1\",\"name\":\"digital representation 1 (DR1)\",\"studyId\":1}"
curl -X POST "http://mm.dto.opencascade.com/api/studies/2/digitalrep" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"createdAt\":\"2020-12-30T11:10:52.372Z\",\"data\":\"{\\\"name\\\":\\\"DR2\\\",\\\"param1\\\":50,\\\"param2\\\":\\\"estimated\\\"}\",\"description\":\"desc of digital representation 1 (DR2) for study_2 of the project_1\",\"name\":\"digital representation 2 (DR2)\",\"studyId\":2}"



# 7. Create study module entity map

#	Creating and deleting the row of the correspondent table "study_module" 
#	are performed during "Registering and "Unregistering" the module entity in the study.

#	StudyModuleEntityController

#	POST
#	/studies-modules-entities

#project 1
#study 1
#study 2

curl -X POST "http://mm.dto.opencascade.com/api/studies-modules-entities" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"entityId\":1111,\"entityName\":\"entity name for study 1 for module 12 (SI) and entity id  1111\",\"entityStatus\":\"Finished\",\"moduleId\":12,\"studyId\":1}"
curl -X POST "http://mm.dto.opencascade.com/api/studies-modules-entities" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"entityId\":101010,\"entityName\":\"entity name for study 1 for module 8 (SPEY) and entity id  101010\",\"entityStatus\":\"Finished\",\"moduleId\":8,\"studyId\":1}"

curl -X POST "http://mm.dto.opencascade.com/api/studies-modules-entities" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"entityId\":2222,\"entityName\":\"entity name for study 2 for module 13 (SG) and entity id  2222\",\"entityStatus\":\"Finished\",\"moduleId\":13,\"studyId\":2}"
curl -X POST "http://mm.dto.opencascade.com/api/studies-modules-entities" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"entityId\":3333,\"entityName\":\"entity name for study 2 for module 9 (RAMS) and entity id  3333\",\"entityStatus\":\"Finished\",\"moduleId\":9,\"studyId\":2}"

#project 2 
#study 3

curl -X POST "http://mm.dto.opencascade.com/api/studies-modules-entities" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"entityId\":1112,\"entityName\":\"entity name for study 3 for module 12 (SI) and entity id  1112\",\"entityStatus\":\"Finished\",\"moduleId\":12,\"studyId\":3}"
curl -X POST "http://mm.dto.opencascade.com/api/studies-modules-entities" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"entityId\":101011,\"entityName\":\"entity name for study 3 for module 8 (SPEY) and entity id  101011\",\"entityStatus\":\"Finished\",\"moduleId\":8,\"studyId\":3}"
