# Main Module (MM) of the DTOceanPlus project (DTOP)

MM manages the projects, the studies, the users and info related to results produced by the modules in the frames of project studies as well as other basic functionality. MM also provides a interaction with the modules by exposing correspondent OpenAPI.

Please see the details below.

## Changing and validation of MM requirements

The MM functional requirements and approach for integration of the modules with MM were being activelly discussed by all DTOP participants till the December, 2020.

During the DTOceanPlus_TWG-2020.12.10 MM requirements have been agreed.

In particular, they are related to Integration of MM and Modules based on using mainly OpenAPI with minimal impact on exiting Module FrontEnds (FEs)  => FEs decoupling.

- MM provides
  - managing the projects, studies, users and other data
  - creation and deletion of the module Entities in the frame of given MM Project Study
  - filtering, sorting and paginations of data
  - separate access of users to their projects and studies.
  - and other necessary functionality
- Module BackEnds (BEs) Requirement : every element of the module Entities list should have unique Sub-Studies == Entities identifiers, "id": 1, 2, 3 used for registration in MM Study as well as Entity name/title.

## Correspondent MM update

Starting this date OCC could start the implementation of this solution and necessary changes including :
- MM data model, database and code refactoring
- MM OpenAPI development with the necessary GET endpoints to serve modules needs and get info about module Entities registered in MM
- Related debugging, testing and providing the tests and GitLab CI procedures

## Components used for MM development and testing

- `nginx` as a proxy service - for development
- `traefik` as a proxy service - for production environment
- `vue.js` for MM FE
- `loopback.io` for MM BE
- `flask` for MM Digital Representation (DR) based on repository `api-cooperative`
- `mysql` as MM database providing MM persistence
- `cypress` as a e2e tests tool

The MM also uses :
- `jest.js` as a unit test tool

## MM Data Model and Database schema

![](./docs/images/MM_data_model_9_1_only.png)

The next MySQL scripts define Database schema and test data :

```bash
mm_mysql_1_db_schema.sql
mm_mysql_2_db_init_data.sql
mm_mysql_3_db_test_data.sql
mm_mysql_4_db_fake_data_sql
```

The `.sql` scripts are located in the directory `.\src\db\scripts` and loaded automatically on initial running of the `mm_database` service.

## MM basic use cases and correspondent calls of MM Open API

Please see MM basic use cases of FE and BE interaction and correspondent `curl `calls of MM Open API in `.\create_test_data_with_curl.sh`.

This bash script creates the same test data as in `mm_mysql_3_db_test_data.sql` in the database initialized with two sql sripts : `mm_mysql_1_db_schema.sql` and `mm_mysql_2_db_init_data.sql` only.

## Integration OpenAPI of Main Module for using by modules

- [Integration OpenAPI of Main Module (MM) for using by modules](./intergation_OpenAPI.md)

This document describes the validated MM Integration OpenAPI for using by modules : the endpoints and the examples.

The database should be initialized with test data by the provided MySQL script `mm_mysql_3_db_test_data.sql` or by
bash script `.\create_test_data_with_curl.sh`.

## Development Environment

Local development environment is based on using docker compose and code available through docker volumes.

All development steps can be also performed in cleaned up environment on local machine, for example using conda environment.

### Prerequisites

You need to install:

- [Docker-compose](https://docs.docker.com/compose/install/)
- [Pre-commit](https://pre-commit.com/)

It is recommended to use IDE:

- [VSCode](https://code.visualstudio.com)

### Pre-install

If you start the project locally for development, for local resolving `mm.dto.opencascade.com` add the correspondent line (*) to `C:\Windows\System32\drivers\etc\hosts` or `/etc/hosts` :

```shell
127.0.0.1 mm.dto.opencascade.com
```

The directory `./src/db/volume/data/mm` is used by `docker-compose` as volume with mm_database data.

The directory `./src/db/scripts` contains SQL scripts automatically used for initialization of MM database and populating it with basic or test data at the first MySQL start with mm_database service.

### Running

And init git hooks

```shell
pre-commit install
```

Execute next command to start docker containers:

```shell
docker-compose up --detach`
```

For building code and environment you can execute next command at the root directory of repository:

```shell
docker-compose build
```

Four MM docker containers should be properly started :

```bash
(dtop_mm) PS D:\dev\dtop\dtop_mm\src\backend> docker ps

CONTAINER ID        IMAGE                       COMMAND                  CREATED             STATUS              PORTS                               NAMES
0762d3f0bb30        nginx:1.19-alpine           "/docker-entrypoint.…"   7 minutes ago       Up 7 minutes        0.0.0.0:80->80/tcp                  mm-nginx
caa6a6fe064c        dtop_mm_mm_frontend:0.0.1   "docker-entrypoint.s…"   7 minutes ago       Up 7 minutes        0.0.0.0:8080->8080/tcp              mm-frontend
0a243b92e8c0        mysql:8.0                   "docker-entrypoint.s…"   7 minutes ago       Up 7 minutes        0.0.0.0:3306->3306/tcp, 33060/tcp   mm-database
169a7f9bd9a7        dtop_mm_digitalrep:0.0.1    "gunicorn --workers=…"   20 minutes ago      Up 48 seconds       0.0.0.0:5000->5000/tcp              mm-digitalrep
43eb34f78f1b        dtop_mm_mm_backend:0.0.1    "docker-entrypoint.s…"   7 minutes ago       Up 7 minutes        0.0.0.0:3000->3000/tcp              mm-backend
```

Execute next command to stop docker containers:

```shell
docker-compose down
```

### Access to Database data

Using the MysQL credentials defined  in `docker-compose.yml` you can acceess to MM database created in docker container for example by means of `HeidiSQL`, as shown on this image :

![](./docs/images/mm_db_heidisql.png)

### MM OpenAPI

MM OpenAPI is implemented using of LoopBack 4.

Current version of MM OpenAPI is available in the directory `.\src\backend` :

```bash
./openapi.json
./openapi.yaml
```

as well as in [api-cooperative](https://gitlab.com/opencascade/dtocean/sandbox/api-cooperative)

### Access to MM OpenAPI with Swagger UI

Swagger UI Single Page Application (SPA) provides users with interactive environment to test the API endpoints defined by the raw spec found at `http://mm.dto.opencascade.com/api/openapi.json`

swagger-ui-dist as a dependency-free module that includes everything you need to serve Swagger UI in a server-side project is provided.

It allow to perform real tests of the designed MM BE Controllers and Endpoints that are used by MM FE and other modules as shown on the image below.

In the browser MM OpenAPI with Swagger UI is available with URL :

```bash
http://mm.dto.opencascade.com/api
```

![](./docs/images/MM_OpenAPI_Swagger-UI_1.png)

#### MM Digital Representation (DR) Service and OpenAPI

MM Digital Representation (DR) Service and OpenAPI are implemented basing on Flask and [api-cooperative](https://gitlab.com/opencascade/dtocean/sandbox/api-cooperative.

This auxilary service provides checking and merging of DRs for all study modules and addresses the correspondent needs of MM FE with the next workflow.

![](./docs/images/MM_DR_4.png)

Current version of MM DR code and OpenAPI is available in the directory `.\src\digitalrep`. Please see =the commented code for the details.

##### Preparation and updating of MM DR code

Run the script `./prepare_dr_schemas.sh` to prepare and update the MM DR code using `api-cooperative` :

```bash
./prepare_dr_schemas.sh
```

Run docker-compose DR service "mm_digitalrep" :
```bash
docker-compose up mm_digitalrep
or
docker-compose up -d
```

To stop run `docker-compose down`.

Execute the tests of MM DR OpenAPI Endpoints using running 'mm-digitalrep' docker container :
```
cd  src/digitalrep/

docker exec mm-digitalrep pytest -v ./tests

============================= test session starts ==============================
platform linux -- Python 3.8.11, pytest-6.0.2, py-1.10.0, pluggy-0.13.1 -- /usr/local/bin/python
cachedir: .pytest_cache
rootdir: /app
collecting ... collected 4 items

tests/test_endpoints.py::test_study_digitalrep_availability PASSED       [ 25%]
tests/test_endpoints.py::test_sample_study_digitalrep PASSED             [ 50%]
tests/test_endpoints.py::test_full_study_digitalrep PASSED               [ 75%]
tests/test_endpoints.py::test_swagger_specification PASSED               [100%]

============================== 4 passed in 3.52s ===============================
```

Additionally to the provided `pytests` the directory `digitalrep\tests\` includes the test script   `check_dr_endponts_by_curl.sh` as well as test data for POST request payload.


Generate MM DR OpenAPI json and yaml files :
```
cd  src/digitalrep/

curl http://localhost:5000/api/swagger.json | python -m json.tool > openapi.json

swagger-cli bundle -t yaml -o openapi.yaml openapi.json
```

##### Access to MM DR Service and OpenAPI with Swagger UI

Swagger UI allows testing of MM DR Services API endpoints and services interactivelly in a browser.

In the development environment MM DR OpenAPI with Swagger UI is available with URLs :

```bash
http://localhost:5000/api/docs/

http://localhost:5000/api/v1/path_for_blueprint_dr/dr_test

http://localhost:5000/api/v1/path_for_blueprint_dr/study_digitalrep
```

![](./docs/images/MM_DR_OpenAPI_Swagger-UI.png)


### Access to MM FE

In the browser MM FE  is available with URL :

```bash
http://mm.dto.opencascade.com
```

### Testing

You can execute dredd tests of MM BE OpenAPI Endpoints using running 'mm-backend' docker container, for example :

```
docker-compose up -d --build mm_backend
docker exec mm-backend bash -c "npm install dredd@12.2.1; node_modules/dredd/bin/dredd"
docker-compose down
```

You can also start e2e integration tests via Cypress.

```shell
docker exec mm-e2e npx cypress run --browser chrome --headless
```

And unit tests for the frontend via Jest.

```shell
docker exec mm_frontend npm run test:unit
```

And lint for the frontend via Eslint.

```shell
docker exec mm_frontend npm run lint
```
