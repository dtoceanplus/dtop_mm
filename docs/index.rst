.. _mm-home:

Main Module
=====================

Introduction 
------------

The Main Module (MM) is the main entry point of the DTOceanPlus suite.
From this module the user can create :ref:`Projects <mm-definitions-project>` and :ref:`Studies <mm-definitions-study>`.

Structure
---------

This module's documentation is divided into three main sections:

- :ref:`mm-tutorials` to give step-by-step instructions on using MM for new users. 

- Description of the different :ref:`mm-features`. 

- A section on :ref:`mm-definitions` that describes the different concpets of MM. 

.. toctree::
   :hidden:
   :maxdepth: 1

   tutorials/index
   how_to/index
   explanation/index

Functionalities
---------------

The Main Module major functionalities are:

#. Creation of Projects and Studies

#. Generate Digital Representation of a study

#. Management of users
