.. _mm-tutorials:

*********
Tutorials
*********

Below is a set of tutorials that guide the user through each of the main functionalities of the Main Module tool.
They are intended for those who are new to the Main Module tool.
It is recommended to follow the tutorials in the suggested order listed below, as certain tutorials are dependent on others. 

#. :ref:`mm-tutorial-create_project`
#. :ref:`mm-tutorial-compare`
#. :ref:`mm-tutorial-dr`
#. :ref:`mm-tutorial-users`

.. toctree::
   :maxdepth: 1
   :hidden:

   create_project
   compare_studies
   digital_representation
   manage_users
