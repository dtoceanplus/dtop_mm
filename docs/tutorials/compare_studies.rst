.. _mm-tutorial-compare: 

Fork & Compare Studies
======================

After having designed and assessed its project, the user can *fork* its study
to try a slightly different design and check if its improve the results.

Fork a study
------------

#. After creating a study, go to the Studies tab of the project.

#. Click the ``Fork Study`` button.

#. In the dialog select the study to fork, and the module where you want to fork.

   .. image:: ../images/Fork_Study.jpg
      :alt: Fork Study
      :align: center
      :scale: 80%

   .. tip:: You can use the *Description* field to note what kind of changes you
     plan to do with this fork. 

#. After validating a new study is created.

   .. warning:: All the modules **before** the fork are **common to both studies**.
      Therefor any change made to these modules will affect both studies.



Compare studies
---------------

**NOT Implemented yet**
