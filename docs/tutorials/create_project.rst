.. _mm-tutorial-create_project: 

Create Project & Studies
========================

This tutorial will explain how to create a project and studies. 

See :ref:`mm-definitions` for the definition of the main concepts.

Create the project
------------------

#. Connect to the DTOceanPlus application with a user with *User* profil.

#. On the Dashboard click the ``Projects`` button.

   .. image:: ../images/Dashboard.jpg
     :alt: Dashboard
     :scale: 50%
     :align: center

#. The *Projects* page lists the existing project. Click the ``Create New Project`` button to create a new project.

#. Fill the information of the project: project name, Technology, Type of Device.
   You can also add a logo and a short desription.

   .. image:: ../images/CreateProject.jpg
     :alt: Dashboard
     :scale: 50%
     :align: center

#. You can edit the project information using the ``Edit`` button in the porjects list. 

#. Click the ``View`` Button in the list to open the project.


Create studies
--------------

A project is composed of one or several studies. (See :ref:`mm-definitions`).

.. note::
   It is possible to create different kind of studies:
     * Standalone: with only one module.
     * Design and Assessment: with all the modules.

To create a study, from the project page:

#. Select the tab corresponding to the type of study you want to create.:

   .. image:: ../images/StudyTabs.jpg
     :alt: Study Tabs
     :align: center

#. Click the ``Create New <type> Study`` button.
#. Give a name and a description to the study.

   .. image:: ../images/CreateStudy.jpg
     :alt: Create Study
     :scale: 80%
     :align: center

#. If you created a standalone study, you need to select the type of the module.

   .. image:: ../images/CreateStudyStandalone.jpg
     :alt: Create Standalone Study
     :scale: 80%
     :align: center

#. Once the study is created, click the ``Open`` button to open the study.

   .. image:: ../images/List_of_Studies.jpg
     :alt: List of Studies
     :align: center

#. Use the ``Edit`` button to rename the study or change the description.

#. Use the ``Delete`` button to delete the study.

Use Modules
-----------

#. The study page shows all the available modules.

   .. image:: ../images/StudyPage.jpg
     :alt: Study
     :align: center

#. For ``Design & Assessment`` study, *Site* and *Machine* needs to be created
   before using the other modules. They are common to all the studies of
   the same project.

#. Other modules can be activated by creating the corresponding ``Create``
   button.

#. A dialog will be displayed to initiate the module. The list of parameters
   depends on the module.

   .. image:: ../images/CreateEntity.jpg
     :alt: ET module
     :scale: 80%
     :align: center

Share the Project
-----------------

You can share the project with other users:

#. Go to the *Users* tab.

#. Register new users to your project.

.. warning::
   * The current version of the project does not check concurent access to the project.
   * All users have the same rights and can unregister any other users.
   * Users linked to a project cannot be deleted.

