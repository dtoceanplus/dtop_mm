.. _mm-definitions:

***********
Definitions
***********

This section describes the background and theory behind the module. 
Describe the calculation methods and assumptions in this section. 
Talk about why certain decisions were made. 
It is expected that a lot of this section can be copied from the alpha version deliverables. 

The sub-headings below are just examples. 
Each module can organise this section as appropriate. 

.. _mm-definitions-project:

Project
-------

.. admonition:: Definition

    The **project** is the main concept of DTOceanPlus suite. It regroups all the date related
    to the "real project" conducted by the user.


.. _mm-definitions-study:

Study
-----

.. admonition:: Definition

    A project is composed of one or several **studies**.

    In the frame of the study, the user will use one or several tools to design the solution
    and then asse it.

    The user can use the studies to test different design and compare them.

.. _mm-definitions-entity:

Entity
------

.. admonition:: Definition
    
    An **entity** represents the instanciation of a module in the frame of a study.
    
    It contains all the data (input and output) concerning the module for this study.

Digital Representation
----------------------

.. admonition:: Definition

    The **Digital Representation** is the export of all the main data of a study.
    It is uses the JSON file format.

