.. _mm-dialog-project:

*******
Project
*******

The project page is composed of several tabs. Each tabs is dedicated to a specific feature:

* *Users*: manage access rights to the project.
* *Standalone* & *Design & Assessment*: manage studies.

.. image:: ../images/ProjectPage.jpg
  :alt: Project page
  :align: center

In the *Studies* tab, the user manages studies (see :ref:`mm-tutorial-create_project` tutorial).

.. image:: ../images/List_of_Studies.jpg
  :alt: Studies
  :align: center
