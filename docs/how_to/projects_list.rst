.. _mm-dialog-projects:

****************
List of Projects
****************

The Projects page list all the projects created by or shared with the user.

.. image:: ../images/List_of_Projects.jpg
  :alt: List of Projects
  :align: center

The projects are presented in a table. By clicking on the buttons on each line, the user can:
* ``View`` the project
* ``Edit`` the project information
* ``Delete`` the project

When the user clicks on the ``Delete`` button, a confirmation dialog will be displayed to the user with
its characteristics (number of studies and entities).

.. image:: ../images/DeleteProject.jpg
  :alt: Delete Project
  :scale: 80%
  :align: center

When the user confirms its choice, the project and all the its studies and entities are deleted.