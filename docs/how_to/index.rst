.. _mm-features:

********
Features
********

This section describes the mai features of the Main Module of the
DTOceanPlus suite of tools.

.. toctree::
   :maxdepth: 1

   dashboard
   projects_list
   project
   study
