.. _mm-dialog-study:

*****
Study
*****

The Study page allows the user to activate the different modules of the
DTOceanPlus suite in the frame of the current study.

.. image:: ../images/StudyPage.jpg
  :alt: Study
  :align: center

To activate a module, click on corresponding ``Create`` button.
A dialog, specific to each tool, is displayed.

.. image:: ../images/CreateEntity.jpg
  :alt: Create Entity
  :scale: 80%
  :align: center

The user will give a name to the entity and validate by clicking the ``Create``
button. When the dialog is closed, the ``Create`` button is replaced by
two buttons.

To access the module, click the ``Open`` button. The module will be
opened in a new tab of the browser. Once you have finished to use the module
close the tab to come back to the study.

If you want to restart the module, you can delete the current results by clicking
the ``Delete`` buttons, and then create a new one.
