.. _mm-dialog-dashboard:

*********
Dashboard
*********

The Dashboard is the main screen of the MM.

.. image:: ../images/Dashboard.jpg
  :alt: Dashboard
  :scale: 50%
  :align: center

The Dashboard presents the main features to the user:

* :ref:`Projects <mm-dialog-projects>`: access the list of projects
* Catalogs: link to the Catalog module
* Logs: See global logs of the application (not used for now).
* Documentation: Link to the documentation of the DTOceanPlus suite.

.. note:: 
    The main features are also available in the navigation bar.

.. note::
    The Dashboard of Administrator is different as the ``Projects`` button is
    replaced by a ``Users`` button to access the list of users (see :ref:`mm-tutorial-users`).

  .. image:: ../images/DashboardAdmin.jpg
    :alt: Dashboard
    :scale: 50%
    :align: center
